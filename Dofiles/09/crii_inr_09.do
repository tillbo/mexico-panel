capture log close
set more off

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/crii_inr_09.log", replace

use "Daten 09/ii_inr_09", clear


**rural income from selling products

	egen incrur=rowtotal( inr03a inr03b inr03c inr03d inr03e inr03f inr03g inr03h inr03i inr03j inr03k), miss  //last year
	egen incrur_mth=rowtotal(  inr04a- inr04k), miss															//last month






**labeling variables
	label var incrur "rural income per year"
	label var incrur_mth "rural income last month"

**keep neede variables

	keep incrur-incrur_mth ls folio

save "Daten 09/ii_inr_09incrur", replace

log close

exit
