set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/master_09.log", replace
*merging 09 datasets

use "Daten 09/iiib_ec_09chro.dta", clear


merge 1:1 folio ls using "Daten 09/iiib_portad", nogen keep(master match)
merge 1:1 folio ls using "Daten 09/iiia_tb_09inco", nogen keep(master match)
merge 1:1 folio ls using "Daten 09/c_ls_09contr", nogen keep(master match)
merge 1:1 pid_link using "Daten 09/s_sa_09heal", nogen keep(master match)
merge 1:1 pid_link  using "Daten 09/iiib_es_09hlth", nogen keep(master match)
merge 1:1 pid_link using "Daten 09/iiib_tp_09parents", nogen keep(master match)
merge 1:1 pid_link using "Daten 09/iiib_sm_09mental", nogen keep(master match)
merge 1:1 pid_link using "Daten 09/iiia_ed_09educ", nogen keep(master match)
merge 1:1 pid_link using "Daten 09/iiib_ca_09insur", keep(master match) nogen

merge m:1 folio using "Daten 09/ii_ah_hhassets09", nogen keep(master match) force
merge m:1 folio using "Daten 09/hhexp09", keep(master match) nogen
merge m:1 folio using "Daten 09/ii_se_09shock", nogen keep(master match)
merge m:1 folio using "Daten 09/ii_in_09nolabinc", nogen keep(master match)
merge m:1 folio using "Daten 09/c_portad_09hhsize", nogen keep(master match)
merge m:1 folio using "Daten 09/c_cvo", nogen keep(master match)
merge m:1 folio using "Daten 09/c_cv", nogen keep(master match)
merge 1:1 pid_link  using "Daten 09/iiib_09conpor", nogen keep(master match)



// create old version folio and pid_link as in previous waves without letters in oreder to have unique identifiers for panel. To preserve the new versions I clone folio and pid_link //
// to create two additional variables with new identifiers with the "new" postfix. Based on these I create pid_link and folio in the old style


clonevar pid_link_new = pid_link
drop pid_link
clonevar folio_new = folio
drop folio
clonevar ls_new = ls
drop ls
gen folio = regexs(0) if regexm(pid_link_new, "^[0-9]+")
gen ls = regexs(0) if regexm(pid_link_new, "[0-9]*$") //
gen newid= folio + ls //	for the later use of bootstrap methods in panels
gen pid_link= folio + ls //
destring newid, replace
destring pid_link folio ls ls00 ls06 ls07, replace


gen wave3 = 1




save "Daten 09/master_merged.dta", replace

log close
exit


