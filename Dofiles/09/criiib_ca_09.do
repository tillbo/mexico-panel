set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiib_ca_09.log", replace


***insurance information

use "Daten 09/iiib_ca_09", clear
*recoding variables from 1=Yes, 3=No to 1=Yes, 0=No
	label define yesno 0 "no" 1 "yes"

	foreach var of varlist   ca01 - ca02g_1 {
	replace `var'=0 if `var'==3
	replace `var'=. if `var'==8
	label values `var' yesno
	}
save "Daten 09/iiib_ca_09insur", replace
log close
exit

