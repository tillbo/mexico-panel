capture log close
global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiib_tp_09.log", replace

**dataset for non-resident parents

use "Daten 09/iiib_tp_09.dta", clear

**chronic diseases parents

	label define yesno 0 "no" 1 "yes"
	foreach var of varlist  tp16m_1 tp16p_1{
	replace `var'=0 if `var'==3
	replace `var'=. if `var'==8
	label values `var' yesno
	}

	#delimit ;

	label define chronic1
	  1 "Diabetes"
      2 "Lung disease"
      3 "Heart disease"
      4 "Hypertension"
      5 "Arthritis/rheumatism/gout/uric acid"
      6 "Cancer"
      7 "Eye problem"
      8 "Ear problem"
      9 "Liver disease"
      10 "Alcoholism"
      11 "Embolism"
      12 "Kidney disease"
      13 "Ulcer"
      14 "The skeletal system"
      15 "Paralysis and disability"
      16 "General pain"
      17 "Gastritis and colitis"
      18 "Hernia"
      19 "Cholesterol"
      20 "Tumor/cysts"
      21 "Nerves"
      22 "Gall bladder"
      23 "Epilepsy and convulsions"
      88 "Other";

	label define educ
      1 "Without instruction"
      2 "Preschool or Kinder"
      3 "Elementary"
      4 "Secondary"
      5 "Hight School"
      6 "Normal basic/high School"
      7 "College"
      8 "Graduate"
      98 "DK";


	  #delimit cr

	  foreach var of varlist  tp16m_2 tp16p_2{
	  label value `var' chronic1
	  }

	  foreach var of varlist  tp11m tp11p{
	  label value `var' educ
	  replace `var'=. if `var'==98
	  }

	keep folio ls pid_link tp11m tp11p tp12m_1 tp12p_1 tp16m_1 tp16m_2 tp16p_1 tp16p_2

save "Daten 09/iiib_tp_09parents", replace

log close
exit
