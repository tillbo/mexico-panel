set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datens�tze""
cd $homefolder
**log-file starten**

log using "Logs/crs_sa_09.log", replace


*disease and disability dataset

use "Daten 09/s_sa_09", clear

*recoding has disease or disability (1=yes, 3=no to 1=yes, 0=no)
	replace sa18_1=0 if sa18_1==3
	replace sa19_1=0 if sa19_1==3
	replace sa18_2=13 if sa19_2==88    //making "other" disability 13 instead of 88
	replace sa19_2=8 if sa19_2==88    //making "other" disability 8 instead of 88


*labeling
	label define sex 1 "male" 3 "female"
	label values sa01 sex
	label define yesno 0 "no" 1 "yes"
	label values  sa18_1 yesno
	label values  sa19_1 yesno

#delimit ;

	label define sick
		1 "Dermatological diseases"
		2 "Respiratory diseases"
		3 "Dental diseases"
		4 "Cold"
		5 "Hypertension"
		6 "Eyesight problems"
		7 "Obesity"
		8 "Diabetes"
		9 "Arthritis/rheumatism"
		10 "Varices"
		11 "Bone Structure"
		12 "Anemia and undernourishment"
		13 "Other" ;

	label define disability
		1 "Ear problems"
		2 "Eyesight problems"
		3 "Mental delay"
		4 "General handicap"
		5 "Paralisis or disability"
		6 "Temporary disability(fractures, sprains)"
		8 "Other" ;
		#delimit cr

	label values sa18_2 sick
	label values sa19_2 disability



* generating variable Body Mass Index (BMI)
* BMI: weight in kilograms divided by the square of the height in metres (kg/m�) (WHO definition)

	gen height_m =  (sa07_21/100)			//variable with height in meters (height in cm divided by 100)
	gen BMI = ( sa08_21/ height_m^2)		//BMI=kg/m�

	label var height_m "measured heigth in cm divided by 100"
	label var BMI "Body Mass Index"
*new waist variable because they are differently coded in 02 and 09
	gen waist=sa11_21
	label var waist "waist in cm"
	
* new HbA1c variable

clonevar hba1c = sa16d_21 

*keeping only useful variables
//	keep folio ls pid_link  sa01 sa03 sa18_1 sa18_2 sa19_1 sa19_2 height_m BMI waist



save "Daten 09/s_sa_09heal", replace

log close
exit
