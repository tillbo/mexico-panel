set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datens�tze""
cd $homefolder


**Master do-file**

    	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/criiib_ec_09.do"					//dropping unneeded variabels from chronic disease dataset, summing health expenditures and creating variabel for diab related diseases
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/criiah_09.do"					//household assets
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crc_ls_09.do"					//"Achtung: delimit cr funktioniert nicht, daher do-file manuell ausf�hren"   labeling control book, value lables etc.
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crhhexp09_i_cs.do"				//expenditure dataset(food, general daily use items), creating new variables capturing total expenditures of hh
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crhhexp09_i_cs1.do"				//expenditure dataset (electronics, school), creating new variables capturing total expenditures of hh
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crexpend_hh.do"					//merging expenditure datasets and creating variable with total expenditure of hh for 1 month
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/criiib_ca_09.do"  				//insurance dataset, only labeling and recoding
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crii_se_09.do"					//economic shocks of hhs, u.a. new variable captur. if sick and unemployed in last 5 years
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crs_sa_09.do"					//"Achtung: delimit cr funktioniert nicht, daher do-file manuell ausf�hren"health dataset: creating BMI, labeling and recoding variables
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/criiia_tb_09.do"					//labour dataset, creation of income and workload variables
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/criiib_es_09.do"					//health status
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crii_inr_09.do"					//rural income household
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crii_in_09.do"					//no-labor household income
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/crc_portad_09.do"				//control book, portad dataset containing information on geography and size of communities hhs living in
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/iiib_sm_09.do"					//mental health
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/criiia_ed_09.do"					//education and indigenous
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/criiib_compor_09.do"					// adding date of interview

	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/master_09.do"					//merging 2009/ data
