capture log close
set more off

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiia_tb_09.log", replace

use "Daten 09/iiia_tb_09", clear


save "Daten 09/iiia_tb_09inco", replace

log close
exit
