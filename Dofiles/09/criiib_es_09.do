set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiib_es_09.log", replace
*health dataset--> health status

use"Daten 09/iiib_es_09", clear


keep  es01-es16  es23-ls pid_link

*recoding variables from 1=Yes, 3=No to 1=Yes, 0=No

	foreach var of varlist   es02 es03_1 es04_1 es06 es08_1 es09 es09a es23 {
	replace `var'=0 if `var'==3
	}

*value labels
	label define yesno 0 "no" 1 "yes"
	foreach var of varlist  es02 es03_1 es04_1 es06 es08_1 es09 es09a es23 {
	label values `var' yesno
	}

save "Daten 09/iiib_es_09hlth.dta", replace

log close

exit
