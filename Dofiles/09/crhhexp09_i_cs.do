capture log close
set more off

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/crhhexp09_i_cs.log", replace

use"Daten 09/i_cs_09", clear

*creating new variables capturing total expenditures for food (last 7 days)

	*VEGETABLES AND FRUITS
		egen ex_fruit = rowtotal( cs02a_12 cs02a_22 cs02a_32 cs02a_42 cs02a_52 cs02a_62 cs02a_72 cs02a_82), miss
	*CEREALS AND GRAINS
		egen ex_cg = rowtotal(cs02b_12 cs02b_22 cs02b_32 cs02b_42 cs02b_52), miss
	*MEETS AND ANIMAL ORIGINATED FOOD
		egen ex_meet = rowtotal( cs02c_12 cs02c_22 cs02c_32 cs02c_42 cs02c_52 cs02c_62 cs02c_72 cs02c_82), miss
	*OTHER INDUSTRIALLY-PROCESED FOOD
		egen ex_procfo = rowtotal(cs02d_12 cs02d_22 cs02d_32 cs02d_42 cs02d_52), miss
	*PUBLIC TRANSPORTATION AND OTHER
		egen ex_trans = rowtotal( cs02e_12 cs02e_22 cs02e_32), miss
	*Main sorts of foods Corn Tortillas, Bakery or unpackaged individual baked goods, Chicken, Milanesa steak, Pasteurized Milk, Hen eggs
	*Red tomato, Packaged Beans or in bulk, White sugar, Soft drinks
		egen ex_mainfood = rowtotal( cs15a cs15b cs15c cs15d cs15e cs15f cs15g cs15h cs15i cs15j), miss
	*Sum of all above expenditures
		egen ex_allfood = rowtotal( ex_fruit ex_cg ex_meet ex_procfo ex_trans ex_mainfood), miss

*labeling food variables
		label var ex_fruit "VEGETABLES AND FRUITS"
		label var ex_cg "CEREALS AND GRAINS"
		label var ex_meet "MEETS AND ANIMAL ORIGINATED FOOD"
		label var ex_procfo "OTHER INDUSTRIALLY-PROCESED FOOD"
		label var ex_trans "PUBLIC TRANSPORTATION AND OTHER"
		label var ex_mainfood "Main sorts of foods Corn Tortillas, etc."
		label var ex_allfood "Sum all food expendit."

*non-food expenditure (personal items(last month)

	egen ex_nonfood = rowtotal( cs16a_2 cs16b_2 cs16c_2 cs16d_2 cs16e_2 cs16f_2 cs16g_2 cs16h_2 cs16i_2), miss
	label var ex_nonfood "Sum non-food expend in pers. items (last month)"


save "Daten 09/hhexp09_i_cs", replace
log close
exit


