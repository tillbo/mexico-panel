set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiib_conpor_09.log", replace
*health dataset--> chronic diseases

use"Daten 09/iiib_conpor", clear

duplicates drop pid_link, force

save "Daten 09/iiib_09conpor.dta", replace

log close

exit
