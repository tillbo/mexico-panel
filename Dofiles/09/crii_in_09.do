capture log close
set more off
global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/crii_in_09.log", replace

use "Daten 09/ii_in_09", clear


**no labor income
/*
	egen incnolabhh=rowtotal( in01a2_2 in01a2_2 in01a3_2 in01a4_2 in01a5_2 in01a6_2 in01a7_2 in01a8_2 in01a9_2 in01a10_21 in01b_2 in01c_2 in01d_2 in01e_2 in01f_2 in01g_2 in01h_2 in01i_2 in01j_2 in01k_21)  //no laber income for total household in last 12 months





**labeling variables
	label var incnolabhh "12 mth no-labor income household"


**keep neede variables

	keep incnolabhh folio
*/
save "Daten 09/ii_in_09nolabinc", replace
log close
exit
