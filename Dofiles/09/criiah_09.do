capture log close
set more off
global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiah.log", replace

use "Daten 09/ii_ah.dta", clear

gen owns_house = ah03a
replace owns_house = 0 if ah03a == 3

gen owns_extrahouse = ah03b

replace owns_extrahouse = 0 if ah03b == 3


gen owns_vehicle = ah03b

replace owns_vehicle = 0 if ah03d == 3

gen owns_finance = ah03h

replace owns_finance = 0 if ah03d == 3


save "Daten 09/ii_ah_hhassets09", replace

log close
exit
