set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiib_ec_09.log", replace
*health dataset--> chronic diseases

use"Daten 09/iiib_ec_09", clear

*recoding variables from 1=Yes, 3=No to 1=Yes, 0=No

	foreach var of varlist  ec01a-ec02i {
	replace `var'=0 if `var'==3
	}

*value labels
	label define yesno 0 "no" 1 "yes"
	foreach var of varlist ec01a-ec02i {
	label values `var' yesno
	}

*generating variables that capture how patient is affected by chronic diseases
	gen chronic=0

	replace chronic=1 if ec01a==1 | ec01b==1 | ec01c==1 		//1 if has either diabetes, hypertension or heart disease
	replace chronic=2 if ec01a==1 & ec01c==1 & ec01c==0			//2 if has 2 of the above diseases
	replace chronic=2 if ec01a==1 & ec01b==1 & ec01c==0			//2 if has 2 of the above diseases
	replace chronic=2 if ec01a==1 & ec01c==1 & ec01b==0			//2 if has 2 of the above diseases
	replace chronic=2 if ec01a==0 & ec01c==1 & ec01b==1			//2 if has 2 of the above diseases
	replace chronic=3 if ec01a==1 & ec01b==1 & ec01c==1			//3 if has all of the above diseases

	label var chronic "diabetes and related chronic disease"

*generating variabel that captures if has any of diab. related diseases
	gen chronic_diab=0
	foreach var of varlist ec01a-ec01c {
	replace chronic_diab=1 if `var'==1
	}
	label var chronic_diab "=1 if has diab, high pressure, heart disease"

*generating variable that captures if has any chronic disease
	gen chronic_any=0
	foreach var of varlist ec01a-ec01i_1{
	replace chronic_any=1 if `var'==1
	label var chronic_any "=1 if has any chronic disease"
	}

/*generating variable that captures expenditure for disease
	replace  ec02a=0 if ec03a==0
	replace  ec02b=0 if ec03b==0
	replace  ec02c=0 if ec03c==0
	replace  ec02d=0 if ec03d==0
	replace  ec02e=0 if ec03e==0
	replace  ec02f=0 if ec03f==0
	replace  ec02g=0 if ec03g==0
	replace  ec02h=0 if ec03h==0
	replace  ec02i=0 if ec03i==0


	foreach var of varlist  ec03a-ec03i{
	replace `var'=. if `var'==0
	} 
*/
	*egen exp_chr=rowtotal( ec03a ec03b ec03c), miss

	*label var exp_chr "expenditures for diab, hypert., heart disease"

*generating variable that captures all expenditures for chronic diseases
*	egen exp_allchr=rowtotal (ec03a-ec03i), miss
*	label var exp_allchr "all expenditures chronic diseases"


	#delimit ;

	label define chronic
	0 "no diabetes rel. disease"
	1 "diab|hypert.|heart disease"
	2 "combination of 2 diseases"
	3 "has all 3 diseases";

	label define chronic_diab
	0 "No"
	1 "Yes";

	label define chronic_any
	0 "No"
	1 "Yes";

	label values chronic chronic;
	label values chronic_diab chronic_diab;
	label values chronic_any chronic_any;



	#delimit cr


save "Daten 09/iiib_ec_09chro.dta", replace

log close

exit
