/*ROBUSTNESS CHECKS FOR BIOMARKER DATA*/

clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""



cd $homefolder
**log-file starten**
log using Logs/biomarker_robustness.log, replace


		use "Paneldaten/final.dta", clear

************************************************************************************************************************
/*********************************BIOMARKER ANALYSIS*******************************************************************/
************************************************************************************************************************


global wage1 "age age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.secondary i.highschool i.college_uni i.married wealth i.indigenous i.worktype i.survey_year insurance i.health_sr"  // no diabetes included to be used with years since diagnosis

global controls "age age_sq i.smallcity i.city i.bigcity i.primary i.secondary i.highschool i.college_uni i.married kids_hh i.indigenous wealth i.health_sr"

global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco Morelos Michoacn NuevoLen Oaxaca Puebla Sinaloa Yucatn Veracruz"


************************************************************************
/*Effect of self-reported diabetes in whole wave 3, not only subsample*/
************************************************************************

eststo works_diab_bio: reg works $controls $states diabetes sex if t==2, ro

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_bio`sex': reg works $controls $states diabetes if sex== `sex' & t==2, ro
}	


/*First I estimate a model for comparison only using self-reported diabetes for the subsample*/

	/*EMPLOYMENT*/
eststo works_diab_bio: reg works $controls $states diabetes sex if t==2 & bio ==1, ro
eststo works_diab_bio_fe: xtreg works $controls $states diabetes sex if t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

//eststo works_diab_all: estpost margins, dydx(*)

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_bio`sex': reg works $controls $states diabetes if sex== `sex' & t==2 & bio ==1, ro
eststo works_diab_bio`sex'_fe: xtreg works $controls $states diabetes if sex== `sex' & t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

}

	/*WAGES AND WORKING HOURS*/

foreach var of varlist $Y1{
eststo `var'diab_bio: reg `var' $wage1 sex diabetes if t==2 & bio ==1, ro
eststo `var'diab_bio_fe: xtreg `var' $wage1 sex diabetes if t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_bio`sex': reg `var' $wage1 diabetes if sex== `sex' & t==2 & bio ==1, cluster(folio)
eststo `var'diab_bio`sex'_fe: xtreg `var' $wage1 diabetes if sex== `sex' & t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

	}
}

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2 N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none)  /// label coeflables(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" ///
	 "Other control variables:age, age squared, state dummies, urbanisation dummies, education dummies, married dummy, number children < 6 and wealth" ///
	 "Calender year dummies are included as data collection for the third wave was streched out over several years" ///
	 "The wage and working hour models additionally control for type of work (agricultural and self employed with non-agricultural employment as the base)" "and for health insurance status")


esttab works_diab_bio works_diab_bio0 works_diab_bio1 lnwage_hrdiab_bio lnwage_hrdiab_bio0 lnwage_hrdiab_bio1 workhrsdiab_bio workhrsdiab_bio0 workhrsdiab_bio1 using labouroutcomes_diabetes_bio.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diabetes) label nobase
       
esttab works_diab_bio_fe works_diab_bio0_fe works_diab_bio1_fe lnwage_hrdiab_bio_fe lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe workhrsdiab_bio_fe workhrsdiab_bio0_fe workhrsdiab_bio1_fe using labouroutcomes_diabetes_bio_fe.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diabetes) label nobase
       

/*EVERYBODY WITH AN HBA1C > 6.4 */

replace diab_hba1c = 1 if diab_controlled == 1
	/*EMPLOYMENT*/
eststo works_diab_all: reg works $controls $states diab_hba1c sex if t==2 & bio ==1, ro
eststo works_diab_all_fe: xtreg works $controls $states diab_hba1c sex if t==2 & bio ==1, fe i(loc_id)

//eststo works_diab_all: estpost margins, dydx(*)

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_all`sex': reg works $controls $states diab_hba1c if sex== `sex' & t==2 & bio ==1, ro
eststo works_diab_all`sex'_fe: xtreg works $controls $states diab_hba1c if sex== `sex' & t==2 & bio ==1, fe i(loc_id)
}

	/*WAGES AND WORKING HOURS*/

foreach var of varlist $Y1{
eststo `var'diab_all: reg `var' $wage1 sex diab_hba1c if t==2 & bio ==1, ro
eststo `var'diab_all_fe: xtreg `var' $wage1 sex diab_hba1c if t==2 & bio ==1, i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_all`sex': reg `var' $wage1 diab_hba1c if sex== `sex' & t==2 & bio ==1, cluster(folio)
eststo `var'diab_all`sex'_fe: xtreg `var' $wage1 diab_hba1c if sex== `sex' & t==2 & bio ==1, i(loc_id) fe
	}
}


esttab works_diab_all works_diab_all0 works_diab_all1 lnwage_hrdiab_all lnwage_hrdiab_all0 lnwage_hrdiab_all1 workhrsdiab_all workhrsdiab_all0 workhrsdiab_all1 using labouroutcomes_diabetes_hba1c.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diab_hba1c) label nobase

esttab works_diab_all_fe works_diab_all0_fe works_diab_all1_fe lnwage_hrdiab_all_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe workhrsdiab_all_fe workhrsdiab_all0_fe workhrsdiab_all1_fe using labouroutcomes_diabetes_hba1c_fe.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diab_hba1c) label nobase

/*SELF-REPORTED DIABETES AND UNDIAGNOSED DIABETES IN ONE SPECIFICATION*/


	/*EMPLOYMENT*/
eststo works_diab_ud: reg works $controls $states sex diabetes diab_ud if t==2 & bio ==1, ro
eststo works_diab_ud_fe: xtreg works $controls $states sex diabetes diab_ud if t==2 & bio ==1, i(loc_id) cluster(loc_id) fe

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_ud`sex': reg works $controls $states diabetes diab_ud obese if sex== `sex' & t==2 & bio ==1, ro
eststo works_diab_ud`sex'_fe: xtreg works $controls diabetes diab_ud insurance if sex== `sex' & t==2 & bio ==1, i(loc_id) cluster(loc_id) fe 	
}



	/*WAGES AND WORKING HOURS*/
foreach var of varlist $Y1{
eststo `var'diab_ud: reg `var' $wage1 $states sex diabetes diab_ud if t==2 & bio ==1, cluster(folio)
eststo `var'diab_ud_fe: xtreg `var' $wage1 $states  sex diabetes diab_ud if t==2 & bio ==1, i(loc_id) cluster(loc_id) fe 	

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud`sex': reg `var' $wage1 $states  diabetes diab_ud if sex== `sex' & t==2 & bio ==1, cluster(folio)
eststo `var'diab_ud`sex'_fe: xtreg `var' $wage1 $states diabetes diab_ud if sex== `sex' & t==2 & bio ==1, i(loc_id) cluster(loc_id) fe 

	}
}


esttab works_diab_ud0 works_diab_ud1 lnwage_hrdiab_ud0 lnwage_hrdiab_ud1 workhrsdiab_ud0 workhrsdiab_ud1 using labouroutcomes_diagnosed_undiagnosed_robust.tex, replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diabetes diab_ud *.health_sr) label nobase

esttab works_diab_ud0_fe works_diab_ud1_fe lnwage_hrdiab_ud0_fe lnwage_hrdiab_ud1_fe workhrsdiab_ud0_fe workhrsdiab_ud1_fe using labouroutcomes_diagnosed_undiagnosed_fe_robust.tex, replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 1 0 1 0 )    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diabetes diab_ud *.health_sr) label nobase

// have 1 big table per dependent variable including all estimates
esttab works_diab_bio works_diab_bio0 works_diab_bio1 works_diab_all works_diab_all0 works_diab_all works_diab_ud works_diab_ud0 works_diab_ud1 ///
works_diab_bio_fe works_diab_bio0_fe works_diab_bio1_fe works_diab_all_fe works_diab_all0_fe works_diab_all_fe works_diab_ud_fe works_diab_ud0_fe works_diab_ud1_fe ///
 using labouroutcomes_all_biomarkermodels_employment.tex, replace comp nogaps ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Only self-reported" "HbA1c \geq 6.5\%" "Self-reported and undiagnosed" "Only self-reported (FE)" "HbA1c \geq 6.5\% (FE)" "Self-reported and undiagnosed (FE)", pattern(1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diabetes diab_ud diab_hba1c) label  
       
esttab lnwage_hrdiab_bio lnwage_hrdiab_bio0 lnwage_hrdiab_bio1 lnwage_hrdiab_all lnwage_hrdiab_all0 lnwage_hrdiab_all lnwage_hrdiab_ud lnwage_hrdiab_ud0 lnwage_hrdiab_ud1 ///
lnwage_hrdiab_bio_fe lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe lnwage_hrdiab_all_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all_fe lnwage_hrdiab_ud_fe lnwage_hrdiab_ud0_fe lnwage_hrdiab_ud1_fe ///
 using labouroutcomes_all_biomarkermodels_wage.tex, replace comp nogaps ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Only self-reported" "HbA1c \geq 6.5\%" "Self-reported and undiagnosed" "Only self-reported (FE)" "HbA1c \geq 6.5\% (FE)" "Self-reported and undiagnosed (FE)", pattern(1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diabetes diab_ud diab_hba1c) label  fragment nogaps
       
esttab workhrsdiab_bio workhrsdiab_bio0 workhrsdiab_bio1 workhrsdiab_all workhrsdiab_all0 workhrsdiab_all workhrsdiab_ud workhrsdiab_ud0 workhrsdiab_ud1 ///
workhrsdiab_bio_fe workhrsdiab_bio0_fe workhrsdiab_bio1_fe workhrsdiab_all_fe workhrsdiab_all0_fe workhrsdiab_all_fe workhrsdiab_ud_fe workhrsdiab_ud0_fe workhrsdiab_ud1_fe ///
 using labouroutcomes_all_biomarkermodels_hours.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Only self-reported" "HbA1c \geq 6.5\%" "Self-reported and undiagnosed" "Only self-reported (FE)" "HbA1c \geq 6.5\% (FE)" "Self-reported and undiagnosed (FE)", pattern(1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diabetes diab_ud diab_hba1c) label fragment nogaps


*++++++Interaction terms: Just playing around+++++++*

global controls "age age_sq i.smallcity i.city i.bigcity i.primary i.secondary i.highschool i.college_uni i.married kids_hh i.indigenous wealth i.survey_year diab_father diab_mother"

forvalues sex=0/1{
display `sex' `diab'
reg works $controls $states i.diabetes##ib(first).health_sr i.diab_ud##ib(first).health_sr if sex== `sex' & t==2 & bio ==1, ro
xtreg works $controls $states i.diabetes##ib(first).health_sr i.diab_ud##ib(first).health_sr if sex== `sex' & t==2 & bio ==1, i(loc_id) cluster(loc_id) fe 	
}
