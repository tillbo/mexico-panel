/*DO-FILE WITH ALL MODELS THAT WILL APPEAR IN FINAL PAPER*/

clear matrix
set more off, perm
capture log close

global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""

global table "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Tables/Robustness/Log"
global estimates "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Estimates"


cd $homefolder
**log-file starten**


		use "Paneldaten/final.dta", clear

log using Logs/panel_wb_robustness.log, replace




	/*PANEL DATA*/
global wage "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous i.worktype i.insurance i.survey_year"
global wage_nf "age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous insurance survey_year2-survey_year7 selfemployed agricultural"

global novariation "indigenous BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco Morelos Michoacn NuevoLen Oaxaca Puebla Sinaloa Yucatn Veracruz"
global controls "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh i.indigenous wealth i.survey_year"
global controls_nf "age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth survey_year2-survey_year7"

global Y1 "lnwage_hr workhrs"




 // SELF-REPORTED DIABETES WITH FIXED EFFECTS



recode ec02a  (1=2) (0=1) (.=0) if diabetes <., gen(diab_anytreat)


gen diab_treat_trans = 0 if diabetes <.
replace diab_treat_trans = 1 if diab_anytreat
bysort sex: eststo worksFE: xtreg works $controls i.ec01a $novariation, re ro  // pooled fixed effects

bysort sex: eststo worksFE: xtreg works $controls i.diab_anytreat  $novariation sex, fe cluster(pid_link)  // pooled fixed effects
	*Employment 

*First estimating main model using mundlak non-linear specification
	mundlak works age $controls_nf diabetes $novariation, use(diabetes $controls_nf) keep hybrid

xtlogit works *__* if sex ==0 , vce(cluster pid_link)
margins, dydx(*__diabetes)

xtlogit works *__* if sex ==1 , vce(cluster pid_link)
margins, dydx(*__diabetes)

drop *__*
*Results very similar to LPM, but take much longer to estimate


/*USING LOG LINK INSTEAD OF LINEAR REGRESSION FOR BINARY OUTCOMES*/

forvalues sex=0/1{ // by gender 
display `sex'
eststo worksWB`sex'_log: xthybrid_keep works age $controls_nf diabetes $novariation if sex== `sex', clusterid(pid_link) use(diabetes $controls_nf) vce(cluster pid_link) full family(binomial) link(logit)
test B_diabetes=W_diabetes
estadd scalar p_wb = r(p)
eststo worksWB`sex'_log_m: estpost margins, dydx(*_diabetes)
rename R_* *
drop W_* B_*
}
forvalues sex=0/1{ // by gender and diabetes type
display `sex'
eststo worksWB`sex'_t_log: xthybrid_keep works age $controls_nf type1 type2 $novariation if sex== `sex', clusterid(pid_link) use(type1 type2 $controls_nf) vce(cluster pid_link) full family(binomial) link(logit)
test B_type1=W_type1
estadd scalar p_wb1 = r(p)
test B_type2=W_type2
estadd scalar p_wb2 = r(p)
eststo worksWB`sex'_t_log_m: estpost margins, dydx(*_type*)
rename R_* *
drop W_* B_*
}



	
	
	*Interaction term between diabetes and worktype*


estwrite *log using "$estimates/fe_results", id() append
estread * using "$estimates/fe_results", id()

	*TABLES FOR SELF-REPORTED DIABETES WITH FIXED EFFECTS

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast   ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*diabetes) nobaselevels  coeflabels(1.diabetes "Diabetes" diabetes "Diabetes" ///
	 W_diabetes "Diabetes (within)" B_diabetes "Diabetes (between)") 

	*table for simple self-reported fixed effects model

esttab worksWB0_log worksWB1_log workhrsWB0 workhrsWB1 lnwage_hrWB0 lnwage_hrWB1 using "$table/WBlabouroutcomes_log.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(p_wb N, fmt(%9.3f  %9.0f ) labels("Within=Between (p-value)" "N")) eform(1 1 0 0 0 0)

esttab worksWB0_log_m worksWB1_log_m workhrsWB0 workhrsWB1 lnwage_hrWB0 lnwage_hrWB1 using "$table/WBlabouroutcomes_log_m.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(p_wb N, fmt(%9.3f  %9.0f ) labels("Within=Between (p-value)" "N"))


/*BY DIABETES TYPE*/

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*type*) nobaselevels  coeflabels(type1 "Type 1" type2 "Type 2" ///
	 W_type1 "Early onset (within)" B_type1 "Early onset (between)" ///
	 W_type2 "Late onset (within)" B_type2 "Late onset (between)" ) 


esttab worksWB0_t_log worksWB1_t_log  workhrsWB0_t workhrsWB1_t lnwage_hrWB0_t lnwage_hrWB1_t using "$table/WBlabouroutcomes_t_log.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(p_wb1 p_wb2 N, fmt(%9.3f  %9.0f ) labels("Within=Between (p-value)" "N")) eform(1 1 0 0 0 0)

esttab worksWB0_t_log_m worksWB1_t_log_m workhrsWB0_t workhrsWB1_t lnwage_hrWB0_t lnwage_hrWB1_t using "$table/WBlabouroutcomes_t_log_m.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(p_wb1 p_wb2 N, fmt(%9.3f  %9.0f ) labels("Within=Between (p-value)" "N"))

       

/*SELECTION INTO DIFFERENT TYPES OF WORK*/

	*SIMPLE MODELS USING XTREG AND STATUS DUMMIES
	

forvalues sex=0/1{

eststo worksWB_employed`sex'_log: xthybrid_keep status2 age $controls_nf diabetes $novariation if sex == `sex', clusterid(pid_link) test use(diabetes $controls_nf) vce(cluster pid_link) full family(binomial) link(logit)  // 
test B_diabetes=W_diabetes
estadd scalar p_wb = r(p)
eststo worksWB_employed`sex'_log_m: estpost margins, dydx(*_diabetes)
rename R_* *
drop W_* B_*

eststo worksWB_agri`sex'_log: xthybrid_keep status3 age $controls_nf diabetes $novariation if sex == `sex', clusterid(pid_link) test use(diabetes $controls_nf) vce(cluster pid_link) full family(binomial) link(logit) // 
test B_diabetes=W_diabetes
estadd scalar p_wb = r(p)
eststo worksWB_agri`sex'_log_m: estpost margins, dydx(*_diabetes)
rename R_* *
drop W_* B_*

eststo worksWB_selfempl`sex'_log: xthybrid_keep status4 age $controls_nf diabetes $novariation if sex == `sex', clusterid(pid_link) test use(diabetes $controls_nf) vce(cluster pid_link) full family(binomial) link(logit) // 
test B_diabetes=W_diabetes
estadd scalar p_wb = r(p)
eststo worksWB_selfempl`sex'_log_m: estpost margins, dydx(*_diabetes)
rename R_* *
drop W_* B_*
}
forvalues sex=1/1{
eststo worksWB_empl`sex'_t_log: xthybrid_keep status2 age $controls_nf type* $novariation if sex == `sex', clusterid(pid_link) test use(type* $controls_nf) vce(cluster pid_link) full family(binomial) link(logit)  
test B_type1=W_type1
estadd scalar p_wb1 = r(p)
test B_type2=W_type2
estadd scalar p_wb2 = r(p)
eststo worksWB_empl`sex'_t_log_m: estpost margins, dydx(*_type*)
rename R_* *
drop W_* B_* 

eststo worksWB_agri`sex'_t_log: xthybrid_keep status3 age $controls_nf type* $novariation if sex == `sex', meglmopts(iterate(20)) clusterid(pid_link) test iterations use(type*  $controls_nf) vce(cluster pid_link) full family(binomial) link(logit) 
test B_type1=W_type1
estadd scalar p_wb1 = r(p)
test B_type2=W_type2
estadd scalar p_wb2 = r(p)
eststo worksWB_agri`sex'_t_log_m: estpost margins, dydx(*_type*)
rename R_* *
drop W_* B_*


eststo worksWB_selfempl`sex'_t_log: xthybrid_keep status4 age $controls_nf type* $novariation if sex == `sex', clusterid(pid_link) iterations test use(type*  $controls_nf) vce(cluster pid_link) full family(binomial) link(logit) 
test B_type1=W_type1
estadd scalar p_wb1 = r(p)
test B_type2=W_type2
estadd scalar p_wb2 = r(p)
eststo worksWB_selfempl`sex'_t_log_m: estpost margins, dydx(*_type*)
rename R_* *
drop W_* B_*
}


estwrite *log* using "$estimates/fe_results", id(make) append
estread *log* using "$estimates/fe_results"


	*TABLES FOR SELF-REPORTED DIABETES EFFECT ON EMPLOYMENT BY SECTOR

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast   ///
 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*diabetes) nobaselevels  coeflabels(1.diabetes "Diabetes" diabetes "Diabetes" type1 "Type 1" type2 "Type 2" ///
 W_diabetes "Diabetes (within)" B_diabetes "Diabetes (between)") 
	 

* WITHIN BETWEEN
esttab worksWB_employed0_log worksWB_agri0_log worksWB_selfempl0_log worksWB_employed1_log worksWB_agri1_log worksWB_selfempl1_log using "$table/worktype_WB_log.tex", replace comp ///
 mti("Non-agric." "Agric."  "Self-employed" "Non-agric." "Agric."  "Self-employed" "Non-agric." "Agric." "Self-employed") mgroups(Males Females, pattern(1 0 0 1 0 0)                   ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span})) ${authorea} stats(p_wb N, fmt(%9.3f %9.0f ) labels("Within=Between (p-value)" "N")) eform(1 1 1 1 1 1)
       
esttab worksWB_employed0_log_m worksWB_agri0_log_m worksWB_selfempl0_log_m worksWB_employed1_log_m worksWB_agri1_log_m worksWB_selfempl1_log_m using "$table/worktype_WB_log_m.tex", replace comp ///
 mti("Non-agric." "Agric."  "Self-employed" "Non-agric." "Agric."  "Self-employed" "Non-agric." "Agric." "Self-employed") mgroups(Males Females, pattern(1 0 0 1 0 0)                   ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span})) ${authorea} stats(p_wb N, fmt(%9.3f %9.0f ) labels("Within=Between (p-value)" "N"))


* WITHIN BETWEEN BY DIABETES TYPE



/*BY DIABETES TYPE*/

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*type*) nobaselevels  coeflabels(type1 "Type 1" type2 "Type 2" ///
	 W_type1 "Type 1 (within)" B_type1 "Type 1 (between)" ///
	 W_type2 "Type 2 (within)" B_type2 "Type 2 (between)" ) 


esttab worksWB_empl0_t_log worksWB_empl1_t_log worksWB_agri0_t_log worksWB_agri1_t_log worksWB_selfempl0_t_log worksWB_selfempl1_t_log using "$table/worktype_WB_t_log.tex", replace comp ///
mti( "Males" "Females" "Males" "Females" "Males" "Females")  eform(1 1 1 1 1 1) mgroups( "Non-agric." "Agriculture" "Self-employed", pattern(1 0 1 0 1 0)   ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span})) ${authorea} stats(p_wb1 p_wb2 N, fmt(%9.3f %9.3f  %9.0f ) labels("Type 1: Within=Between (p-value)" "Type 2: Within=Between (p-value)" "N"))

esttab worksWB_empl0_t_log_m worksWB_empl1_t_log_m worksWB_agri0_t_log_m worksWB_agri1_t_log_m worksWB_selfempl0_t_log_m worksWB_selfempl1_t_log_m using "$table/worktype_WB_t_log_m.tex", replace comp ///
mti( "Males" "Females" "Males" "Females" "Males" "Females") mgroups( "Non-agric." "Agriculture" "Self-employed", pattern(1 0 1 0 1 0)   ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span})) ${authorea} stats(p_wb1 p_wb2 N, fmt(%9.3f %9.3f  %9.0f ) labels("Type 1: Within=Between (p-value)" "Type 2: Within=Between (p-value)" "N"))
       
log close       
       
       
//  DIABETES DURATION

	*linear specification
	
log using Logs/panel_finalmodels_duration_robustness.log, replace

global controls " age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh i.indigenous wealth i.survey_year "
global controls_nf "age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth survey_year2 - survey_year7"
global fe " age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh indigenous wealth"
global years "survey_year2 - survey_year7"
global wage_nf " age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous insurance survey_year2-survey_year7 selfemployed agricultural"
global wage_fe " age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous insurance selfemployed agricultural"



gen type1_years = type1*years_diabetes
gen type2_years = type2*years_diabetes


forvalues sex=0/1{  // by gender
display `sex'
eststo works_l_WB`sex'_log: xthybrid_keep works $controls_nf years_diabetes age $novariation if sex == `sex', clusterid(pid_link) test use(years_diabetes $controls_nf) vce(cluster pid_link) full family(binomial) link(logit) // 
test B_years_diabetes=W_years_diabetes
estadd scalar p_wb = r(p), replace 
eststo works_l_WB`sex'_log_m: estpost margins, dydx(*_diabetes)
rename R_* *
drop W_* B_*

}

forvalues sex=0/1{  // by gender
display `sex'
eststo works_l_WB`sex'_t_log: xthybrid_keep works $controls_nf age type1_years type2_years $novariation if sex == `sex', clusterid(pid_link) test use(type1_years type2_years $controls_nf) vce(cluster pid_link) full family(binomial) link(logit)  // 
test B_type1_years=W_type1_years
estadd scalar p_wb1 = r(p)
test B_type2_years=W_type2_years
estadd scalar p_wb2 = r(p)
eststo works_l_WB`sex'_t_log_m: estpost margins, dydx(*type*)
rename R_* *
drop W_* B_*
}   



estwrite *log* using "$estimates/duration_WB", id() append
estread *log_m using "$estimates/duration_WB", id()


	*using splines
	
		*employment
  
 * do not estimate splines for type 1 because too few observations

foreach var of varlist diab_y1-diab_y4{  // diabetes splines by type
gen type1_`var' = `var'*type1
gen type2_`var' = `var'*type2
}


forvalues sex=0/1{  // by gender
display `sex'

eststo w_sp_WB`sex'_log: xthybrid_keep works age $controls_nf diab_y* $novariation if sex == `sex', clusterid(pid_link) use(diab_y* $controls_nf) vce(cluster pid_link) full  family(binomial) link(logit)
	
	test B_diab_y1=W_diab_y1
	foreach var in y2 y3 y4{
	test B_diab_`var'=W_diab_`var', accu
	}
	estadd scalar p_wb = r(p)
eststo w_sp_WB`sex'_log_m: estpost margins, dydx(*diab_y*)
rename R_* *
drop W_* B_*
	
}

/*
forvalues sex=0/1{  // by gender and type
display `sex'	
eststo w_sp_WB`sex'_t_log: xthybrid works age $controls_nf type1_diab_y* type2_diab_y* $novariation if sex == `sex', clusterid(pid_link) use(type2_diab_y* $controls_nf) vce(cluster pid_link) full family(binomial) link(logit) 
	foreach var in y1 y2 y3 y4{
	test B_type2_diab_`var'=W_type2_diab_`var'
	}
	estadd scalar p_wb = r(p)
}
*/

estwrite *log using "$estimates/duration_WB", id() append


// using dummies

foreach var of varlist years_diabetes_groups12-years_diabetes_groups15{
gen t2`var' = `var'*type2
}



forvalues sex=0/1{  // by gender
	display `sex'
	eststo w_dumm_WB`sex'_log: xthybrid_keep works age $controls_nf years_diabetes_groups12-years_diabetes_groups15  $novariation if sex == `sex', clusterid(pid_link) test use(years_diabetes_groups12-years_diabetes_groups15 $controls_nf) vce(cluster pid_link) full  family(binomial) link(logit) 
	test B_years_diabetes_groups12=W_years_diabetes_groups12
		foreach var in  13 14 15{
		test B_years_diabetes_groups`var'=W_years_diabetes_groups`var', accum
		}
		estadd scalar p_wb = r(p)
		eststo w_dumm_WB`sex'_log_m: estpost margins, dydx(*diab_y*)
	
rename R_* *
drop W_* B_*		

}

/*				
forvalues sex=0/1{  // by gender
	display `sex'
	eststo w_dumm_WB`sex'_t_log: xthybrid works age $controls_nf t2years_diabetes_g*  $novariation if sex == `sex', clusterid(pid_link) test use(t2years_diabetes_g*  $controls_nf) vce(cluster pid_link) full family(binomial) link(logit)
		test B_t2years_diabetes_groups12=W_t2years_diabetes_groups12
		foreach var in 13 14 15{
		test B_t2years_diabetes_groups`var'=W_t2years_diabetes_groups`var', accum
		}
		estadd scalar p_wb = r(p)
		
}
*/


estwrite *log using "$estimates/duration_WB", id() append
estread *dumm* using "$estimates/duration_WB"
estread *_l* using "$estimates/duration_WB"
estread *sp* using "$estimates/duration_WB"
estread *log* using "$estimates/duration_WB"



	*TABLES FOR DIABETES DURATION WITH WITHIN-BETWEEN MODEL

 global authoreal b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast eform(1 1 0 0 0 0) ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  ///
       coeflabels(years_diabetes "Years since diagnosis" W_years_diabetes "Years since diagnosis (within)" B_years_diabetes "Years since diagnosis (between)") 

	*table for simple self-reported fixed effects model

esttab works_l_WB0_log works_l_WB1_log workhrs_l_WB0 workhrs_l_WB1 lnwage_hr_l_WB0 lnwage_hr_l_WB1 using "$table/WBlabouroutcomes_duration_log.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal} stats(p_wb N, fmt(%9.3f  %9.0f ) labels("Within=Between (p-value)" "N")) keep(*years_diabetes)

esttab works_l_WB0_log_m works_l_WB1_log_m workhrs_l_WB0 workhrs_l_WB1 lnwage_hr_l_WB0 lnwage_hr_l_WB1 using "$table/WBlabouroutcomes_duration_log_m.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal} stats(p_wb N, fmt(%9.3f  %9.0f ) labels("Within=Between (p-value)" "N")) keep(*years_diabetes)



*Splines
esttab w_sp_WB0_log w_sp_WB1_log workhrs_sp_WB0 workhrs_sp_WB1 lnwage_hr_sp_WB0 lnwage_hr_sp_WB1 ///
 using "$table/duration_WB_sp_log.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log Weekly wages", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal}  keep(*diab_y*) stats(p_wb N, fmt(%9.3f  %9.0f ) labels("Within=Between (p-value)" "N")) refcat(W_diab_y1 "Within-effects" B_diab_y1 "Between effects", nolabel)
                   
              
       
*Dummies
esttab w_dumm_WB0_log w_dumm_WB1_log workhrs_dumm_WB0 workhrs_dumm_WB1 lnwage_hr_dumm_WB0 lnwage_hr_dumm_WB1 ///
 using "$table/duration_WB_dumm_log.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log Weekly wages" , pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal}  keep(*years_diabetes_groups*) stats(p_wb N, fmt(%9.3f  %9.0f ) labels("Within=Between (p-value)" "N")) refcat(W_years_diabetes_groups12 "Within-effects" B_years_diabetes_groups12 "Between effects", nolabel)
                   
       
       
/*
************************************************************************************************************************
/*********************************BIOMARKER ANALYSIS*******************************************************************/
************************************************************************************************************************
clear matrix
set more off, perm
capture log close

global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""

global table "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Tables"
global estimates "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Estimates"


cd $homefolder

		use "Paneldaten/final.dta", clear
		
log using Logs/biomarkers_wb.log, replace
 		

global wage1 "age age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.secondary i.highschool i.college_uni i.married wealth i.indigenous i.insurance i.worktype i.survey_year "  // no diabetes included to be used with years since diagnosis

global controls "age age_sq i.smallcity i.city i.bigcity i.primary i.secondary i.highschool i.college_uni i.married kids_hh i.indigenous i.insurance wealth i.survey_year"

global Y1 "lnwage_hr workhrs"

global controls_nf "age age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh indigenous wealth survey_year2-survey_year7"



************************************************************************
/*Effect of self-reported diabetes in whole wave 3, not only subsample*/
************************************************************************

recode ec02aa_1 (.=0) if ec01a <. & t == 2, gen(diab_treat)  // diabetes treatment type
eststo works_diab_total: xtreg works $controls $states i.diab_treat sex if t==2, fe i(loc_id) cluster(loc_id) 
bysort sex: xtreg works $controls $states i.diab_treat if t==2, fe i(loc_id) cluster(loc_id) 

bysort sex: xtreg hba1c $controls $states i.diab_treat age_diagnosis if t==2 & ec01a ==1 & age_diagnosis > 0, fe i(loc_id) cluster(loc_id) 


eststo works_diab_total: reg works $controls $states i.diab_treat sex if t==2, ro
eststo works_diab_total: logit works $controls $states i.ec01a i.health_sr sex if t==2, ro

eststo works_diab_total_fe: xtreg works $controls $states i.diabetes sex if t==2, fe i(loc_id) cluster(loc_id)

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_total`sex': reg works $controls $states i.diabetes if sex== `sex' & t==2, ro
eststo works_diab_total`sex'_fe: xtreg works $controls $states i.diabetes if sex== `sex' & t==2, fe i(loc_id) cluster(loc_id)

}	

foreach var of varlist $Y1{
eststo `var'diab_total: reg `var' $wage1 sex i.diabetes if t==2, ro
eststo `var'diab_total_fe: xtreg `var' $wage1 sex i.diabetes if t==2, fe i(loc_id) cluster(loc_id)

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_total`sex': reg `var' $wage1 i.diabetes if sex== `sex' & t==2, cluster(folio)
eststo `var'diab_total`sex'_fe: xtreg `var' $wage1 i.diabetes if sex== `sex' & t==2, fe i(loc_id) cluster(loc_id)

	}
}

 global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(N, fmt(%9.0f) labels("N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) nonotes  /// label coeflabels(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" ///
	 "Other control variables:age, age squared, state dummies, urbanisation dummies, education dummies, married dummy, number children < 6 and wealth" ///
	 "Calender year dummies are included as data collection for the third wave was streched out over several years" ///
	 "The wage and working hour models additionally control for type of work (agricultural and self employed with non-agricultural employment as the base)" "and for health insurance status")


esttab works_diab_total0 works_diab_total1 lnwage_hrdiab_total0 lnwage_hrdiab_total1 workhrsdiab_total0 workhrsdiab_total1 using "$table/labouroutcomes_diabetes_total.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diabetes) label nobase
       
esttab works_diab_total0_fe works_diab_total1_fe lnwage_hrdiab_total0_fe lnwage_hrdiab_total1_fe workhrsdiab_total0_fe workhrsdiab_total1_fe using "$table/labouroutcomes_diabetes_total_fe.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diabetes) label nobase
       




/*First I estimate a model for comparison only using self-reported diabetes for the subsample*/

	/*EMPLOYMENT*/
eststo works_diab_bio: reg works $controls $states i.diabetes sex if t==2 & bio ==1, ro
eststo works_diab_bio_fe: xtreg works $controls $states i.diabetes sex if t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

//eststo works_diab_all: estpost margins, dydx(*)

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_bio`sex': reg works $controls $states i.diabetes if sex== `sex' & t==2 & bio ==1, ro
eststo works_diab_bio`sex'_fe: xtreg works $controls $states i.diabetes if sex== `sex' & t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

}
	/*WAGES AND WORKING HOURS*/

foreach var of varlist $Y1{
eststo `var'diab_bio: reg `var' $wage1 sex i.diabetes if t==2 & bio ==1, ro
eststo `var'diab_bio_fe: xtreg `var' $wage1 sex i.diabetes if t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_bio`sex': reg `var' $wage1 i.diabetes if sex== `sex' & t==2 & bio ==1, cluster(folio)
eststo `var'diab_bio`sex'_fe: xtreg `var' $wage1 i.diabetes if sex== `sex' & t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

	}
}

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01)  booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) nonotes  /// label coeflabels(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" ///
	 "Other control variables:age, age squared, state dummies, urbanisation dummies, education dummies, married dummy, number children < 6 and wealth" ///
	 "Calender year dummies are included as data collection for the third wave was streched out over several years" ///
	 "The wage and working hour models additionally control for type of work (agricultural and self employed with non-agricultural employment as the base)" "and for health insurance status")


esttab works_diab_bio works_diab_bio0 works_diab_bio1 lnwage_hrdiab_bio lnwage_hrdiab_bio0 lnwage_hrdiab_bio1 workhrsdiab_bio workhrsdiab_bio0 workhrsdiab_bio1 using "$table/labouroutcomes_diabetes_bio.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diabetes) label nobase
       
esttab works_diab_bio_fe works_diab_bio0_fe works_diab_bio1_fe lnwage_hrdiab_bio_fe lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe workhrsdiab_bio_fe workhrsdiab_bio0_fe workhrsdiab_bio1_fe using "$table/labouroutcomes_diabetes_bio_fe.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diabetes) label nobase
       

/*EVERYBODY WITH AN HBA1C > 6.4 */

//replace diab_hba1c = 1 if diab_controlled == 1  // normally not commented out, only in this robustness check
	/*EMPLOYMENT*/
eststo works_diab_all: reg works $controls $states i.diab_hba1c sex if t==2 & bio ==1 & diabetes <., ro
eststo works_diab_all_fe: xtreg works $controls $states i.diab_hba1c sex if t==2 & bio ==1 & diabetes <., fe i(loc_id)
gen sample = 1 if e(sample) == 1

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_all`sex': reg works $controls $states i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_all`sex'_fe: xtreg works $controls $states i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


	/*WAGES AND WORKING HOURS*/

foreach var of varlist $Y1{
eststo `var'diab_all: reg `var' $wage1 sex i.diab_hba1c if t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_all_fe: xtreg `var' $wage1 sex i.diab_hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_all`sex': reg `var' $wage1 i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_all`sex'_fe: xtreg `var' $wage1 i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}


esttab works_diab_all works_diab_all0 works_diab_all1 lnwage_hrdiab_all lnwage_hrdiab_all0 lnwage_hrdiab_all1 workhrsdiab_all workhrsdiab_all0 workhrsdiab_all1 using "$table/labouroutcomes_diabetes_hba1c.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab_hba1c) label nobase

esttab works_diab_all_fe works_diab_all0_fe works_diab_all1_fe lnwage_hrdiab_all_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe workhrsdiab_all_fe workhrsdiab_all0_fe workhrsdiab_all1_fe using "$table/labouroutcomes_diabetes_hba1c_fe.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab_hba1c) label nobase

/*HbA1c Diabetes indicator and self-reported diabetes in one specification*/

eststo works_sr_a1c_fe: xtreg works $controls $states i.diab_hba1c i.diabetes sex if t==2 & bio ==1 & diabetes <., fe i(loc_id)
eststo works_sr_a1c_fe_ia: xtreg works $controls $states i.diab_hba1c##i.diabetes sex if t==2 & bio ==1 & diabetes <., fe i(loc_id)
lincom 1.diabetes + 1.diab_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1

estadd scalar skal_sr r(estimate)
estadd scalar skalsd_sr r(se)
lincom 1.diab_hba1c + 1.diab_hba1c#1.diabetes  //effect of biomarker diagnosed diabetes irrespective of diagnosis when controlling for sr false positives
estadd scalar skal_bio r(estimate)
estadd scalar skalsd_bio r(se)

forvalues sex=0/1{
display `sex' `diab'
//eststo works_sr_a1c`sex': reg works $controls $states i.diab_hba1c i.diabetes  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_sr_a1c`sex': reg works $controls $states i.diabetes##i.diab_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_sr_a1c`sex'_fe_ia: xtreg works $controls $states i.diabetes##i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
lincom 1.diabetes + 1.diab_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  works_sr_a1c`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  works_sr_a1c`sex'_fe_ia
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  works_sr_a1c`sex'_fe_ia
}



foreach var of varlist $Y1{
eststo `var'sr_a1c: reg `var' $wage1 sex i.diab_hba1c i.diabetes if t==2 & bio ==1 & diabetes <., ro
eststo `var'sr_a1c_fe: xtreg `var' $wage1 sex i.diab_hba1c i.diabetes if t==2 & bio ==1 & diabetes <., i(loc_id) fe
eststo `var'sr_a1c_fe_ia: xtreg `var' $wage1 sex i.diab_hba1c##i.diabetes if t==2 & bio ==1 & diabetes <., i(loc_id) fe
lincom 1.diabetes + 1.diab_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) : `var'sr_a1c_fe_ia
estadd scalar skalsd_sr r(se) :  `var'sr_a1c_fe_ia
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) : `var'sr_a1c_fe_ia

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'sr_a1c`sex': reg `var' $wage1 i.diab_hba1c i.diabetes if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'sr_a1c`sex'_fe: xtreg `var' $wage1 i.diab_hba1c i.diabetes if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
eststo `var'sr_a1c`sex'_fe_ia: xtreg `var' $wage1 i.diab_hba1c##i.diabetes if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
lincom 1.diabetes + 1.diab_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) : `var'sr_a1c`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  `var'sr_a1c`sex'_fe_ia
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'sr_a1c`sex'_fe_ia
}
}


/*ONLY UNDIAGNOSED*/
       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_ud`sex': reg works $controls $states i.diab_ud  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_ud`sex'_fe: xtreg works $controls $states i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


foreach var of varlist $Y1{
eststo `var'diab_ud: reg `var' $wage1 sex i.diab_ud if t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_ud_fe: xtreg `var' $wage1 sex i.diab_ud if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud`sex': reg `var' $wage1 i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_ud`sex'_fe: xtreg `var' $wage1 i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}

         

/*SELF-REPORTED DIABETES AND UNDIAGNOSED DIABETES IN ONE SPECIFICATION*/


	/*EMPLOYMENT*/
eststo works_diab_ud_sr: reg works $controls $states sex i.diabetes i.diab_ud if t==2 & bio ==1 & diabetes <., ro
eststo works_diab_ud_sr_fe: xtreg works $controls $states sex i.diabetes i.diab_ud if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe

forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_ud_sr`sex': reg works $controls $states i.diabetes i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_ud_sr`sex'_fe: xtreg works $controls $states i.diabetes i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe 	
}




	/*WAGES AND WORKING HOURS*/
foreach var of varlist $Y1{
eststo `var'diab_ud_sr: reg `var' $wage1 $states sex i.diabetes i.diab_ud if t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_ud_sr_fe: xtreg `var' $wage1 $states  sex diabetes diab_ud if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 	

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud_sr`sex': reg `var' $wage1 $states  i.diabetes i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_ud_sr`sex'_fe: xtreg `var' $wage1 $states i.diabetes i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 

	}
}



/*DIABETES SEVERITY USING CONTINUOUS HBA1C VARIABLE INSTEAD OF DUMMIES (Panel D Table 9)*/

gen ud_hba1c = diab_ud*hba1c
gen diab_sr_hba1c = diabetes*hba1c
gen diab_all_hba1c =  diab_hba1c * hba1c

forvalues sex=0/1{
display `sex' `diab'
*eststo works_bio_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_bio_sev`sex'_fe: xtreg works $controls $states i.diabetes  diab_all_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
eststo works_bio_sev`sex'_fe_ia: xtreg works $controls $states i.diabetes##c.diab_all_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
lincom 1.diabetes + c.diab_all_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  works_bio_sev`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  works_bio_sev`sex'_fe_ia
}	



foreach var of varlist $Y1{
eststo `var'bio_sev: reg `var' $wage1 $states sex i.diabetes  diab_all_hba1c if t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_sev_fe: xtreg `var' $wage1 $states  sex i.diabetes diab_all_hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 	
eststo `var'bio_sev_fe_ia: xtreg `var' $wage1 $states  sex i.diabetes##c.diab_all_hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 	
lincom 1.diabetes + c.diab_all_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate):  `var'bio_sev_fe_ia
estadd scalar skalsd_sr r(se) :  `var'bio_sev_fe_ia

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'bio_sev`sex': reg `var' $wage1 $states  i.diabetes diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_sev`sex'_fe_ia: xtreg `var' $wage1 $states i.diabetes##c.diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 
lincom 1.diabetes + c.diab_all_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'bio_sev`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  `var'bio_sev`sex'_fe_ia

	}
}



//Diabetes and other disease indicators (Panel B Table 10)
eststo works_biochronic_fe_ia: xtreg works $controls $states i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease sex  if t==2 & bio ==1 & diabetes <., fe i(loc_id)

       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_bio_chronic`sex': reg works $controls $states i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_biochronic`sex'_fe_ia: xtreg works $controls $states i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease  if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  works_biochronic`sex'_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  works_biochronic`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  works_biochronic`sex'_fe_ia

}


foreach var of varlist $Y1{
//eststo `var'_bio_chronic: reg `var' $wage1 sex i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if t==2 & bio ==1 & diabetes <., ro
eststo `var'_biochronic_fe_ia: xtreg `var' $wage1 sex i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease systolic if t==2 & bio ==1 & diabetes <., i(loc_id) fe
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'_biochronic_fe_ia
estadd scalar skalsd_sr r(se) : `var'_biochronic_fe_ia
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'_biochronic_fe_ia

forvalues sex=0/1{
display "`var'"`sex'
//eststo `var'_biochronic`sex': reg `var' $wage1 i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'_biochronic`sex'_fe_ia: xtreg `var' $wage1 i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'_biochronic`sex'_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'_biochronic`sex'_fe_ia
estadd scalar skalsd_sr r(se) : `var'_biochronic`sex'_fe_ia
	}
}
       




       
//Diabetes and self-reported health (Panel C Table 10)

       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_biohealth`sex': reg works $controls $states i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_biohealth`sex'_fe_ia: xtreg works $controls $states i.diab_hba1c##i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) : works_biohealth`sex'_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  works_biohealth`sex'_fe_ia
estadd scalar skalsd_sr r(se) : works_biohealth`sex'_fe_ia

}


foreach var of varlist $Y1{
eststo `var'_biohealth_ia: reg `var' $wage1 sex i.diab_hba1c##i.diabetes i.health_sr if t==2 & bio ==1 & diabetes <., ro
eststo `var'_biohealth_fe_ia: xtreg `var' $wage1 sex i.diab_hba1c##i.diabetes i.health_sr if t==2 & bio ==1 & diabetes <., i(loc_id) fe
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'_biohealth_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'_biohealth_fe_ia
estadd scalar skalsd_sr r(se) : `var'_biohealth_fe_ia

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'_biohealth`sex': reg `var' $wage1 i.diab_hba1c##i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'_biohealth`sex'_fe_ia: xtreg `var' $wage1 i.diab_hba1c##i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'_biohealth`sex'_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'_biohealth`sex'_fe_ia
estadd scalar skalsd_sr r(se) : `var'_biohealth`sex'_fe_ia
	}
}
       
       
       


estwrite * using "$estimates/biomarker_results", id() replace
estread * using "$estimates/biomarker_results"


/*New table*/
 
 
 global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01)  booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) nonotes  /// 
	 addnote("Robust standard errors in parentheses" ///
	 "Other control variables:age, age squared, state dummies, urbanisation dummies, education dummies, married dummy, number children < 6 and wealth" ///
	 "Calender year dummies are included as data collection for the third wave was streched out over several years" ///
	 "The wage and working hour models additionally control for type of work (agricultural and self employed with non-agricultural employment as the base)" "and for health insurance status")


  


esttab works_diab_bio0_fe works_diab_bio1_fe lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe workhrsdiab_bio0_fe workhrsdiab_bio1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", replace comp nogaps ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*) label fragment noobs 
       
esttab works_diab_all0_fe works_diab_all1_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe workhrsdiab_all0_fe workhrsdiab_all1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diab*) label fragment noobs nomtitles nonumbers 
       
esttab works_sr_a1c0_fe_ia works_sr_a1c1_fe_ia	 lnwage_hrsr_a1c0_fe_ia lnwage_hrsr_a1c1_fe_ia workhrssr_a1c0_fe_ia workhrssr_a1c1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diab*) label fragment noobs nomtitles nonumbers ///
      stats(skal_sr skalsd_sr ftest, layout(@ (@) @) labels("Linear Combination: Self-reported" " " "F-test: $\beta_{1}+\beta_{3} = \beta_{2}$") fmt(%9.3f %9.3f %9.3f))
       
esttab works_bio_sev0_fe_ia works_bio_sev1_fe_ia lnwage_hrbio_sev0_fe_ia lnwage_hrbio_sev1_fe_ia workhrsbio_sev0_fe_ia workhrsbio_sev1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
 ${table_reduced}  keep(1.diab* diab_all_hba1c) label fragment nomtitles nonumbers 
 
esttab works_biochronic0_fe_ia works_biochronic1_fe_ia lnwage_hr_biochronic0_fe_ia lnwage_hr_biochronic1_fe_ia workhrs_biochronic0_fe_ia workhrs_biochronic1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diab* overweight obese hypertension heart_disease) label fragment noobs nomtitles nonumbers ///
      stats(skal_sr skalsd_sr ftest, layout(@ (@) @) labels("Linear Combination: Self-reported" " " "F-test: $\beta_{1}+\beta_{3} = \beta_{2}$") fmt(%9.3f %9.3f %9.3f))

esttab works_biohealth0_fe_ia works_biohealth1_fe_ia lnwage_hr_biohealth0_fe_ia lnwage_hr_biohealth1_fe_ia workhrs_biohealth0_fe_ia workhrs_biohealth1_fe_ia  ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
 ${table_reduced}  keep(1.diab* *.health_sr) label fragment nomtitles nonumbers   ///
      stats(skal_sr skalsd_sr ftest, layout(@ (@) @) labels("Linear Combination: Self-reported" " " "F-test: $\beta_{1}+\beta_{3} = \beta_{2}$") fmt(%9.3f %9.3f %9.3f))



/*Old tables*/

esttab works_diab_ud_sr works_diab_ud_sr0 works_diab_ud_sr1 lnwage_hrdiab_ud_sr lnwage_hrdiab_ud_sr0 lnwage_hrdiab_ud_sr1 workhrsdiab_ud_sr workhrsdiab_ud_sr0 workhrsdiab_ud_sr1 using "$table/labouroutcomes_diagnosed_undiagnosed.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*)

esttab works_diab_ud_sr_fe works_diab_ud_sr0_fe works_diab_ud_sr1_fe lnwage_hrdiab_ud_sr_fe lnwage_hrdiab_ud_sr0_fe lnwage_hrdiab_ud_sr1_fe workhrsdiab_ud_sr_fe workhrsdiab_ud_sr0_fe workhrsdiab_ud_sr1_fe using "$table/labouroutcomes_diagnosed_undiagnosed_fe.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(diabetes diab_ud)
       
       
// have 1 big table per dependent variable including all community fixed effects estimates sr_a1c
esttab works_diab_bio0_fe works_diab_bio1_fe works_diab_all0_fe works_diab_all1_fe works_sr_a1c0_fe works_sr_a1c1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_employment_fe.tex", replace comp nogaps ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Self-reported" "A1c" "A1c and self-reported", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*) label nogaps
       
esttab  lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe lnwage_hrsr_a1c0_fe lnwage_hrsr_a1c1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_wage.tex", replace comp nogaps ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Self-reported" "A1c" "A1c and self-reported", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*) label  fragment nogaps
       
esttab workhrsdiab_bio0_fe workhrsdiab_bio1_fe workhrsdiab_all0_fe workhrsdiab_all1_fe workhrssr_a1c0_fe workhrssr_a1c1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_hours.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Self-reported" "A1c" "A1c and self-reported", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*) label  fragment nogaps


label define diabetes 0 "No diabetes diagnosis" 1 "Diabetes diagnosis", replace
label val diabetes diabetes
label define diab_hba1c 0 "HbA1c $< 6.5\%$" 1 "HbA1c $\geq 6.5\%$", replace
label val diab_hba1c diab_hba1c
eststo summary: estpost tab diabetes diab_hba1c if sample ==1, col row
bysort sex: tab diabetes diab_hba1c if sample ==1, col row

esttab summary using "$table/bio_matrix.tex", replace unstack cells("b(label(freq)) pct(label(\%)fmt(2))") booktabs noobs nonumber collabels("{n}" "\%")   ///
 eqlabels("HbA1c $< 6.5\%$" "HbA1c $\geq 6.5\%$", prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) alignment(S S) nolz
 
/*DIABETES SEVERITY USING CONTINUOUS HBA1C VARIABLE INSTEAD OF DUMMIES (Panel A Table 10)*/

gen ud_hba1c = diab_ud*hba1c
gen diab_sr_hba1c = diabetes*hba1c
gen diab_all_hba1c =  diab_hba1c * hba1c

forvalues sex=0/1{
display `sex' `diab'
*eststo works_bio_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_bio_sev`sex'_fe_ia: xtreg works $controls $states i.diabetes##c.diab_all_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
}	



foreach var of varlist $Y1{
eststo `var'bio_sev_ia: reg `var' $wage1 $states sex i.diabetes##c.diab_all_hba1c if t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_sev_fe_ia: xtreg `var' $wage1 $states  sex diabetes##c.diab_all_hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 	

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'bio_sev`sex'_ia: reg `var' $wage1 $states  i.diabetes##c.diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_sev`sex'_fe_ia: xtreg `var' $wage1 $states i.diabetes##c.diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 

	}
}


esttab works_bio_sev0_fe works_bio_sev1_fe lnwage_hrbio_sev0_fe lnwage_hrbio_sev1_fe workhrsbio_sev0_fe workhrsbio_sev1_fe using "$table/labouroutcomes_sev_fe.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log Weekly wages" "Weekly work hours", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* diab_all_hba1c) label 


//Diabetes and other disease indicators (Panel B Table 10)

       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_bio_chronic`sex': reg works $controls $states i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo worksbio_chronic`sex'_fe_ia: xtreg works $controls $states i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease  if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


foreach var of varlist $Y1{
//eststo `var'_bio_chronic: reg `var' $wage1 sex i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if t==2 & bio ==1 & diabetes <., ro
eststo `var'bio_chronic_fe_ia: xtreg `var' $wage1 sex i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease systolic if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
//eststo `var'_bio_chronic`sex': reg `var' $wage1 i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_chronic`sex'_fe_ia: xtreg `var' $wage1 i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}
       
esttab worksbio_chronic0_fe_ia worksbio_chronic1_fe_ia lnwage_hrbio_chronic0_fe_ia lnwage_hrbio_chronic1_fe_ia workhrsbio_chronic0_fe_ia workhrsbio_chronic1_fe_ia using "$table/labouroutcomes_diagnosed_undiagnosed_fe_chronic.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 1 0 1 0 )    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* overweight obese hypertension heart_disease) label nobase


       
//Diabetes and self-reported health (Panel C Table 10)

       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_bio_health`sex': reg works $controls $states i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo worksbio_health`sex'_fe_ia: xtreg works $controls $states i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


foreach var of varlist $Y1{
eststo `var'bio_health: reg `var' $wage1 sex i.diab_hba1c##i.diabetes i.health_sr if t==2 & bio ==1 & diabetes <., ro
eststo `var'bio_health_fe_ia: xtreg `var' $wage1 sex i.diab_hba1c i.diabetes i.health_sr if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'bio_health`sex': reg `var' $wage1 i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_health`sex'_fe_ia: xtreg `var' $wage1 i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}
       
esttab worksbio_health0_fe_ia worksbio_health1_fe_ia lnwage_hrbio_health0_fe_ia lnwage_hrbio_health1_fe_ia workhrsbio_health0_fe_ia workhrsbio_health1_fe_ia using "$table/labouroutcomes_diagnosed_undiagnosed_fe_srhealth.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 1 0 1 0 )    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* *.health_sr) label nobase
       


* Table 10

global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01)  booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) nonotes


  


esttab works_diab_bio0_fe works_diab_bio1_fe lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe workhrsdiab_bio0_fe workhrsdiab_bio1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", replace comp nogaps ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diabetes) label fragment noobs 
       
esttab works_diab_all0_fe works_diab_all1_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe workhrsdiab_all0_fe workhrsdiab_all1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diab_hba1c) label fragment noobs nomtitles nonumbers 
       
esttab works_sr_a1c0_fe_ia works_sr_a1c1_fe_ia	 lnwage_hrsr_a1c0_fe_ia lnwage_hrsr_a1c1_fe_ia workhrssr_a1c0_fe_ia workhrssr_a1c1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diabetes 1.diab_hba1c) label fragment noobs nomtitles nonumbers ///
      stats(skal_sr skalsd_sr skal_bio skalsd_bio, layout(@ (@) @ @) labels("Linear Combination: Self-reported" " " "Linear Combination: Biomarker" " ") fmt(%9.3f %9.3f %9.3f %9.3f))
       
esttab works_bio_sev0_fe_ia works_bio_sev1_fe_ia lnwage_hrbio_sev0_fe_ia lnwage_hrbio_sev1_fe_ia workhrsbio_sev0_fe_ia workhrsbio_sev1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
 ${table_reduced}  keep(1.diabetes diab_all_hba1c) label fragment  noobs nomtitles nonumbers 

esttab worksbio_chronic1_fe_ia worksbio_chronic1_fe_ia lnwage_hrbio_chronic0_fe_ia lnwage_hrbio_chronic1_fe_ia workhrsbio_chronic0_fe_ia workhrsbio_chronic1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diabetes 1.diab_hba1c) label fragment noobs nomtitles nonumbers 
      
esttab worksbio_health0_fe_ia worksbio_health1_fe_ia lnwage_hrbio_health0_fe_ia lnwage_hrbio_health1_fe_ia workhrsbio_health0_fe_ia workhrsbio_health1_fe_ia  ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
 ${table_reduced}  keep(1.diabetes 1.diab_hba1c) label fragment noobs nomtitles nonumbers  





       
// DIABETES SEVERITY FOR DIAGNOSED AND UNDIAGNOSED USING DIABETES DUMMIES BECAUSE USING INTERACTION TERMS DOES NOT WORK AS EVERYBODY WITH HBA1C
// ABOVE 6.4 AND WITHOUT DIAGNOSIS IS AUTOMATICALLY UNDIAGNOSED. HENCE THERE IS NO REFERENCE GROUP OF THOSE WITH UNDIAGNOSED DIABETES BELOW HBA1C 6.4


eststo works_diab_sev: reg works $controls $states sex i.diabetes i.diab_hba1c hba1c if t==2 & bio ==1 & diabetes <., ro
eststo works_diab_sev_fe: xtreg works $controls $states sex i.diabetes i.diab_hba1c hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe


forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_sev`sex'_fe: xtreg works $controls $states i.diabetes i.diab_hba1c i.health_sr hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
}	

	/*WAGES AND WORKING HOURS*/
foreach var of varlist $Y1{
eststo `var'diab_sev: reg `var' $wage1 $states sex i.diabetes i.diab_hba1c hba1c if t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_sev_fe: xtreg `var' $wage1 $states sex i.diabetes i.diab_hba1c hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_sev`sex': reg `var' $wage1 $states i.diabetes i.diab_hba1c hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_sev`sex'_fe: xtreg `var' $wage1 $states i.diabetes i.diab_hba1c hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
	}
}

/*esttab works_diab_sev0 works_diab_sev1 lnwage_hrdiab_sev0 lnwage_hrdiab_sev1 workhrsdiab_sev0 workhrsdiab_sev1 using "$table/labouroutcomes_sev.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log Weekly wages" "Weekly work hours", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* hba1c) label*/
       
esttab works_diab_sev0_fe works_diab_sev1_fe lnwage_hrdiab_sev0_fe lnwage_hrdiab_sev1_fe workhrsdiab_sev0_fe workhrsdiab_sev1_fe using "$table/labouroutcomes_sev_fe.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log Weekly wages" "Weekly work hours", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* hba1c) label







forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
xtreg works $controls $states diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
}	


clonevar diabetes_treat= ec02aa_1
replace diabetes_treat = 0 if diabetes <1 
recode diabetes_treat (3 = 2) (4=3)

gen diabetes_med = 0 if diabetes <.
replace diabetes_med = 1 if ec02a == 0
replace diabetes_med = 2 if ec02a == 1

forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
xtreg works i.diabetes_med $controls $states  if sex== `sex', cluster(pid_link) fe
}	




/*Obesity checks*/

forvalues sex=0/1{
foreach var of varlist lnwage_hr workhrs { // by gender
display `sex'
eststo `var'WB`sex'_obese: xthybrid `var' age $wage_nf $states overweight obese  diabetes if sex== `sex', clusterid(pid_link) test use(diabetes $controls_nf overweight obese) vce(cluster pid_link) full
test B_diabetes=W_diabetes
	}
}
	
forvalues sex=0/1{
foreach var of varlist lnwage_hr workhrs { // by gender
display `sex'
xthybrid `var' age $wage_nf $states diabetes if sex== `sex' & obese <., clusterid(pid_link) test use(diabetes $controls_nf) vce(cluster pid_link) full
test B_diabetes=W_diabetes
	}
}
