clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""



cd $homefolder
**log-file starten**
log using Logs/fixed_effects.log, replace


		use "Paneldaten/final.dta", clear
		
//do "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Dofiles/Panel/mean_generating.do

global Z "age age_sq  i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest  i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous  i.diabetes"

global lg "age age_sq  i.married kids_hh wealth i.diabetes" // 

global nofix "age age_sq wealth smallcity city bigcity kids_hh i.t i.married"
global nofix_nf "age age_sq wealth smallcity city bigcity kids_hh married"


global wage "age_sq i.smallcity i.city i.bigcity i.south_southeast i.central i.westcentr i.northwest i.secondary i.highschool i.college_uni i.married wealth i.indigenous i.worktype i.insurance i.diabetes i.survey_year i.married kids_hh"
global wage1 "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married wealth kids_hh i.indigenous i.worktype i.insurance i.survey_year"  // no diabetes included to be used with years since diagnosis
global wage_clean "age age_sq i.smallcity i.city i.bigcity i.south_southeast i.central i.westcentr i.northwest i.secondary i.highschool i.college_uni i.married i.indigenous i.worktype i.insurance i.diabetes"  // no endogeneous variables



global Z_wealth "age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.secondary i.highschool i.college_uni i.married kids_hh indigenous i.diabetes"

global Y "works"
global Y1 "lnwage_hr workhrs"

global years "i.t"

global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco Morelos Michoacn NuevoLen Oaxaca Puebla Sinaloa Yucatn Veracruz"

global regions "south_southeast northwest central westcentr"

global robust " hypertension heart_disease"

global controls "age_ini i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh  i.indigenous i.survey_year wealth diab_father diab_mother"

global controls_nf "age_ini smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous survey_year2-survey_year9 diab_father diab_mother"

global mundlak_nf "age_ini smallcity city bigcity secondary highschool college_uni indigenous kids_hh married wealth diab_father diab_mother"


global fe "diff__*"
global bw "mean__*"


* OLS

replace workhrs = . if workhrs == 0



  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2_o N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) keep(1.diabetes) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth, parental education")


/*no fixed effects included (dummies) to better understand data*/

// employment
forvalues sex=0/1{
display `sex'
eststo noFE_works`sex': reg works $nofix $years diabetes if sex== `sex', ro cluster(pid_link) // similar coefficient but more significant for males and 8% reduction and more significant for females
}

// wages and workhours

forvalues sex=0/1{
foreach var of varlist $Y1{ // by gender
display `sex'
eststo noFE_`var'`sex': reg `var' $nofix $years diabetes if sex== `sex', ro cluster(pid_link)    // negative effect on female working hours and positive effect on male wages
	}
}



/*now normal OLS regression including dummy fixed effects*/

// employment
eststo worksOLS_pooled: reg works sex $Z $years, cluster(pid_link) // pooled

forvalues sex=0/1{
display `sex'
eststo worksOLS`sex': reg works $Z $years if sex== `sex', cluster(pid_link) // by gender
}

// wages and workhours

forvalues sex=0/1{
foreach var of varlist $Y1{ // by gender
display `sex'
eststo `var'OLS`sex': reg `var' $wage $years if sex== `sex', cluster(pid_link)
	}
}


/*Fixed Effects Panel*/

// employment
//rename works Works


eststo worksFE: xtreg works $controls i.diabetes i.sex $states, fe cluster(pid_link)  //fixed effects
eststo worksRE: xtreg works age_ini $controls i.diabetes sex $states, re cluster(pid_link)  //random effects
//xtoverid, cluster(pid_link)

mundlak works $mundlak_nf age_sq survey_year2-survey_year9 sex diabetes if age > 20, hybrid full nocomparison keep 

/*
mundlak works $mundlak_nf survey_year2-survey_year9 sex diabetes, hybrid full nocomparison keep 
//eststo worksFE: xtreg works $fe_bw, re cluster(pid_link)  // between-within
xtgee works $fe $bw *mean_sex *mean_raven_score *mean_height_m *mean_depression_indicator, family(binomial) link(logit) corr(exchangeable) vce(robust)  // xtgee probit estimates using within between technique to get fixed effects
margins, dydx(demean_diabetes mean_diabetes) at(mean_age=(15(10)64))
marginsplot
*/
xtgee works *__* if sex== 0 & age > 20, family(binomial) link(logit) corr(exchangeable) vce(robust)
margins, dydx(*diabetes*)


forvalues sex=0/1{ // by gender 
display `sex'
//areg works $controls i.diabetes if sex== `sex', absorb(pid_link) ro
eststo worksFE`sex': xtreg works age_sq $controls i.diabetes $states diab_father diab_mother if sex== `sex', fe ro
//eststo worksRE`sex': xtreg works $controls i.diabetes $states diab_father diab_mother if sex== `sex', re ro
//xtoverid, cluster(pid_link)
//eststo worksFE`sex': xtgee works $controls i.diabetes if sex== `sex', family(binomial) link(logit) corr(exchangeable) vce(robust) 
//margins diabetes, over(age)
//marginsplot, x(age) name(workmargins`sex', replace)
//mundlak works $controls_nf diabetes if sex== `sex', full use($nofix)
//eststo worksbw`sex': xtreg works $fe $bw if sex== `sex', re ro   // within-between effects model
//qui xtgee works $fe $bw if sex== `sex', family(binomial) link(logit) corr(exchangeable) vce(robust)
//margins, dydx(*diabetes*)
//qui xtgee works $fe $bw if sex== `sex', family(binomial) link(probit) corr(exchangeable) vce(robust)
//margins, dydx(demean_diabetes) at(mean_age=(15(1)64))
//marginsplot, name(sex`sex', replace)
//marginsplot, name(xtgee`sex')
}

// using lagged diabetes
sort pid_link t
gen l1diabetes = l.diabetes
forvalues sex=0/1{ // by gender and lagged

display `sex'
eststo worksFE`sex': xtreg works $controls l.diabetes if sex== `sex', re ro
//eststo worksFE`sex': xtgee works $controls i.diabetes if sex== `sex', family(binomial) link(probit) corr(exchangeable) vce(robust) 
//margins diabetes, over(age)
//marginsplot, x(age) name(workmargins`sex', replace)
//mundlak works $controls_nf diabetes high_altitude2 high_altitude3 if sex== `sex', full use($nofix_nf diabetes high_altitude2 high_altitude3)
}



forvalues sex=0/1{ // by gender hausman test for RE or FE using xtoverid
display `sex'
eststo worksFE`sex': xtreg works $Z $years if sex== `sex', re cluster(pid_link)
xtoverid, cluster(pid_link)
}

// wages and workhours

foreach var of varlist $Y1 { //pooled
eststo `var'FE_pooled: xtreg `var' $wage $states sex, ro fe
//eststo `var'RE_pooled: xtreg `var' age $wage $states sex, ro re

}

forvalues sex=0/1{
foreach var of varlist $Y1 { // by gender
display `sex'
eststo `var'FE`sex': xtreg `var' $wage $years if sex== `sex', fe ro
//eststo `var'RE`sex': xtreg `var' $wage $years if sex== `sex', re ro

//xtreg `var' $fe demean_agricultural demean_selfemployed $bw mean_agricultural mean_selfemployed if sex== `sex', re ro

	}
}


esttab worksOLS0 worksOLS1  lnwage_hrOLS0 lnwage_hrOLS1 workhrsOLS0 workhrsOLS1 using "`dir'FE_OLS.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced} 

esttab noFE_works0 noFE_works1  noFE_lnwage_hr0 noFE_lnwage_hr1 noFE_workhrs0 noFE_workhrs1 using noFE.tex, replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced} 



/* Fixed Effects Stratifying it by worktype*/

forvalues wt=1/3{
foreach var of varlist $Y1 { // by worktype
display `wt'
eststo `var'FE`wt': xtreg `var' $wage $years sex if worktype== `wt', fe ro

	}
}

forvalues sex=0/1{
forvalues wt=1/3{
foreach var of varlist $Y1 { // by worktype and sex
display `wt'`sex'
eststo `var'FE`wt'`sex': xtreg `var' $wage $years if sex== `sex' & worktype== `wt', fe ro
		}
	}
}


/*Interaction term between diabetes and worktype*/

foreach var of varlist $Y1 { // worktype diabetes interaction
eststo `var'FEwt: xtreg `var' $wage1 i.diabetes##i.worktype sex diab_father diab_mother, fe ro
eststo `var'REwt: xtreg `var' $wage1 i.diabetes##i.worktype sex diab_father diab_mother, re ro

//margins diabetes, at (worktype = (1(1)3))
//marginsplot, scheme(sj) name(`var')
}

testparm i.diabetes#i.worktype

forvalues sex=0/1{
foreach var of varlist $Y1 { // by worktype
display `sex'
eststo `var'FEwt`sex': xtreg `var' $wage1 i.diabetes##i.worktype diab_father diab_mother if sex== `sex', fe ro
eststo `var'REwt`sex': xtreg `var' $wage1 i.diabetes##i.worktype diab_father diab_mother if sex== `sex', re ro

testparm diabetes#worktype
		}
}



/*The effect of diabtes on the selection into different worktypes*/


/*MLOGIT*/

label values status .


constraint 1 []rldist3=0 
femlogit status age age_sq smallcity city bigcity married kids_hh wealth diabetes insurance t2 t3, base(0) ro
margins, predict(outcome(_3))


femlogit status smallcity city bigcity married kids_hh wealth diabetes survey_year2-survey_year9 if sex ==0, base(0) ro group(pid_link)
femlogit status smallcity city bigcity married kids_hh wealth diabetes survey_year2-survey_year9 if sex ==1, base(0) ro group(pid_link)


global mlogit_fe "*mean_diabetes *mean_age_sq *mean_smallcity *mean_city *mean_bigcity *mean_south_southeast *mean_central *mean_westcentr *mean_northwest  *mean_secondary *mean_highschool *mean_college_uni *mean_married  *mean_insurance *mean_wealth *mean_indigenous *mean_survey_year2 *mean_survey_year3 *mean_survey_year4 *mean_survey_year5 *mean_survey_year6 *mean_survey_year6 *mean_survey_year7 *mean_survey_year8 *mean_survey_year9"

global mlogit "*mean_diabetes *mean_age_sq *mean_smallcity *mean_city *mean_bigcity *mean_south_southeast *mean_central *mean_westcentr *mean_northwest  *mean_secondary *mean_highschool *mean_college_uni *mean_married  *mean_insurance *mean_wealth *mean_indigenous *mean_BajaCaliforniaSur *mean_Coahuila *mean_Durango *mean_Guanajuato *mean_Jalisco *mean_DistritoFederal *mean_Michoacn *mean_Morelos *mean_NuevoLen *mean_Oaxaca *mean_Puebla *mean_Sinaloa *mean_Sonora *mean_Veracruz *mean_Yucatn *mean_survey_year2 *mean_survey_year3 *mean_survey_year4 *mean_survey_year5 *mean_survey_year6 *mean_survey_year6 *mean_survey_year7 *mean_survey_year8 *mean_survey_year9"

global mean "mean_diabetes mean_age_sq mean_smallcity mean_city mean_bigcity mean_south_southeast mean_central mean_westcentr mean_northwest  mean_secondary mean_highschool mean_college_uni  mean_insurance mean_wealth mean_indigenous mean_survey_year2-mean_survey_year9"


mundlak works $mundlak_nf survey_year2-survey_year9 sex diabetes $states, hybrid full nocomparison keep //use($interest_nf survey_year2-survey_year9 sex) nocomparison 


// use hypbird model proposed by allison
mlogit status $fe $bw, cluster(pid_link) base(0)
//mprobit status $fe $bw *mean_sex, cluster(pid_link) base(0)

forvalues i=1/3{
eststo marg_`i': margins, dydx(*diabetes) predict(outcome(`i'))
}

forvalues sex=0/1{
 mlogit status $fe $bw if sex == `sex', cluster(pid_link) base(0)
	forvalues i=0/3{
	eststo marg_`i'`sex': margins, dydx(*diabetes) predict(outcome(`i'))
	}
}

forvalues sex=0/1{
 mlogit status $controls i.diabetes if sex == `sex', cluster(pid_link) base(0)
	forvalues i=0/3{
	eststo marg_`i'`sex': margins, dydx(diabetes) predict(outcome(`i'))
	}
}

estwrite * using "savedestimates/diabetes_dummy", id() append

/*Using each type of work as seperate outcome instead of using mlogit to get a first idea of the possible selection effect of diabetes*/

dummies status
eststo worksFE_employed: xtreg status2 $controls i.diabetes##sex $states if status <2, fe cluster(pid_link)  // non-agricultural employment  --> no effect
eststo worksFE_agricultural: xtreg status3 $controls i.diabetes##sex $states if status ==0 | status == 2, fe cluster(pid_link)  // agricultural employment --> sign neg effect -3.6%points
eststo worksFE_selfemployed: xtreg status4 $controls i.diabetes##sex $states if status ==0 | status == 3, fe cluster(pid_link)  // self employed --> borderline sign effect (p = 0.066) -3.9 percentage points 
forvalues sex=0/1{
eststo worksFE_employed`sex': xtreg status2 $controls i.diabetes $states if status <2 & sex == `sex', fe cluster(pid_link)  // non-agricultural employment  --> sign. neg effect for men but not for women
eststo worksFE_agricultural`sex': xtreg status3 $controls i.diabetes $states if (status ==0 | status == 2) & sex == `sex', fe cluster(pid_link)  // agricultural employment --> sign neg effect for women
eststo worksFE_selfemployed`sex': xtreg status4 $controls i.diabetes $states if (status ==0 | status == 3) & sex == `sex', fe cluster(pid_link)  // self employed --> sign neg effect for women
}


// find no gender differences when including interaction term with diabetes

**************************************************
***********TABLES FOR DIABETES AS A DUMMY*********
**************************************************

 
  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2_a N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) keep(1.diabetes *worktype) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth")

  global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2_w r2_o N, fmt(%9.3f %9.3f %9.0f) labels("R2 within" "R2" "N")) booktabs obslast nolz  ///
	 alignment(D{.}{.}{-1}l)   collabels(none) keep(*diabetes *worktype) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, state, urban, education, indigenous, marital status, children, wealth, insurance, parental diabetes")

  global table_reduced_years b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2_a N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) label nobaselevels ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth")


esttab worksFE worksFE0 worksFE1 lnwage_hrFE_pooled lnwage_hrFE0 lnwage_hrFE1 workhrsFE_pooled workhrsFE0 workhrsFE1 using FElabouroutcomes.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} 
       
esttab worksRE worksRE0 worksRE1 lnwage_hrRE_pooled lnwage_hrRE0 lnwage_hrRE1 workhrsRE_pooled workhrsRE0 workhrsRE1 using RElabouroutcomes.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} 
       


// table with interactione effects
esttab lnwage_hrFEwt lnwage_hrFEwt0 lnwage_hrFEwt1 workhrsFEwt workhrsFEwt0 workhrsFEwt1 using FElabouroutcomes_interaction.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Log monthly wages (FE)" "Monthly work hours (FE)" "Log monthly wages (RE)" "Monthly work hours (RE)", pattern(1 0 0 1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} 
       
 esttab lnwage_hrREwt lnwage_hrREwt0 lnwage_hrREwt1 workhrsREwt workhrsREwt0 workhrsREwt1 using RElabouroutcomes_interaction.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Log monthly wages" "Monthly work hours" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} 
      

// table with mlogit
global tablenofact_marg_reduced cells(margins_b(fmt(3)star) margins_se(fmt(3)par)) star(* 0.10 ** 0.05 *** 0.01) stats(N, fmt(%9.3f %9.0f) labels("N")) booktabs obslast nolz  ///
	  alignment(S S) coeflabels(diabetes Diabetes)  collabels(none) keep(diabetes) ///
	 addnote("Marginal effects; Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth, parental education" "* p < 0.1, ** p < 0.05, *** p < 0.01")

*pooled	 
esttab marg_1 marg_2 marg_3 using mlogit.tex, replace comp ///
 mti("Agric." "Non-agric." "Self-employed")  ${tablenofact_marg_reduced}
// time since diagnosis linear continuous years

*by sex
esttab marg_100 marg_200 marg_300 marg_110 marg_210 marg_310 using mlogit_sex_2002.tex, replace comp ///
 mti("Agric." "Non-agric." "Self-employed" "Agric." "Non-agric." "Self-employed") mgroups(Males Females, pattern(1 0 0 1 0 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span}))  ${tablenofact_marg_reduced}

esttab marg_101 marg_201 marg_301 marg_111 marg_211 marg_311 using mlogit_sex_2005.tex, replace comp ///
 mti("Agric." "Non-agric." "Self-employed" "Agric." "Non-agric." "Self-employed") mgroups(Males Females, pattern(1 0 0 1 0 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span}))  ${tablenofact_marg_reduced}

esttab marg_102 marg_202 marg_302 marg_112 marg_212 marg_312 using mlogit_sex_2009.tex, replace comp ///
 mti("Agric." "Non-agric." "Self-employed" "Agric." "Non-agric." "Self-employed") mgroups(Males Females, pattern(1 0 0 1 0 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span}))  ${tablenofact_marg_reduced}
       
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
*************************************************************************************************
***************USE OF DIABETES DURATION INSTEAD OF DIABETES DUMMY********************************
*************************************************************************************************
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

/*Using linear specification*/

log close

log using Logs/duration_test.log, replace


drop $fe $bw
mundlak works $mundlak_nf survey_year2-survey_year9 sex years_diabetes $states, hybrid full nocomparison // keep //use($interest_nf survey_year2-survey_year9 sex) nocomparison 
//global controls "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh  wealth i.indigenous i.survey_year"



* working

xtgee works $fe $bw mean_sex mean_indigenous if years_diabetes < 20, ro corr(exchangeable) link(logit) family(binomial)
margins, dydx(*__years_diabetes)
marginsplot



eststo worksFE_l_pooled: xtreg works $controls years_diabetes $states diab_father diab_mother if years_diabetes < 20, ro fe  // pooled
eststo worksRE_l_pooled: xtreg works age $controls years_diabetes $states diab_father diab_mother if years_diabetes < 20, ro re  // pooled


eststo worksRE_l_pooled: xtreg works $fe $bw if years_diabetes < 20, ro re  // pooled

*margins, dydx(years_diabetes) saving(margins_years_diabetes, replace)

forvalues sex=0/1{  // by gender
display `sex'
xtset pid_link t
eststo works_l_FE`sex': xtreg works age_sq $controls years_diabetes $states diab_father diab_mother if sex== `sex' & years_diabetes <20, cluster(pid_link) fe  // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
//eststo works_l_RE`sex': xtreg works $controls years_diabetes $states diab_father diab_mother if sex== `sex' & years_diabetes <20, cluster(pid_link) re  // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
//mundlak works $mundlak_nf survey_year2-survey_year9 years_diabetes $states if sex== `sex' & years_diabetes <20, hybrid full nocomparison // keep //use($interest_nf survey_year2-survey_year9 sex) nocomparison 
/*xtset, clear
xtreg works age_sq $controls years_diabetes $states diab_father diab_mother if sex== `sex', fe i(folio_sib)
xtreg works age_sq $controls years_diabetes $states diab_father diab_mother if sex== `sex', fe i(folio)
xtset pid_link t*/
*margins, dydx(years_diabetes)saving(margins_years_diabetes`sex', replace)
*marginsplot
*xtgee works $fe $bw if sex== `sex', ro corr(exchangeable) link(probit) family(binomial)
//lpoly works years_diabetes if sex== `sex', name(works`sex', replace)
}


//coefplot worksFE_l_pooled || works_l_FE0 || works_l_FE1, keep(years_diabetes) xline(0) bycoefs byopts(yrescale)  xlabel(, labsize(large)) ylabel( 1 "Pooled" 2 "Males" 3 "Females", labsize(large)) xtitle("Change in employment probability", size(large))
//graph export marginsplot_duration_combo.eps, replace

* wages and workhours
global wage1 "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married wealth kids_hh i.indigenous i.worktype i.insurance i.survey_year"  // no diabetes included to be used with years since diagnosis


foreach var of varlist $Y1{  // pooled
eststo `var'_l_FE_pooled: xtreg `var' $wage1 $states c.years_diabetes i.worktype sex , cluster(pid_link) fe
eststo `var'_l_RE_pooled: xtreg `var' $wage1 $states c.years_diabetes i.worktype sex, cluster(pid_link) re

}



forvalues sex=0/1{  // by gender
foreach var of varlist $Y1{
display `sex' `var'
eststo `var'_l_FE`sex': xtreg `var' $wage1 $states c.years_diabetes i.worktype if sex== `sex', ro fe
eststo `var'_l_RE`sex': xtreg `var' $wage1 $states c.years_diabetes i.worktype if sex== `sex', ro re
	}
}

// OLS
eststo works_l_OLS_pooled: reg works $controls years_diabetes $years sex  if t==2
lvr2plot
predict d1, cooksd
clist pid_link works $controls_nf years_diabetes d1 if d1>4/16244 & t == 2 & d1 <. , noobs
rreg works $controls years_diabetes $years sex  if t==2

reg works $controls years_diabetes $years sex  if t==2, ro cluster(folio)
foreach var of varlist $Y1{  // pooled
eststo `var'_l_OLS_pooled: reg `var' $wage1 years_diabetes sex  if t==2, ro cluster(folio)
}

forvalues sex=0/1{  // by gender
foreach var of varlist $Y1{
display `sex' `var'
eststo `var'_l_OLS`sex': reg `var' $wage1 years_diabetes if t==2 & sex== `sex', ro 
	}
}


******************************************************************************************************
/* Using splines:  time since diagnosis using splines 0-1 2-5 6-9 10-15 >15 for time since diagnosis*/
******************************************************************************************************
global fe_l "demean_diab_y* demean_age demean_age_sq demean_smallcity demean_city demean_bigcity demean_south_southeast demean_central demean_westcentr demean_northwest  demean_secondary demean_highschool demean_college_uni demean_married demean_kids_hh demean_wealth demean_indigenous demean_t2 demean_t3"
global bw_l "mean_diab_y* mean_age mean_age_sq mean_smallcity mean_city mean_bigcity mean_south_southeast mean_central mean_westcentr mean_northwest  mean_secondary mean_highschool mean_college_uni mean_married mean_kids_hh mean_wealth mean_indigenous mean_t2 mean_t3"


* working
global Z_length_group "diab_y*"

/*testing if interaction effects are different from zero. not the case here
eststo worksFE_sp_pooled: xtreg works $controls c.diab_y*##sex $years, cluster(pid_link) fe  // pooled
testparm c.diab_y1#sex
testparm c.diab_y2#sex
testparm c.diab_y3#sex
testparm c.diab_y4#sex
*/

eststo worksFE_sp_pooled: xtreg works $controls diab_y* $years i.obese overweight $robust, cluster(pid_link) fe  // pooled
//margins, dydx(diab_y*) saving(margins_pooled, replace)
//marginsplot , xlabel(1 "0-3" 2 "4-10" 3 "11-19" 4 "20+") plotopts(connect(i)) xscale(range(0.5 4.5)) yline(0) xtitle("Years since diagnosis -- linear splines", size(large)) ytitle("Changes in employment probability", size(large)) ylabel(, nogrid) title("")
//graph export marginsplot_duration_pooled.eps, replace




forvalues sex=0/1{  // by gender
display `sex'
xtset pid_link t
eststo w_sp_FE`sex': xtreg works age_sq $controls diab_y* if sex== `sex',  cluster(pid_link) fe
/*xtset, clear
eststo w_sp_familyFE`sex': xtreg works age_sq $controls diab_y* if sex== `sex', fe i(folio_sib)
eststo w_sp_familyFE`sex': xtreg works age_sq $controls diab_y* if sex== `sex', fe i(folio)
xtset pid_link t
//mundlak works $mundlak_nf age survey_year2-survey_year9 diab_y* $states if sex== `sex', hybrid full nocomparison // keep //use($interest_nf survey_year2-survey_year9 sex) nocomparison 
//margins, dydx(diab_y*) saving(margins_`sex', replace)
//marginsplot
*graph export marginsplot_duration_sex`sex'.eps, replace
*eststo w_sp_febw`sex': xtgee works $fe_l $bw_l if sex== `sex', family(binomial) link(probit) corr(exchangeable) vce(robust)
*margins, dydx(demean_diab_y* mean_diab_y*)*/
}

coefplot (worksFE_sp_pooled, label(Pooled)) (w_sp_FE0 , label(Males)) (w_sp_FE1, label(Females)), keep(diab_y*) xline(0) ylabel(1 "0-3" 2 "4-10" 3 "11-19" 4 "20+") xlabel(, labsize(large)) ytitle("Years since diagnosis - linear splines", size(large)) xtitle("Changes in employment probability", size(large)) legend(rows(1))
graph export marginsplot_duration_all.eps, replace

* wages and workhours

foreach var of varlist $Y1{  // pooled
eststo `var'_sp_FE_pooled: xtreg `var' $wage1 diab_y* $years sex, cluster(pid_link) fe
xtreg `var' $wage1 c.diab_y*##sex $years, cluster(pid_link) fe
/*testin if difference between sexes using interaction term
Not the case here so no need to stratify
foreach var of varlist diab_y* {
testparm c.`var'#sex
}
*/
//eststo `var'_sp_FE_pooled_febw: xtreg `var' $fe_l $bw_l mean_sex demean_sex demean_agricultural demean_selfemployed  mean_agricultural mean_selfemployed,  cluster(pid_link) re
}

forvalues sex=0/1{  // by gender
foreach var of varlist $Y1{
display `sex' `var'
eststo `var'_sp_FE`sex': xtreg `var' $wage1 diab_y* $years if sex== `sex', cluster(pid_link) fe
	}
}

// OLS

foreach var of varlist $Y1{  // pooled
eststo `var'_sp_OLS_p: reg `var' $wage1 diab_y* sex  if t==2, ro cluster(pid_link)
}

forvalues sex=0/1{  // by gender
foreach var of varlist $Y1{
display `sex' `var'
eststo `var'_sp_OLS`sex': reg `var' diabetes $wage1 diab_y* if t==2 & sex== `sex', ro cluster(pid_link)
	}
}

***************************************************************************************************************
/*Using time sinces diagnosis squared additionally to time since diagnosis to capture possible non-linearities*/

eststo worksFE_sq_pooled: xtreg works $controls c.years_diabetes##c.years_diabetes $years sex, ro fe  // pooled


forvalues sex=0/1{  // by gender
display `sex'
eststo w_sq_FE`sex': xtreg works $controls c.years_diabetes##c.years_diabetes $years if sex== `sex', cluster(pid_link) fe
}

* wages and workhours

foreach var of varlist $Y1{  // pooled
eststo `var'_sq_FE_pooled: xtreg `var' $wage1 c.years_diabetes##c.years_diabetes sex $years, cluster(pid_link) fe
}

forvalues sex=0/1{  // by gender
foreach var of varlist $Y1{
display `sex' `var'
eststo `var'_sq_FE`sex': xtreg `var' $wage1 c.years_diabetes##c.years_diabetes $years if sex== `sex', ro fe
	}
}

***************************************************************************************************************
/*Using time sinces diagnosis and age at diagnosis groups to interact*/

xtreg works $controls i.age_diagnosis_group $Z_length_group $years sex, ro fe  // pooled


forvalues sex=0/1{  // by gender
display `sex'
eststo w_sq_FE`sex': probit works $controls i.age_diagnosis_group $years if sex== `sex' & t ==2, cluster(folio)
margins, dydx(*)
}

* wages and workhours

foreach var of varlist $Y1{  // pooled
eststo `var'_sq_FE_pooled: xtreg `var' $wage1 c.years_diabetes##c.years_diabetes sex $years, cluster(pid_link) fe
}

forvalues sex=0/1{  // by gender
foreach var of varlist $Y1{
display `sex' `var'
eststo `var'_sq_FE`sex': xtreg `var' $wage1 c.years_diabetes##c.years_diabetes $years if sex== `sex', ro fe
	}
}



// Heckman for wages

global selection "age age_sq i.smallcity i.city i.bigcity   i.secondary i.highschool i.college_uni i.married wealth kids_hh i.indigenous i.diabetes"
global interest "age age_sq i.smallcity i.city i.bigcity   i.secondary i.highschool i.college_uni i.indigenous i.worktype i.married i.diabetes wealth"
global interest_nf "age_sq smallcity city bigcity   secondary highschool college_uni indigenous south_southeast northwest central westcentr agricultural selfemployed married diabetes wealth"

global mean "mean_diabetes mean_age mean_age_sq mean_smallcity mean_city mean_bigcity mean_south_southeast mean_central mean_westcentr mean_northwest  mean_secondary mean_highschool mean_college_uni mean_married mean_wealth mean_indigenous mean_agricultural mean_selfemployed mean_survey_year2-mean_survey_year9"
global mundlak "*mean_diabetes *mean_age_sq *mean_smallcity *mean_city *mean_bigcity *mean_south_southeast *mean_central *mean_westcentr *mean_northwest  *mean_secondary *mean_highschool *mean_college_uni *mean_married *mean_wealth *mean_indigenous *mean_agricultural *mean_selfemployed *mean_BajaCaliforniaSur *mean_Coahuila *mean_Durango *mean_Guanajuato *mean_Jalisco *mean_DistritoFederal *mean_Michoacn *mean_Morelos *mean_NuevoLen *mean_Oaxaca *mean_Puebla *mean_Sinaloa *mean_Sonora *mean_Veracruz *mean_Yucatn *mean_survey_year2 *mean_survey_year3 *mean_survey_year4 *mean_survey_year5 *mean_survey_year6 *mean_survey_year6 *mean_survey_year7 *mean_survey_year8 *mean_survey_year9"



drop mills*
gen millsratio = 0
gen millsratio0 = 0
gen millsratio1 = 0
forvalues t=0/2{
display `t'	
heckman lnwage_hr $interest $regions sex if t==`t', select($selection $regions sex) mills(mills`t') twostep	
	replace millsratio = mills`t' if t==`t'
	forvalues sex=0/1{
	display `sex'`t'
	heckman lnwage_hr $interest $regions if t==`t' & sex == `sex', select($selection $regions) mills(mills`t'`sex') twostep
	replace millsratio`sex' = mills`t'`sex' if t==`t' & sex == `sex'
	}
}


eststo heckit_p: xtreg lnwage_hr $interest $mean mean_sex millsratio, vce(cluster pid_link) re

drop $fe $bw
mundlak lnwage_hr $interest_nf survey_year2-survey_year9 sex, hybrid full nocomparison keep //use($interest_nf survey_year2-survey_year9 sex) nocomparison 

eststo heckit_p: xtreg lnwage_hr diff__* mean__*  millsratio, vce(cluster pid_link) re



forvalues sex=0/1{
xtreg lnwage_hr diff__* mean__*  millsratio if sex == `sex', re cluster(pid_link)
//reg lnwage_hr $wage i.survey_year $mean mean_sex millsratio if sex == `sex', vce(cluster pid_link)
}


global tablenofact b(%9.3f) se(%9.3f)  star(* 0.1 ** 0.05 *** 0.01) stats(r2_o N, fmt(%9.3f %9.0f) labels("R2" "N")) wide booktabs obslast nolz  ///
	 alignment(S S) keep(1.*) coeflabels(_cons Constant 1.diabetes Diabetes 1.age25to34 "Age 25--34" 1.age35to44 "Age 35--44" 1.age45to54 "Age 45--54" 1.age55to64 "Age 55--64" ///
	 1.rural Rural 1.smallcity "Small city" 1.city City 1.bigcity "Big city" 1.central Central 1.westcentr Westcentral 1.northeastcentr Northeastcentral ///
	 1.northwest Northwestcentral 1.primary Primary 1.secondary Secondary 1.highschool Highschool 1.college_uni "College or university" ///
  1.indig Indigenous 1.married Married kids_hh "Children (under 15)" wealth Wealth 1.educ_par "Education parents")
  
  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2_o N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) keep(1.diabetes) label /// coeflables(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth, parental education")

  global table_reduced_years b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2_o N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) label nobaselevels ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, marital status, children, wealth.")

  global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2_a N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(D{.}{.}{-1}l)   collabels(none) label nobaselevels /// coeflables(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth")



esttab worksFE_pooled worksFE0 worksFE1 lnwage_hrFE_pooled lnwage_hrFE0 lnwage_hrFE1 workhrsFE_pooled workhrsFE0 workhrsFE1 using FElabouroutcomes.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced} 

esttab worksFE_l_pooled works_l_FE0 works_l_FE1 works_l_OLS0 works_l_OLS1 works_l_OLS_pooled using FEworks_l.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("All waves" "Only 2009", pattern(1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced_years} 



// length estimates using linear length estimate 
esttab worksFE_l_pooled works_l_FE0 works_l_FE1 lnwage_hr_l_FE_pooled lnwage_hr_l_FE0 lnwage_hr_l_FE1 ///
workhrs_l_FE_pooled workhrs_l_FE0 workhrs_l_FE1 using FElabouroutcomens_l.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} keep(years_diabetes *worktype) 

// length estimates using linear spline length estimate 

esttab worksFE_sp_pooled w_sp_FE0 w_sp_FE1 lnwage_hr_sp_FE_pooled lnwage_hr_sp_FE0 lnwage_hr_sp_FE1 ///
workhrs_sp_FE_pooled workhrs_sp_FE0 workhrs_sp_FE1 using FElabouroutcomens_sp.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) ${authorea}  keep(diab_y*)


       
       
