clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""


cd $homefolder
**log-file starten**
log using Logs/descriptives.log, replace

		use "Paneldaten/final.dta", clear
		
		
global table "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Tables/" // directory to save tables to
global figures "/home/till/Dokumente/Chapter-3/figures" // directory to save tables to
	


**********************************************************************************
************Kdensity plots to investigate distribution of outcome variabls********
**********************************************************************************



*******************************************************************************************************************
*******************Lpoly regression to investigate relationships between different RHS and LHS variables as well***
*******************as between LHS variables************************************************************************
*******************************************************************************************************************

/*Relationship between age and diabetes*/
global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco Morelos Michoacn NuevoLen Oaxaca Puebla Sinaloa Yucatn Veracruz"

quietly xtreg works age_ini age_sq years_diabetes i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh i.indigenous wealth i.survey_year $states, fe
gen sample_panel =1 if e(sample)==1 

twoway lpolyci works years_diabetes if sex == 0, fcolor(gs15%40) alpattern(dash_dot) alcolor(none)  || lpolyci works years_diabetes if sex == 1, ///
name(works, replace) ylabel(0(0.2)1, ang(hor) nogrid) xlabel(#15) bw(0.75) scheme(s1mono) xtitle(Years since diagnosis) ytitle(Employment) title(Employment) ///
legend(label(1 "CI") label(2 "Males") label(4 "Females") cols(1) order(2 4) size(3)) fcolor(gs15%40) alcolor(none) lpattern(dash) alpattern(dash)  || if years_diabetes < 25  // fintensity(inten20)
graph export "$figures/lpoly_works_diabetesduration.eps", replace 

twoway lpolyci  lnwage_hr years_diabetes if sex == 0, fcolor(gs15%40) alpattern(dash_dot) alcolor(none) || lpolyci lnwage_hr years_diabetes if sex == 1, title(Wages) ytitle("Log hourly wage") name(wages, replace) ///
ylabel(, ang(hor) nogrid) xlabel(#15) bw(0.75) xtitle(Years since diagnosis) scheme(s1mono) fcolor(gs15%40) alcolor(none) lpattern(dash) alpattern(dash) legend(label(1 "CI") label(2 "Males")  label(4 "Females") cols(1) order(2 4)) xtitle(Years since diagnosis) || if years_diabetes < 25
graph export "$figures/lpoly_wage_diabetesduration.eps", replace 

twoway lpolyci workhrs years_diabetes if sex == 0, fcolor(gs15%40) alpattern(dash_dot) alcolor(none) || lpolyci workhrs years_diabetes if sex == 1, title(Working hours) ytitle("Weekly working hours") name(hours, replace) ///
ylabel(, ang(hor) nogrid) xlabel(#15) bw(0.75) scheme(s1mono) legend(label(1 "CI") label(2 "Males") label(4 "Females")  position(3) cols(1) order(2 4)) fcolor(gs15%40) alcolor(none) lpattern(dash) alpattern(dash) xtitle(Years since diagnosis) || if years_diabetes < 25
graph export "$figures/lpoly_workhrs_diabetesduration.eps", replace 

grc1leg works hours wages, col(2) altshrink  legendfrom(wages) scheme(s1mono)
graph export "$figures/lpoly_combined.eps", replace  cmyk(on)
graph export "$figures/lpoly_combined.svg", replace  
graph export "$figures/lpoly_combined.pdf", replace  


****************************************************
*********Tables for descriptive statistics**********
****************************************************

global y "works wage_hr workhrs"
global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco Morelos Michoacn NuevoLen Oaxaca Puebla Sinaloa Yucatn Veracruz"
global controls "age_ini age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married i.insurance  kids_hh i.indigenous wealth i.survey_year"
global controls_nf "age insurance smallcity city bigcity married  kids_hh indigenous primary secondary highschool college_uni wealth"
global bio "hba1c diab_hba1c diab_ud"
 xtreg works $controls i.diabetes sex $states, fe
gen sample_panel =1 if e(sample)==1 
reg works age age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh i.indigenous wealth i.survey_year i.diabetes $states hba1c if t==2
gen sample_bio = 1 if e(sample)==1							
replace wage_hr = . if lnwage_hr ==.

recode diabetes (0=1) (1=0), gen(diab_rec)

forvalues sex=0/1{

eststo sum`sex': estpost sum $y status1 - status4  $controls_nf if sex == `sex' & sample_panel ==1, det
eststo sumbio`sex': estpost sum $y status1 - status4  $controls_nf $bio if sex == `sex' & sample_bio==1, det
eststo ttest_`sex': estpost ttest $y status1 - status4  $controls_nf if sex == `sex' & sample_panel ==1, by(diab_rec)
eststo ttest_`sex'_wage: estpost ttest $y status2 - status4 $controls_nf if sex == `sex' & sample_panel ==1 & lnwage_hr <., by(diab_rec)
eststo ttest_`sex'_unemployed: estpost ttest $controls_nf if sex == `sex' & sample_panel ==1 & works==0, by(diab_rec)


// only using biomarker sample and according to diabetes status: no diabetes, diagnosed and undiagnosed
eststo sumbio_nodia`sex': estpost sum $y works status1 - status4  $controls_nf $bio sex if bio == 1 & diabetes == 0 & diab_ud==0 & sex==`sex', det
eststo sumbio_dia_sr`sex': estpost sum $y works status1 - status4 $controls_nf $bio sex if bio == 1 & diabetes == 1 & sex==`sex', det
eststo sumbio_dia_ud`sex': estpost sum $y works status1 - status4 $controls_nf $bio sex if bio == 1 & diab_ud == 1 & sex==`sex', det
}

bysort sex: sum years_diabetes if sample_panel ==1 & diabetes == 1, det  //  years of diabetes for those diagnosed
bysort sex: sum years_diabetes if bio == 1 & sample_panel ==1 & diabetes == 1, det  //  years of diabetes for those diagnosed


esttab ttest_0 ttest_1 using "$table/ttest_panel.tex" , replace cells("mu_2(fmt(2)) mu_1(fmt(2)) p(fmt(2))")  nonumber mti("Males" "Females") ///
booktabs  label alignment(S S) collabels("No self-reported diabetes" "Self-reported diabetes" "p (t-test)")

esttab ttest_0_wage ttest_1_wage using "$table/ttest_panel_wage.tex" , replace cells("mu_2(fmt(2)) mu_1(fmt(2)) p(fmt(2))")  nonumber mti("Males" "Females") ///
booktabs  label alignment(S S) collabels("No self-reported diabetes" "Self-reported diabetes" "p (t-test)")

esttab ttest_0_unemployed ttest_1_unemployed using "$table/ttest_panel_unemployed.tex" , replace cells("mu_2(fmt(2)) mu_1(fmt(2)) p(fmt(2))")  nonumber mti("Males" "Females") ///
booktabs  label alignment(S S) collabels("No self-reported diabetes" "Self-reported diabetes" "p (t-test)")

forvalues diabetes=0/1{
	forvalues sex=0/1{
eststo ttest_`diabetes'`sex'_diabetes: estpost ttest status1 - status4 $controls_nf if sex == `sex' & diabetes == `diabetes' & sample_panel ==1, by(works)
	}
}

esttab ttest_00_diabetes ttest_10_diabetes using "$table/ttest_panel_unemployed_men.tex" , replace cells("mu_2(fmt(2)) mu_1(fmt(2)) p(fmt(2))")  nonumber mti("No self-reported diabetes" "Self-reported diabetes") ///
booktabs  label alignment(S S) collabels("Employed" "Not employed" "p (t-test)")

esttab ttest_01_diabetes ttest_11_diabetes using "$table/ttest_panel_unemployed_women.tex" , replace cells("mu_2(fmt(2)) mu_1(fmt(2)) p(fmt(2))")  nonumber mti("No self-reported diabetes" "Self-reported diabetes") ///
booktabs  label alignment(S S) collabels("Employed" "Not employed" "p (t-test)")

/*
esttab sum0 sum1 using "$table/descriptives.tex" , replace cells(mean(fmt(3)) sd(par fmt(3)))  nonumber mti("Males" "Females") ///
 booktabs collabels(Mean) label
esttab sumbio0 sumbio1 using "$table/descriptives.tex" , append cells(mean(fmt(3)) sd(par fmt(3)))  nonumber ///
 booktabs label f nonumber nomtitles
 */

/*Table used in paper*/
esttab sum0 sum1 sumbio0 sumbio1  using "$table/descriptives_panel_bio.tex" , replace cells(mean(fmt(2)) sd(par fmt(2))) nonumber mti("Males" "Females" "Males" "Females") ///
 booktabs collabels(none) label mgroups("Panel" "Biomarker", pattern(1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(})  ///
       span erepeat(\cmidrule(lr){@span})) addnote("Standard deviations in parentheses")


// Tables with information on no diabetes, self-reported diabetes and undiagnosed diabetes groups by sex
local table "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Tables/" 
esttab sumbio_nodia0 sumbio_dia_sr0 sumbio_dia_ud0 using "`table'descriptives_only_bio_males.tex" , replace  main(mean(fmt(2)) aux(sd(fmt(2)) nonumber mti("No diabetes" "Only self-reported" "Undiagnosed") ///
 booktabs collabels(none) label addnote("Standard deviations in parentheses")

ttest hba1c if diab_hba1c == 1, by(diabetes)


// Test for difference in hba1c between those with diabetes reporting it only 1 time in three waves to reporting it two times in three waves.
// For statements made in appendix

  gen diabetes2 = 1 if diabetes13 ==1 | diabetes12 == 1  // everybody with two reports but inconsistent
  replace diabetes2 =0  if diabetes13 ==0 | diabetes12 == 0  // evrybody with one report but uinconsistent
   ttest hba1c, by(diabetes2)  // test for differences
 
 
/*Kernel density plot for hba1c levels for those with inconsistent self-reports*/ 
twoway kdensity hba1c if diabetes2==0, lpattern(dash) fcolor(none)  || kdensity hba1c if diabetes2==1 ||, ///
 xline(6.4) legend(label(1 "Assumed to not have diabetes diagnosis") label(2 "Assumed to have diabetes diagnosis") cols(1)) ///
 xtitle("HbA1c") ytitle("Density")   ylabel(, nogrid) text(.5 6.40 "Diabetes threshold") name(hba1c, replace)  scheme(s1mono)

 graph export "$figures/kdensity_hba1c_inconsist.eps", replace 
  
  
/* Not used for paper at the moment
forvalues sex=0/1[
eststo ttest_pooled`sex': estpost ttest $y $controls_nf diabetes years_diabetes hba1c if works < . & wealth < . & indig < ., by(sex)

/*Table for bioamarker data comparing subsample to overall sample*/
eststo ttest_bio: estpost ttest $y $controls_nf diabetes years_diabetes sex if age>44, by(bio)
esttab ttest_bio using ttest_bio.tex , replace cells("mu_2(fmt(3)) mu_1(fmt(3)) p(fmt(3))") ///
 booktabs  collabels("Mean ()" "Mean (men)" "p (t-test)")

//esttab ttest_pooled using ttest_pooled.tex , replace cells("mu_2(fmt(3)) mu_1(fmt(3)) p(fmt(3))") ///
 //booktabs  collabels("Mean (women)" "Mean (men)" "p (t-test)")




forvalues wt =1/3{
forvalues sex=0/1{
eststo sumpooled`sex'`wt': estpost  tabstat $y if sex == `sex' & worktype == `wt', by(t) statistics(count mean sd variance min max kurtosis skewness q) columns(statistics) listwise
esttab sumpooled`sex'`wt' using sumpooled`sex'`wt'.tex, replace cells("count mean sd variance min max p25 p50 p75 kurtosis skewness")  nonumber  noobs

	}
}


 esttab sumpooled using sum.tex, replace cells("count mean sd variance min max p25 p50 p75 kurtosis skewness")  nonumber  noobs
 esttab sumpooled0 using summale.tex, replace cells("count mean sd variance min max p25 p50 p75 kurtosis skewness")  nonumber  noobs
 esttab sumpooled1 using sumfemale.tex, replace cells("count mean sd variance min max p25 p50 p75 kurtosis skewness")  nonumber  noobs



//replace diab_hba1c =0 if hba1c <7
/*Information on self-report specificity and sensitifity using biomarker data*/

bysort diab_hba1c: tab diabetes if h

tab diabetes diab_hba1c, r

reg diab_hba1c i.diabetes sex age rural smallcity city bigcity south* central westcentr northeastcentr northwest noeducation primary secondary highschool college_uni married kids_hh wealth indigenous works age_sq, ro

margins i.diabetes

*/
*Testing what predicts being diagnosed
gen diab_diag_ud = 0 if diab_hba1c == 0 & diabetes == 0  // no diabetes
replace diab_diag_ud = 1 if diab_hba1c == 1 & diabetes == 0 // undiagnosed diabetes
replace diab_diag_ud = 2 if diab_hba1c == 1 & diabetes == 1 // sel-reported diabetes


bysort sex: reg diabetes sex age smallcity city bigcity $states primary secondary highschool college_uni married kids_hh wealth indigenous systolic i.status age_sq insurance obese overweight hypertension heart_disease works hba1c pee_night i.health_sr i.depression_indicator if diab_hba1c==1, ro 


// mainly hypertension diagnosis and 

bysort sex: mlogit diab_diag_ud hba1c sex age smallcity city bigcity $states primary secondary highschool college_uni married kids_hh wealth indigenous i.status age_sq insurance obese overweight hypertension heart_disease works, ro base(0)
bysort sex: reg diabetes sex age smallcity city bigcity $states primary secondary highschool college_uni married kids_hh wealth indigenous systolic i.status age_sq insurance obese overweight hypertension heart_disease works hba1c pee_night i.health_sr i.depression_indicator if diab_hba1c==1, ro 

gen diabetes_sr_ud = 0 if diab_ud == 1
replace diabetes_sr_ud = 1 if ec01a == 1 & t == 2 & hba1c <. & diab_controlled == 0

global y "works wage_hr workhrs"
global controls_nf "age insurance smallcity city bigcity married  kids_hh indigenous primary secondary highschool college_uni wealth"

eststo ttest_bio0: estpost ttest $y $controls_nf health_sr1 health_sr2 health_sr3 health_sr4 health_sr5 systolic  hba1c hypertension systolic diastolic ec01c bmi obese chronic_other1 chronic_other2 if hba1c <. & sex == 0 & diab_controlled == 0 & sample_bio == 1, by(diabetes_sr_ud)
eststo ttest_bio1: estpost ttest $y $controls_nf health_sr1 health_sr2 health_sr3 health_sr4 health_sr5 systolic  hba1c hypertension systolic diastolic ec01c bmi obese chronic_other1 chronic_other2 if hba1c <. & sex == 1 & diab_controlled == 0 & sample_bio == 1, by(diabetes_sr_ud)


esttab ttest_bio0 ttest_bio1 using "$table/ttest.tex" , replace cells("mu_2(fmt(3)) mu_1(fmt(3)) p(fmt(3))")  nonumber mti("Males" "Females") ///
booktabs  noobs label alignment(S S) collabels("Mean Diagnosed diabetes" "Mean undiagnosed diabetes" "p (t-test)")



/*Number of observations with within and between variation for employment sample*/

mundlak works age $controls_nf diabetes $states, use(diabetes $controls_nf) keep hybrid

xtsum mean__diabetes if mean__diabetes ==1 // n gives number of persons where that had diabetes throughout
xtsum mean__diabetes if mean__diabetes !=1 & mean__diabetes !=0 // n gives number of persons that first reported a diagnosis during the sample
