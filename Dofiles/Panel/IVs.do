clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""


cd $homefolder
**log-file starten**
log using Logs/panel_IV.log, replace

		use "Paneldaten/final.dta", clear
rename 	south_southeast sse
rename college_uni uni
global controls_nf "age age_sq smallcity city bigcity sse central westcentr northwest  secondary highschool uni married  kids_hh wealth indigenous t2 t3"

global fe "demean_diabetes demean_age demean_age_sq demean_smallcity demean_city demean_bigcity demean_south_southeast demean_central demean_westcentr demean_northwest  demean_secondary demean_highschool demean_college_uni demean_married demean_kids_hh demean_wealth demean_indigenous demean_t2 demean_t3"
global bw "mean_diabetes mean_age mean_age_sq mean_smallcity mean_city mean_bigcity mean_south_southeast mean_central mean_westcentr mean_northwest  mean_secondary mean_highschool mean_college_uni mean_married mean_kids_hh mean_wealth mean_indigenous mean_t2 mean_t3"

drop *mean_sex

global Y "works"
      
       
//IV regressions

/*

forvalues sex=0/1{
foreach var of varlist $Y{
display `sex' `var'
eststo `var'_IV`sex': ivreg2 `var' $IV $years (diabetes = diab_father diab_mother) if sex== `sex',  ro ffirst first endog(diabetes)
	}
}
forvalues sex=0/1{
display `sex'
eststo works_BP`sex': biprobit $BP if sex== `sex',  ro 
}

*/

bysort pid_link: center $Y diabetes $controls_nf lnwage_hr workhrs  agricultural selfemployed
reg c_works c_age-c_t3 c_diabetes if sex== 0
hettest


forvalue i=0/1{
ivreg2h c_works c_age-c_t3 (c_diabetes = )   if  sex == `i', endog(c_diabetes) ffirst first cluster(pid_link) gmm2s
//foreach var of varlist c_lnwage_hr c_workhrs{
//ivreg2h `var' c_age-c_selfemployed (c_diabetes = )   if  sex == `i', endog(c_diabetes) ffirst first cluster(pid_link) gmm2s
	}
}

global Y1 "lnwage_hr workhrs"


// generating Lewbel IVs for every wave seperately so that I can use fixed effects later
forvalue i=0/1{
foreach var of varlist  t1-t3{
ivreg2h $Y $controls_nf (diabetes = )   if `var' == 1 & sex == `i',endog(diabetes) gen(`var'`i', replace) ffirst first savefirst ///
	   savefprefix(firstivmal) ro 
}
}


// combining the different Lewbel IVs for each wave to one IV that can be used as IV in FE iv regression using only the Lewbel IV for age



qui probit diab_ud diab_father diab_mother bmi $controls_nf, ro
margins, dydx(*)
qui probit diabetes diab_father diab_mother bmi $controls_nf if t==2, ro
margins, dydx(*)



forvalue sex=0/1{  // IV Lewbel approach to use fixed effects. Not possible with standard instruments as they are not time variant
foreach var of varlist $Y{

	display `sex' `var' // to know which regression is running
	

quietly ivreg2h `var' $IV $years (diabetes = diab_father diab_mother) if sex==`sex', endog(diabetes) gen(age,replace) ///
	    cluster(folio) ro 
	   
	    	   
eststo ivregh_`var'`sex':  /// bootstrap, cluster(folio) rep(500):  ///
xtivreg2 `var' $IV $years (diabetes = age*age*)  if sex==`sex', fe endog(diabetes) ffirst first savefirst ///
	   savefprefix(firstivinf_h) 
drop age*age*	   
}
}

global tablenofactors_reduced b(%9.3f) se(%9.3f)  star(* 0.1 ** 0.05 *** 0.01) stats(r2 widstat j jp estat estatp N, fmt(%9.3f ///
%9.3f %9.3f %9.3f %9.3f %9.3f %9.0f) labels( ///
	"R2"  "F stat (H0: diabetes unidentified)" "Hansen J stat (H0: valid instruments)" "\hspace{10 mm}p value" "Endogeneity (H0: Diabetes exogenous)" "\hspace{10 mm}p value" N)) nogaps booktabs obslast nolz  ///
	alignment(S S) keep(diabetes) coeflabels(diabetes Diabetes)

esttab  ivregh_works0 ivregh_works1 ivregh_lnwage_adj0 ivregh_lnwage_adj1 ivregh_workinghrs0 ivregh_workinghrs1  using ivregh_panel_reduced.tex, mgroups("Employment" "Log monthly wage" "Monthly workhours", pattern(1 0 1 0 1 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) replace  comp  ///
 mti("Male" "Female" "Male" "Female" "Male" "Female") ${tablenofactors_reduced}   
 
 
 /*Using IV for biomarker sample of third wave*/
 
 ivreg2 works $controls_nf  sex (diabetes  = diab_father diab_mother) if t == 2 & bio == 1, endog(diabetes ) ro  ffirst first

 ivreg2 works $controls_nf  educ_par sex (diab_ud  = diab_father diab_mother) if t == 2 & bio == 1, endog(diab_ud) ro  ffirst first

reg works $controls_nf sex diabetes diab_ud
hettest

 biprobit (works = $controls_nf sex diabetes diab_ud) ( diabetes = diab_father diab_mother sex $controls_nf) if t == 2, ro
 biprobit (works = $controls_nf sex  diab_ud) ( diab_ud =  diab_mother sex $controls_nf) if t == 2, ro


 ivreg2h works $controls_nf sex (diabetes = i.high_ ) if t == 2, endog(diab_ud) ro  ffirst first gen(age,replace)

ivreg2 lnwage_hr $controls_nf sex (diab_ud = diab_father diab_mother) if t == 2, ro endog(diab_ud) ffirst first
ivreg2 workhrs $controls_nf sex (diab_ud = diab_father diab_mother) if t == 2, ro endog(diab_ud) ffirst first

ivreg2 works $controls_nf sex  (diabetes diab_ud = age*age*), ro endog(diabetes diab_ud) ffirst first
ivreg2 works $controls_nf sex diabetes (diab_ud  = agediab_ud*age*), ro endog(diab_ud) ffirst first
