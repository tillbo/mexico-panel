clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
* global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""


cd $homefolder
**log-file starten**
log using Logs/panel_test.log, replace

		use "Paneldaten/final.dta", clear


global Z "i.age25to34 i.age35to44 i.age45to54 i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh wealth educ_par i.diabetes"

global Zhba1c "i.age25to34 i.age35to44 i.age45to54 i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.indig i.married kids_hh wealth i.diab_severity"

global Zdiag "i.age25to34 i.age35to44 i.age45to54 i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.indig i.married kids_hh wealth diab_diag"

global Z8 "i.age25to34 i.age35to44 i.age45to54 i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni  i.married kids_hh wealth diab_8"

global Z9 "i.age25to34 i.age35to44 i.age45to54 i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.indig i.married kids_hh wealth diab_9"

global controls "i.age25to34 i.age35to44 i.age45to54 i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh wealth educ_par"

global Z_length_group "i.years_diabetes_groups"
global Z_length "i.diabetes##i.years_diabetes_groups"

global Zyoung "i.age35to44 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.indig i.married kids_hh wealth i.educ_par i.diabetes"

global Zold "i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.indig i.married kids_hh wealth i.educ_par i.diabetes"
global Zage "age i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.indig i.married kids_hh wealth i.educ_par i.diabetes"

global IV "age25to34 age35to44 age45to54 age55to64 smallcity city bigcity central westcentr northeastcentr northwest primary secondary highschool college_uni indig married kids_hh wealth (diabetes = diab_father diab_mother)"


quietly ivreg2 works age25to34 age35to44 age45to54 age55to64 smallcity city bigcity central westcentr northeastcentr northwest primary secondary highschool college_uni ///
  indig married kids_hh  educ_par wealth    ///
           (diabetes = diab_father diab_mother) if missinginfo == 0
drop if e(sample)~=1									/* drop observations missing on any variables will use */

	
    * males
 
 
eststo probworksmale: bootstrap,rep(500) cluster(folio): ///
reg works $Z  if sex== 0 & t==2, ro
reg works $Z diab_father diab_mother hba1c if sex== 1 & t==2, ro
reg works $Z sex hba1c if t==2, ro
reg works $Z sex if t==2 & hba1c <., ro
reg works $Zdiag hba1c sex if t==2 & hba1c <. & age >= 45, ro

reg works $Zhba1c $Z_length_group if sex== 0 & t==2, ro
reg works $Zhba1c $Z_length_group sex if t==2, ro

reg works $Zhba1c sex if t==2 & diabetes==0, ro
reg works $Zhba1c if sex == 0 & t==2 & diabetes==0, ro
reg works $Zhba1c if sex == 1 & t==2 & diabetes==0, ro


reg works $Z8 sex if t==2, ro
reg works $Z9 diabetes if sex== 0 & t==2, ro
reg works $Z9 if sex== 1 & t==2, ro
xtreg works $controls $Z_length_group t if sex== 0, ro fe
xtreg works $controls $Z_length_group t if sex== 1, ro fe
xtreg works $controls i.years_before_diabetes_groups t if sex== 0, ro fe
xtreg works $controls i.years_before_diabetes_groups t if sex== 1, ro fe

xtreg works $controls $Z_length_group t sex, ro fe
reg works $controls $Z_length_group t folio if sex== 0, ro 

xtreg works $controls $Z_length t if sex== 1, ro fe
xtreg works $controls $Z_length t if sex== 0, ro fe

reg works $controls $Z_length_group##diab_severity if sex== 0 & t == 2, ro 
reg works $controls $Z_length_group i.diab_severity if sex== 1 & t == 2, ro 
reg works $controls type1 sex if t == 2, ro 


xtreg works $Z t if sex== 0, ro fe

xtreg works $Z t  if sex== 1, ro fe

xtivreg2 works $IV if sex==0, fe endog(diabetes) ffirst first savefirst ///
	   savefprefix(firstivmal) ro 

/* ** without agegroups */
/* bootstrap,rep(500) cluster(folio): /// */
/* probit works age age_sq smallcity city bigcity central westcentr northeastcentr northwest primary secondary highschool college_uni indig married kids_hh wealth    diabetes if sex==0 */

/* scoregof, bootstrap(500) */


/* margins, dydx(*) */

** older than 44 with agegroups **


bootstrap,rep(500) cluster(folio): ///
probit works $Zold if sex==0 & age >= 45

scoregof

eststo probworksmalemargold: estadd margins, dydx(*) // if I raise cut off to 50 effect is 13 percentage points. if lowered to 40 it is 11 percentage points


/* ** older than 44 without agegroups ** */


/* bootstrap,rep(500) cluster(folio): /// */
/* probit works age age_sq smallcity city bigcity central westcentr northeastcentr northwest primary secondary highschool college_uni indig married kids_hh wealth     diabetes if sex==0 & age >= 45 */

/* scoregof */

/* margins, dydx(*) */


** younger than 45 with agegroups **

bootstrap,rep(500) cluster(folio): ///
probit works $Zyoung if sex==0 & age < 45  // no effect if I raise or lower cut off by 5 years. still very insignificant

scoregof

eststo probworksmalemargyoung: estadd margins, dydx(*)

/* ** younger than 45 without agegroups ** */

/* bootstrap,rep(500) cluster(folio): /// */
/* probit works  age age_s smallcity city bigcity central westcentr northeastcentr northwest primary secondary highschool college_uni  indig married kids_hh wealth    diabetes if sex==0 & age < 45 */

/* scoregof */

/* margins, dydx(*) */


/* LPM for males */ eststo probworksmaleLPM: reg works $Z  if sex==0, cluster(folio)


/* LPM for males older 44 */ eststo probworksmaleLPMold: reg works $Zold if sex==0 & age >= 45, cluster(folio)

/* LPM for males younger 45*/ eststo probworksmaleLPMyoung: reg works $Zyoung if sex==0 & age < 45, cluster(folio)



/* Female regressions */

** with agegroups **
  
bootstrap,rep(500) cluster(folio): ///
probit works $Z if sex==1

eststo scorgof_f:  scoregof

eststo probworksfemalemarg: estadd margins, dydx(*) atmeans









/* ** without agegroups ** */
  
/* bootstrap,rep(500) cluster(folio): /// */
/* probit works age age_sq smallcity city bigcity central westcentr northeastcentr northwest primary secondary highschool college_uni indig married kids_hh wealth    diabetes if sex==1 */

/* scoregof */

/* margins, dydx(*) */


** older than 44 with agegroups **

bootstrap,rep(500) cluster(folio): ///
probit works $Zold if sex==1 & age >= 45  // for females if cut-off lowered to 40 coefficient increases to 5.5 and is highly sign. 
// if cut off 50 years only 10 percent sign. level and about 5 percentage points effect
scoregof

eststo probworksfemalemargold: estadd margins, dydx(*)

/* ** older than 44 without agegroups ** */

/* bootstrap,rep(500) cluster(folio): /// */
/* probit works age age_sq smallcity city bigcity central westcentr northeastcentr northwest primary secondary highschool college_uni indig married kids_hh wealth     diabetes if sex==1 & age >= 45 */

/* scoregof */

/* margins, dydx(*) */


** younger than 45 with agegroups **

bootstrap,rep(500) cluster(folio): ///
probit works $Zyoung if sex==1 & age < 45 // still insignificant if cut-off are change by 5 years

scoregof

eststo probworksfemalemargyoung: estadd margins, dydx(*) atmeans




 
/* LPM for females*/ 
eststo probworksfemaleLPM:  ///
reg works $Z if sex==1, cluster(folio)

/* LPM for females older 44 */ eststo probworksfemaleLPMold: reg works $Zold if sex==1 & age >= 45, cluster(folio)

/* LPM for females younger 45*/ eststo probworksfemaleLPMyoung: reg works $Zyoung if sex==1 & age < 45, cluster(folio)


******Regress bvy age group*******
gen agegroups= 0
replace agegroups = 1 if age25to34 == 1 
replace agegroups = 2 if age35to44 == 1 
replace agegroups = 3 if age45to54 == 1 
replace agegroups = 4 if age55to64 == 1 

forvalues i=0/4 {
forvalues sex=0/1{

bootstrap, rep(250) cluster(folio): quietly probit works $Zage if sex== `sex' & agegroups == `i', ro
eststo scorgof_age`i'`sex': scoregof
display `i'`sex'
eststo worksmarg_age`i'`sex': estadd margins, dydx(*)
}
}


global tablenofact_marg_reduced cells(margins_b(fmt(3)star) margins_se(fmt(3)par)) star(* 0.10 ** 0.05 *** 0.01) stats(ll N, fmt(%9.3f %9.0f) labels("Log likelihood" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) keep(1.diabetes) coeflabels(1.diabetes Diabetes) ///
	 addnote("Marginal effects; Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth, parental education" "* p < 0.1, ** p < 0.05, *** p < 0.01")
 

esttab worksmarg_age30 worksmarg_age31 worksmarg_age40 worksmarg_age41 using probitmarg_agegroups.tex, replace comp ///
 mti("Males" "Females" "Males" "Females") mgroups("45-54" "55-64", pattern(1 0 1 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span}))  ${tablenofact_marg_reduced} 

**************
estwrite * using "savedestimates/test", id() replace
estread "savedestimates/test", id()
// probit

	*reduced

esttab probworksmalemargyoung probworksfemalemargyoung probworksmalemargold probworksfemalemargold using probitmargboth_reduced.tex, replace comp ///
 mti("Males" "Females" "Males" "Females") mgroups(15-44 45-64, pattern(1 0 1 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${tablenofact_marg_reduced}       


global tablenofact_marg cells("margins_b(fmt(3)star) margins_se(fmt(3)par)") star(* 0.10 ** 0.05 *** 0.01) stats(ll N, fmt(%9.3f %9.0f) labels("Log likelihood" "N")) wide booktabs obslast nolz  ///
	 alignment(S S) keep(1.* wealth kids_hh) coeflabels(_cons Constant 1.diabetes Diabetes 1.age25to34 "Age 25--34" 1.age35to44 "Age 35--44" 1.age45to54 "Age 45--54" 1.age55to64 "Age 55--64" ///
	 1.rural Rural 1.smallcity "Small city" 1.city City 1.bigcity "Big city" 1.central Central 1.westcentr Westcentral 1.northeastcentr Northeastcentral ///
	 1.northwest Northwestcentral 1.primary Primary 1.secondary Secondary 1.highschool Highschool 1.college_uni "College or university" ///
  1.indig Indigenous 1.married Married kids_hh "Children (under 15)" wealth Wealth 1.educ_par "Education parents") collabels(none) ///
	 addnote("Marginal effects; Robust standard errors in parentheses" "* p < 0.1, ** p < 0.05, *** p < 0.01")

	*complete


esttab probworksmalemarg probworksfemalemarg using probitmarg.tex, replace comp ///
 mti("Males" "Females") ${tablenofact_marg} 


esttab probworksmalemargold probworksfemalemargold using probitmargold.tex, replace comp ///
 mti("Males" "Females") ${tablenofact_marg} 


esttab probworksmalemargyoung probworksfemalemargyoung using probitmargyoung.tex, replace comp ///
 mti("Males" "Females") ${tablenofact_marg} 


esttab probworksmalemargyoung probworksfemalemargyoung probworksmalemargold probworksfemalemargold using probitmargboth.tex, replace comp ///
 mti("Males" "Females" "Males" "Females") mgroups(15-44 45-64, pattern(1 0 1 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${tablenofact_marg} 
       
       
    // LPM   

global tablenofact b(%9.3f) se(%9.3f)  star(* 0.1 ** 0.05 *** 0.01) stats(r2_a N, fmt(%9.3f %9.0f) labels("R2" "N")) wide booktabs obslast nolz  ///
	 alignment(S S) keep(1.*) coeflabels(_cons Constant 1.diabetes Diabetes 1.age25to34 "Age 25--34" 1.age35to44 "Age 35--44" 1.age45to54 "Age 45--54" 1.age55to64 "Age 55--64" ///
	 1.rural Rural 1.smallcity "Small city" 1.city City 1.bigcity "Big city" 1.central Central 1.westcentr Westcentral 1.northeastcentr Northeastcentral ///
	 1.northwest Northwestcentral 1.primary Primary 1.secondary Secondary 1.highschool Highschool 1.college_uni "College or university" ///
  1.indig Indigenous 1.married Married kids_hh "Children (under 15)" wealth Wealth 1.educ_par "Education parents")



esttab probworksmaleLPM probworksfemaleLPM using LPM.tex, replace comp ///
 mti("Males" "Females") ${tablenofact} 

 
 esttab probworksmaleLPMold probworksfemaleLPMold using LPMold.tex, replace comp ///
 mti("Males" "Females") ${tablenofact} 


 esttab probworksmaleLPMyoung probworksfemaleLPMyoung using LPMyoung.tex, replace comp ///
 mti("Males" "Females") ${tablenofact} 


 esttab probworksmaleLPMyoung probworksfemaleLPMyoung probworksmaleLPMold probworksfemaleLPMold using LPMboth.tex, replace comp ///
 mti("Males" "Females" "Males" "Females") mgroups(15-44 45-64, pattern(1 0 1 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${tablenofact} 
