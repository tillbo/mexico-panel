clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
 global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""


cd $homefolder
**log-file starten**
log using Logs/diab_severity.log, replace

		use "Paneldaten/final.dta", clear
		
		

global Y "lnwage_hr workhrs"

global Z "age age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous i.diabetes"

global wage1 "age age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.secondary i.highschool i.college_uni i.married wealth i.indigenous i.worktype diab_father diab_mother"  // no diabetes included to be used with years since diagnosis

global Zhba1c "age age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.indig i.married kids_hh i.indigenous wealth  i.diab_severity diab_father diab_mother"

global Zdiag "age age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.indig i.married kids_hh i.indigenous wealth  diab_diag"

global controls "age age_sq i.smallcity i.city i.bigcity i.primary i.secondary i.highschool i.college_uni i.married kids_hh i.indigenous wealth "
global controls_nf "age age_sq smallcity city bigcity primary secondary highschool college_uni married kids_hh wealth indigenous "

global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco DistritoFederal Michoacn Morelos NuevoLen Oaxaca Puebla Sinaloa Sonora Veracruz Yucatn"

global Z_interact "i.diabetes##i.diab_severity"
global Z_interact_l "i.years_diabetes_groups##i.diab_severity"
global hba1c_interact "i.diabetes##c.hba1c"

global robust " hypertension heart_disease"

reg bio $controls  sex over45 works diabetes insurance if t == 2, ro


estpost ttest $controls_nf diabetes sex  if t == 2, by(bio)

estpost ttest $controls_nf diabetes sex  if t == 2 & age >= 45, by(bio)
estpost ttest $controls_nf diabetes sex if t == 2 & age < 45, by(bio)

global diab "diabetes diab_severity diab_diag"

global drop "*age* *city*  *central *westcentr *northeastcentr *northwest *primary *secondary *highschool *college_uni *indigenous *married kids_hh wealth *educ_par *indigenous"

egen newid = group(folio_new)

****************************************************************************************************************
************************Investigating the effect of HbA1c on labour market outcomes for 2009********************
****************************************************************************************************************

************************************************************************
/*Effect of self-reported diabetes in whole wave 3, not only subsample*/
************************************************************************

eststo works_diab_bio: reg works $controls $states diabetes sex if t==2, ro

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_bio`sex': reg works $controls $states diabetes if sex== `sex' & t==2, ro
//eststo works_diab_all`sex': estpost margins, dydx(*)
}



***************************************************************************
/*The relationship of HbA1c in a subsample of those with self-reported diabetes*/
***************************************************************************



foreach var of varlist $Y{  // diabetes severity for subsample of those with diabetes
eststo `var': reg `var' $controls sex i.diab_severity if t==2 & diabetes == 1 , ro
		forvalues sex=0/1{
display `sex'
eststo `var'OLS`sex': reg `var' $controls i.diab_severity if sex== `sex' & t==2 & diabetes == 1, ro
		}
	}	
	
***************************************************************************
/*The relationship of self-reported diabetes, HbA1c groups, or diabetes in
general (defined by either self-diagnosis or HbA1c >=6.5*/
***************************************************************************

/*First I estimate a model for comparison only using self-reported diabetes*/

	/*EMPLOYMENT*/
eststo works_diab_bio: reg works $controls $states diabetes sex if t==2 & bio ==1, ro cluster(loc_id)
eststo works_diab_bio_fe: xtreg works $controls $states diabetes sex diab_father diab_mother if t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

//eststo works_diab_all: estpost margins, dydx(*)


forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_bio`sex': reg works $controls $states over45##diabetes if sex== `sex' & t==2 & bio ==1, ro
//eststo works_diab_all`sex': estpost margins, dydx(*)
}
//coefplot works_diab_all || works_diab_all0 || works_diab_all1, keep(*diab*) xline(0) bycoefs byopts(yrescale)  xlabel(, labsize(large)) ylabel( 1 "Pooled" 2 "Males" 3 "Females", labsize(large) nogrid) xtitle("Change in employment probability", size(large))
//graph export marginsplot_diabetes_all_over_threshold.eps, replace


	/*WAGES AND WORKING HOURS*/

foreach var of varlist $Y{
eststo `var'diab_bio: reg `var' $wage1 sex diabetes if t==2 & bio ==1, cluster(folio)
forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_bio`sex': reg `var' $wage1 diabetes if sex== `sex' & t==2 & bio ==1, cluster(folio)
	}
}

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2 N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) /// label coeflables(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth")


esttab works_diab_bio works_diab_bio0 works_diab_bio1 lnwage_hrdiab_bio lnwage_hrdiab_bio0 lnwage_hrdiab_bio1 workhrsdiab_bio workhrsdiab_bio0 workhrsdiab_bio1 using labouroutcomes_diabetes_bio.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(*diab*) label nobase
/*SELF-REPORTED DIABETES AND UNDIAGNOSED DIABETES IN ONE SPECIFICATION*/

/*see what happens if cut-off value is changed to 7 to increase specificity according to study by  Piette (2010).
replace diab_ud = 0 if hba1c < 7 & diabetes == 0  // does not make a qualitative difference for any results!!!
replace diab_hba1c = 0 if hba1c < 7 // does not make a qualitative difference for any results!!!

*/	
	/*EMPLOYMENT*/
eststo works_diab_ud: reg works $controls $states sex diabetes diab_ud diab_father diab_mother obese overweight i.health_sr insurance if t==2 & bio ==1, ro
eststo works_diab_ud_fe: xtreg works $controls $states sex diabetes diab_ud diab_father diab_mother obese overweight i.health_sr insurance if t==2 & bio ==1, i(loc_id) cluster(loc_id) fe

//eststo works_diab_ud: estpost margins, dydx(*diab*)
//marginsplot	
//Test for difference in outcomes by sex using interaction terms
/*probit works $controls diabetes##sex diab_ud##sex if t==2 & bio ==1, cluster(folio)
margins, dydx(*)
testparm diabetes#sex	
testparm diab_ud#sex	
*/	
	forvalues sex=0/1{
	display `sex' `diab'
	eststo works_diab_ud`sex': reg works $controls $states diabetes diab_ud diab_father diab_mother if sex== `sex' & t==2 & bio ==1, ro
	eststo works_diab_ud_`sex'_fe: xtreg works $controls $states diabetes diab_ud diab_father diab_mother if sex== `sex' & t==2 & bio ==1, i(loc_id) cluster(loc_id) fe 
	//eststo works_diab_ud`sex': estpost margins, dydx(*)
	}

coefplot (works_diab_ud, label(Pooled)) (works_diab_ud0 , label(Males)) (works_diab_ud1, label(Females)), keep(*diab*) xline(0) byopts(yrescale) ylabel(, labsize(large)) xlabel(, labsize(large)) ylabel(1 "Diagnosed" 2 "Undiagnosed", labsize(large) angle(90)) xtitle("Change in employment probability", size(large)) legend(rows(1))
graph export marginsplot_diabetes_ud.eps, replace


	/*WAGES AND WORKING HOURS*/
foreach var of varlist $Y{
eststo `var'diab_ud: reg `var' $wage1 sex diabetes diab_ud if t==2 & bio ==1, cluster(folio)
forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud`sex': reg `var' $wage1 diabetes diab_ud if sex== `sex' & t==2 & bio ==1, cluster(folio)
	}
}

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2 N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) /// label coeflables(1.diabetes Diabetes) ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth, parental education")

esttab works_diab_ud works_diab_ud0 works_diab_ud1 lnwage_hrdiab_ud lnwage_hrdiab_ud0 lnwage_hrdiab_ud1 workhrsdiab_ud workhrsdiab_ud0 workhrsdiab_ud1 using labouroutcomes_diagnosed_undiagnosed.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(*diab*)

/*EVERYBODY WITH AN HBA1C > 6.4 */

replace diab_hba1c = 1 if diab_controlled == 1
	/*EMPLOYMENT*/
eststo works_diab_all: reg works $controls $states diab_hba1c sex if t==2 & bio ==1, ro
eststo works_diab_all_fe: xtreg works $controls $states diab_hba1c sex if t==2 & bio ==1, fe i(loc_id)

//eststo works_diab_all: estpost margins, dydx(*)

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_all`sex': reg works $controls $states diab_hba1c if sex== `sex' & t==2 & bio ==1, ro
eststo works_diab_all`sex'_fe: xtreg works $controls $states diab_hba1c if sex== `sex' & t==2 & bio ==1, fe i(loc_id)

//eststo works_diab_all`sex': estpost margins, dydx(*)
}
//coefplot works_diab_all || works_diab_all0 || works_diab_all1, keep(*diab*) xline(0) bycoefs byopts(yrescale)  xlabel(, labsize(large)) ylabel( 1 "Pooled" 2 "Males" 3 "Females", labsize(large) nogrid) xtitle("Change in employment probability", size(large))
//graph export marginsplot_diabetes_all_over_threshold.eps, replace


	/*WAGES AND WORKING HOURS*/

foreach var of varlist $Y{
eststo `var'diab_all: reg `var' $wage1 sex diab_hba1c if t==2 & bio ==1, ro
eststo `var'diab_all_fe: xtreg `var' $wage1 sex diab_hba1c if t==2 & bio ==1, i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_all`sex': reg `var' $wage1 diab_hba1c if sex== `sex' & t==2 & bio ==1, cluster(folio)
eststo `var'diab_all`sex'_fe: reg `var' $wage1 diab_hba1c if sex== `sex' & t==2 & bio ==1, i(loc_id) fe
	}
}


esttab works_diab_all works_diab_all0 works_diab_all1 lnwage_hrdiab_all lnwage_hrdiab_all0 lnwage_hrdiab_all1 workhrsdiab_all workhrsdiab_all0 workhrsdiab_all1 using labouroutcomes_diabetes_hba1c.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(*diab*) label nobase


*********************************************
/*First assessing non-linearity using lpoly*/
*********************************************

foreach var of varlist works{ 
lpoly `var' hba1c if diabetes==1, ci title("`var'") name("`var'") // pooled
graph export lpoly_`var'_hba1c.eps, replace 

forvalues sex = 0/1{  // by gender
	lpoly `var' hba1c if sex == `sex' & diabetes==1, ci title("`var'`sex'") name("`var'`sex'")
	graph export lpoly_`var'_hba1c_`sex'.eps, replace 
	}
}	

*********************************************
/*Using HbA1c as continuous variable*/
*********************************************
reg works $controls hba1c sex, cluster(folio)
//margins, dydx(*)

forvalues sex=0/1{   // Employment
probit works $controls $states hba1c if sex == `sex', cluster(folio)
margins, dydx(*)
}

forvalues sex=0/1{ // wages and working hours
foreach var of varlist $Y { // effects of HbA1C in labour market outcomes
eststo `var'hba1c: reg `var' $wage1 i.sex hba1c, cluster(folio)
	display `sex'
	eststo `var'`sex'hba1c: reg `var' $wage1 hba1c if sex== `sex', cluster(folio)
	}	

}


/*
*********************************************
/*Using HbA1c splines*/
*********************************************
mkspline hba1c1 6.4 hba1c2 8.5 hba1c3 12 hba1c4= hba1c
mkspline hba1c_spline = hba1c, cubic knots(5 6.4 8.5  12)

probit works $controls $states sex hba1c1 - hba1c4, cluster(folio)
margins, dydx(*)

forvalues sex=0/1{   // Employment
probit works $controls $states hba1c1 - hba1c4 if sex == `sex', cluster(folio)
margins, dydx(*)
}


//linear
foreach var of varlist $Y{ // effects of HbA1C splines in labour market outcomes
eststo `var'hba1c_spline: reg `var' $wage1 sex hba1c1 - hba1c4 if t==2, cluster(folio)
	forvalues sex=0/1{
	display `sex'
	eststo `var'`sex'hba1c_spline: reg `var' $wage1 hba1c1 - hba1c4 if sex== `sex' & t==2, cluster(folio)
	}	

}

//cubic

/*Employment*/
probit works $controls sex hba1c_spline*, cluster(folio)
margins, dydx(*)

forvalues sex=0/1{   // Employment
display `sex'
probit works $controls hba1c_spline* if sex == `sex', cluster(folio)
margins, dydx(*)
}

/*Wages and working hours*/

foreach var of varlist $Y{ // effects of HbA1C cubic splines in labour market outcomes
eststo `var'hba1c_spline_c: reg `var' $wage1 hba1c_spline* if t==2, cluster(folio)
	forvalues sex=0/1{
	display `sex'
	eststo `var'`sex'hba1c_spline_c: reg `var' $wage1 hba1c_spline* if sex== `sex' & t==2, cluster(folio)
	}	

}

*********************************************
/*Interacting HbA1c with diabetes diagnosis*/
*********************************************

/*Employment*/

	eststo workshba1c: probit works `var' $controls diabetes sex if t==2 & bio == 1, ro


eststo workshba1c_ia: reg works $controls $states sex $hba1c_interact if t==2, ro

	forvalues sex=0/1{
	display `sex'
	eststo works`sex'hba1c: reg works `var' $controls $states $hba1c_interact if sex== `sex' & t==2, ro
	}	

/*Wages and working hours*/

foreach var of varlist $Y{ // effects of HbA1C in labour market outcomes interacted with diagnosis
eststo `var'hba1c_ia: reg `var' $wage1 sex $hba1c_interact if t==2, cluster(folio)
	forvalues sex=0/1{
	display `sex'
	eststo `var'`sex'hba1c: reg `var' $wage1 $hba1c_interact if sex== `sex' & t==2, cluster(folio)
	}	

}

****************************************************
/*Interacting HbA1c groups with diabetes diagnosis*/
****************************************************
/*Employment*/
replace age = edad
replace age_sq = edad^2

eststo worksia: reg works $controls sex $Z_interact if t==2, ro
eststo worksia_fe: xtreg works $controls $states sex $Z_interact if t==2, fe i(loc_id)

		forvalues sex=0/1{
display `sex'
eststo works`sex'ia: reg works $controls $Z_interact if sex== `sex' & t==2, ro
eststo works`sex'ia_fe: xtreg works $controls $states $Z_interact if sex== `sex' & t==2, fe i(loc_id)

	}


/*Wages and working hours*/

foreach var of varlist $Y{   // interaction terms between diabetes diagnosis and diabetes severity groups
eststo `var'ia: reg `var' $controls $states sex $Z_interact if t==2, ro
eststo `var'ia_fe: xtreg `var' $controls $states sex $Z_interact if t==2, fe i(loc_id)
	forvalues sex=0/1{
display `sex'
eststo `var'`sex'ia: reg `var' $controls $states $Z_interact if sex== `sex' & t==2, ro
eststo `var'`sex'ia_fe: xtreg `var' $controls $states $Z_interact if sex== `sex' & t==2, fe i(loc_id)

	}
}	

****************************************************
/*Interacting HbA1c groups with diabetes length*/
****************************************************

foreach var of varlist $Y{   // interaction terms between diabetes_severity and diabetes length
eststo `var'ia_l: reg `var' $controls sex $Z_interact_l if t==2, ro
		forvalues sex=0/1{
display `sex'
eststo `var'`sex'ia_l: reg `var' $controls $Z_interact_l if sex== `sex' & t==2, ro
	}
}	




/*SELF-REPORTED DIABETES, UNDIAGNOSED DIABETES AND HBA1C IN ONE SPECIFICATION*/
	
	/*EMPLOYMENT*/
probit works $controls sex diabetes diab_ud hba1c if t==2 & bio ==1, cluster(folio)
eststo works_diab_ud: estpost margins, dydx(diabetes diab_ud) at(hba1c=(4(1)14))
marginsplot

forvalues sex=0/1{
display `sex' `diab'
probit works $controls diabetes diab_ud hba1c if sex== `sex' & t==2 & bio ==1, ro
//eststo works_diab_ud`sex': estpost margins, dydx(*)
margins, dydx(diabetes) at(hba1c=(4(2)14))
marginsplot, name(hba1c`sex', replace)

}
	
	/*WAGES AND WORKING HOURS*/
foreach var of varlist $Y{
eststo `var'diab_ud: reg `var' $wage1 sex diabetes diab_ud  hba1c if t==2 & bio ==1, cluster(folio)
forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud`sex': reg `var' $wage1 diabetes diab_ud hba1c if sex== `sex' & t==2 & bio ==1, cluster(folio)
	}
}

/*SELF-REPORTED DIABETES, UNDIAGNOSED DIABETES AND HBA1C IN ONE SPECIFICATION USING INTERACTION OF HBA1C WITH DIABETES AND UNDIAGNOSED DIABETES*/

	/*EMPLOYMENT*/
	

probit works $controls i.diabetessev##sex i.diab_udsev##sex if t==2 & bio ==1, ro
margins, dydx( i.diabetessev i.diab_udsev)

testparm i.diabetessev#sex
testparm i.diab_udsev#sex

gen diabetes_hba1c = diabetes*hba1c

*/

// using hba1c dummies
reg works $controls sex i.diabetessev i.diab_udsev if t==2 & bio ==1, ro
eststo works_diab_ud_sev: estpost margins, dydx(*)


//testing use of splines. no effects are found
/*qui probit works $controls sex diagn_sp_hba1c3 diagn_sp_hba1c4 diagn_sp_hba1c5 diagn_sp_hba1c6 ud_sp_hba1c3 ud_sp_hba1c4 ud_sp_hba1c5 ud_sp_hba1c6 if t==2 & bio ==1, ro
eststo works_diab_ud: estpost margins, dydx(*)*/



forvalues sex=0/1{
display `sex' `diab'
reg works $controls i.diabetessev i.diab_udsev diab_ud if sex== `sex' & t==2 & bio ==1, ro
//eststo works_diab_ud_sev`sex': estpost margins, dydx(diabetessev diab_udsev)
}

coefplot (works_diab_ud_sev, label(Pooled)) (works_diab_ud_sev0 , label(Males)) (works_diab_ud_sev1, label(Females)), keep(*diab*) xline(0) xtitle("Changes in employment probability", size(large))  ///
omitted  legend(rows(1)) ///
 heading(1.diabetessev = "{bf:Diagnosed}" 1.diab_udsev = "{bf:Undiagnosed}" ///
 , labcolor(orange))
graph export marginsplot_diabetes_ud_sev.eps, replace

	
	/*WAGES AND WORKING HOURS*/
foreach var of varlist $Y{
reg `var' $wage1 i.diabetessev##sex i.diab_udsev##sex if t==2 & bio ==1, ro
testparm i.diabetessev#sex
testparm i.diab_udsev#sex
eststo `var'diab_ud: reg `var' $wage1 sex i.diabetessev i.diab_udsev if t==2 & bio ==1, ro
forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud`sex': reg `var' $wage1 i.diabetessev i.diab_udsev if sex== `sex' & t==2 & bio ==1, ro
	}
}

  global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2 N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none)   ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth.")

esttab works_diab_ud works_diab_ud0 works_diab_ud1 lnwage_hrdiab_ud lnwage_hrdiab_ud0 lnwage_hrdiab_ud1 workhrsdiab_ud workhrsdiab_ud0 workhrsdiab_ud1 using labouroutcomes_diagnosed_undiagnosed_hba1c.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log hourly wages" "Weekly working hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(*diab*) label nobase



	foreach diab in $diab{
eststo works`diab': reg works $controls sex i.`diab' if t==2 & bio ==1, cluster(folio)
		forvalues sex=0/1{
display `sex' `diab'
eststo works`sex'`diab': reg works $controls i.`diab' if sex== `sex' & t==2 & bio ==1, cluster(folio)
		}
}

	
	foreach var of varlist $Y{ // effects of self reported diabetes, diabetes severity (diagnosed and undiagnosed), and diabetes (self-reported + undiagnosed)
	foreach diab in $diab{
eststo `var'`diab': reg `var' $controls sex i.`diab' if t==2 & bio ==1, cluster(folio)
		forvalues sex=0/1{
display `sex' `diab'
eststo `var'`sex'`diab': reg `var' $controls i.`diab' if sex== `sex' & t==2 & bio ==1, cluster(folio)
		}
	}	

}





  global table_reduced_years b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) stats(r2 N, fmt(%9.3f %9.0f) labels("R2" "N")) booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) keep(*diab*) label nobaselevels ///
	 addnote("Robust standard errors in parentheses" "Other control variables: age, region, urban, education, indigenous, marital status, children, wealth, parental education")



/*MATCHING DIAGNOSED AND UNDIAGNOSED*/


teffects psmatch (works) (diagnosis age hba1c sex bmi diab_mother diab_father insurance i.high_altitude hypertension i.health_sr)  // new matching command with correct SE

gen worksbio = works if bio ==1
heckman worksbio  $controls sex  if t==2, select(bio over45) 	

global controls "age age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh i.indigenous wealth "

recode bio (1=0) (0=1), gen(bio_inv)

set seed 123456
gen u=uniform()
sort u
psmatch2 bio_inv  over45 if t==2, out() logit

tab diabetes if t == 2 & age > 44
tab diabetes if t == 2 & age > 44 & bio == 1


tab diabetes [fweight=_weight] if t == 2
tab diabetes [fweight=_weight] if t == 2 & bio  == 1
tab diabetes if t == 2 & bio == 1


reg works $controls sex diabetes diab_ud[fweight=_weight] if t==2 & bio == 1, ro

probit works $controls diabetes sex [fweight=_weight] if t==2 & bio == 1, ro   // sign neg effect


reg works $controls diabetes sex [fweight=_weight] if t==2 & bio == 1 & sex == 0 , ro   // sign neg effect
reg works $controls diabetes sex [fweight=_weight] if t==2 & bio == 1 & sex == 1 , ro  // sign. neg. effect
reg works $controls sex diabetes diab_ud [fweight=_weight] if t==2 & bio == 1, ro  // similar as to what found earlier, only diagnosed diabetes sign.
reg works $controls sex insurance diab_diag [fweight=_weight] if t==2 & bio == 1, ro  // non neg. effect for men, and only weak relationship for pooled and male sample
reg works $controls sex insurance diab_diag [fweight=_weight] if t==2 & bio == 1 & sex==0, ro  // non neg. effect for men, and only weak relationship for pooled and male sample


//ALSO USE MEASURED BLOODPRESSURE FOR MATCHING VAR SA12_21 AND 22


/*
/*Table for hba1c as continuous variable*/
esttab workshba1c works0hba1c works1hba1c lnincome_adjhba1c lnincome_adj0hba1c lnincome_adj1hba1c workhrs_normalhba1c workhrs_normal0hba1c workhrs_normal1hba1c using labouroutcomes_hba1c.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(hba1*)

/*Table for hba1c linear splines*/

esttab workshba1c_spline works0hba1c_spline works1hba1c_spline lnincome_adjhba1c_spline lnincome_adj0hba1c_spline lnincome_adj1hba1c_spline workhrs_normalhba1c_spline workhrs_normal0hba1c_spline workhrs_normal1hba1c_spline using labouroutcomes_hba1c_spline.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(hba1*)

 /*Table for hba1c groups (dummy variables)*/
      
esttab worksdiab_severity worksOLS0diab_severity worksOLS1diab_severity lnwage_adjdiab_severity lnwage_adjOLS0diab_severity lnwage_adjOLS1diab_severity workinghrsdiab_severity workinghrsOLS0diab_severity workinghrsOLS1diab_severity using labouroutcomes.tex, append comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* 2.diab* 3.diab*)
       
       

esttab worksdiab_diag worksOLS0diab_diag worksOLS1diab_diag lnwage_adjdiab_diag lnwage_adjOLS0diab_diag lnwage_adjOLS1diab_diag workinghrsdiab_diag workinghrsOLS0diab_diag workinghrsOLS1diab_diag using labouroutcomes.tex, append comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*)
       
       esttab works worksOLS0 worksOLS1 lnwage_adj lnwage_adjOLS0 lnwage_adjOLS1 workinghrs workinghrsOLS0 workinghrsOLS1 using labouroutcomes.tex, append comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* 2.diab* 3.diab*)

esttab worksia works0ia works1ia lnwage_adjia lnwage_adj0ia lnwage_adj1ia workinghrsia workinghrs0ia workinghrs1ia using labouroutcomes_severity.tex, replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Log monthly wages" "Monthly work hours", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  drop($drop 0.*)



****************************************************************************
/******************AGE AT DIAGNOSIS****************************************/
****************************************************************************


probit works $controls i.age_diagnosis_group sex if t == 2, cluster(folio)
probit works $controls i.age_diagnosis_group if t == 2 & sex == 0, cluster(folio)
probit works $controls i.age_diagnosis_group if t == 2 & sex == 1, cluster(folio)

margins, dydx(*)
****************************************************************************
/******************IV TESTS************************************************/
****************************************************************************

ivreg2 works $controls_nf sex (diab_ud= diab_father diab_mother) if diabetes==0, endog(diab_ud) ro  ffirst first
ivreg2 works $controls_nf (diab_diag= diab_father diab_mother) if bio ==1 & sex==0, endog(diab_diag) ro  ffirst first
ivreg2 works $controls_nf (diab_diag= diab_father diab_mother) if bio ==1 & sex==1, endog(diab_diag) ro  ffirst first
ivreg2 works $controls_nf (diabetes= diab_father diab_mother) if sex == 0, endog(diabetes) ro  ffirst first
ivreg2 works $controls_nf (diabetes= diab_father diab_mother) if sex == 1, endog(diabetes) ro  ffirst first

biprobit (works = $controls_nf sex diabetes) (diabetes = $controls_nf sex diab_father diab_mother), ro
biprobit (works = $controls_nf diabetes) (diabetes = $controls_nf diab_father diab_mother) if sex== 0, ro
biprobit (works = $controls_nf diabetes) (diabetes = $controls_nf diab_father diab_mother) if sex== 1, ro

biprobit (works = $controls_nf sex diab_diag) (diab_diag = $controls_nf sex diab_father diab_mother), ro
biprobit (works = $controls_nf diab_diag) (diab_diag = $controls_nf diab_father diab_mother) if sex== 0 & bio ==1, ro
biprobit (works = $controls_nf diab_diag) (diab_diag = $controls_nf diab_father diab_mother) if sex== 1 & bio ==1, ro

biprobit (works = $controls_nf sex diab_ud) (diab_ud = $controls_nf sex diab_father diab_mother) if diabetes ==0, ro
biprobit (works = $controls_nf diab_ud) (diab_ud = $controls_nf diab_father diab_mother) if sex== 0 & diabetes==0, ro
biprobit (works = $controls_nf diab_ud) (diab_ud = $controls_nf diab_father diab_mother) if sex== 1 & diabetes==0, ro






reg works $Z, ro
reg works $Zhba1c, ro
reg works $Zdiag, ro
reg works
