**log-file starten**


    	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/09/master_do_09.do"					//dropping unneeded variabels from chronic disease dataset, summing health expenditures and creating variabel for diab related diseases
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/master_do_05.do"
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/master_do_02.do"					//dropping unneeded variabels from chronic disease dataset, summing health expenditures and creating variabel for diab related diseases
clear matrix
set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""


cd $homefolder


		use "Daten 02/master_merged.dta", clear

log using Logs/master_merge.log, replace

*merging '05 and '02 datasets

append using "Daten 05/master_merged.dta", force generate(t)
append using "Daten 09/master_merged.dta", force 

**t-->time variable

	replace t = 2 if wave3 == 1
	

**setting panel variables
	destring pid_link, replace  //pid_link has to be numeric, for that destring
	drop if pid_link==. //dropping observations with no observations
	xtset pid_link t
	
**dropping var variables

	drop var1 var2 var3


	
save "Paneldaten/master_merged.dta", replace

log close
exit
