clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""

local dir "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Dofiles/Tables"

cd $homefolder
**log-file starten**
*log using Logs/panel_altitude.log, replace

		use "Paneldaten/final.dta", clear

gen fatperc = 76 - (20 * height_m/waist_m) if sex == 1 & height_m <. & waist_m < .
replace fatperc = 64 - (20 * height_m/waist_m) if sex == 0 & height_m <. & waist_m < .

gen fat_check = 20 * height_m/waist_m if height_m <. & waist_m < .



twoway lpoly diabetes fatperc  if sex == 0, fcolor(none) alpattern(dash_dot)  || lpoly diabetes bmi if sex == 0, ///
name(works, replace) ///
   xlabel(#15)  lpattern(dash) alpattern(shortdash) fcolor(none)  // fintensity(inten20)

twoway lpoly diabetes fatperc  if sex == 1, fcolor(none) alpattern(dash_dot)  || lpoly diabetes bmi if sex == 1, ///
name(works, replace) ///
   xlabel(#15)  lpattern(dash) alpattern(shortdash) fcolor(none) || if bmi <60  // fintensity(inten20)


graph export "$figures/lpoly_works_diabetesduration.eps", replace 

****************************************************************************************
/*****************************Altitude regression**************************************/
****************************************************************************************
global controls "age i.smallcity i.city i.bigcity south_southeast central westcentr northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous"

global controls_nf "age age_sq smallcity city bigcity  primary secondary highschool college_uni married  kids_hh wealth indigenous"

global nofix "age1 age2 age3 age4 wealth kids_hh diabetes t2 t3 married bmi"

global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco DistritoFederal Michoacn Morelos NuevoLen Oaxaca Puebla Sinaloa Sonora Veracruz Yucatn"


global fe " demean_age demean_age_sq demean_smallcity demean_city demean_bigcity demean_south_southeast demean_central demean_westcentr demean_northwest  demean_secondary demean_highschool demean_college_uni demean_married demean_kids_hh demean_insurance demean_wealth demean_indigenous demean_t2 demean_t3 demean_BajaCaliforniaSur demean_Coahuila demean_Durango demean_Guanajuato demean_Jalisco demean_DistritoFederal demean_Michoacn demean_Morelos demean_NuevoLen demean_Oaxaca demean_Puebla demean_Sinaloa demean_Sonora demean_Veracruz demean_Yucatn demean_high_altitude2 demean_high_altitude3" 
global bw "mean_age mean_age_sq mean_smallcity mean_city mean_bigcity mean_south_southeast mean_central mean_westcentr mean_northwest  mean_secondary mean_highschool mean_college_uni mean_married mean_kids_hh mean_insurance mean_wealth mean_indigenous mean_t2 mean_t3 mean_BajaCaliforniaSur mean_Coahuila mean_Durango mean_Guanajuato mean_Jalisco mean_DistritoFederal mean_Michoacn mean_Morelos mean_NuevoLen mean_Oaxaca mean_Puebla mean_Sinaloa mean_Sonora mean_Veracruz mean_Yucatn mean_high_altitude2 mean_high_altitude3"


reg diabetes c.altitud##c.altitud rural age1 age2 age3 age4 age5 smallcity city bigcity primary secondary highschool college_uni bmi sex, ro cluster(mpio)
reg diabetes altitud age1 age2 age3 age4 age5 smallcity city bigcity primary secondary highschool college_uni, cluster(pid_link) 
reg hba1c c.altitud##c.altitud age1 age2 age3 age4 age5 smallcity city bigcity primary secondary highschool college_uni diabetes, ro 
reg hba1c c.altitud age age_sq smallcity city bigcity primary secondary highschool college_uni diabetes, cluster(ent) 


/*Regressions to investigate relationship between altitude and overweight, fat percentage and diabetes
Findings indicate negative association with fat percentage and bmi for men but not women. No relationship with self-reported
diabetes. Positive relationship with measured diabetes for men but not for wome*/




reg diab_hba1c   rural age age_sq smallcity city bigcity primary secondary highschool college_uni sex $states i.high_altitude works insurance, ro
reg diab_hba1c rural age age_sq smallcity city bigcity primary secondary highschool college_uni $states i.high_altitude works insurance if sex == 0, ro
reg diab_hba1c rural age age_sq smallcity city bigcity primary secondary highschool college_uni $states i.high_altitude works insurance if sex == 1, ro


xtlogit diabetes i.high_altitude $controls sex i.t insurance wealth works insurance
xtreg diabetes i.high_altitude sex age wealth smallcity city bigcity primary secondary highschool college_uni $states, ro


reg obese i.high_altitude $controls sex i.t , cluster(pid_link) ro
bysort sex: xtlogit obese i.high_altitude $controls sex i.t
bysort sex: reg obese i.high_altitude $controls sex if t==2 , ro


reg diabetes i.high_altitude $controls i.t if sex == 0 , cluster(mpio)


reg diabetes i.high_altitude $controls bmi i.t if sex == 1 , cluster(mpio)
reg works i.high_altitude diabetes $controls bmi i.t if sex == 1 , cluster(mpio)

/*BMI*/
xtreg bmi high_altitude2 high_altitude3 $controls_nf $states if sex == 0 ,  re

xtreg bmi high_altitude2 high_altitude3 $controls_nf $states if sex == 1 ,  re ro

/*Fat percentage*/

xtreg fatperc high_altitude2##sex high_altitude3##sex $controls_nf $states & fatperc > 5,  re ro

xtreg fatperc high_altitude2 high_altitude3 $controls_nf $states if sex == 0 & fatperc > 5,  re ro

xtreg fatperc high_altitude2 high_altitude3 $controls_nf $states if sex == 1 & fatperc > 5,  re ro

xtreg fatperc c.altitud##sex $controls_nf $states & fatperc > 5,  re ro

xtreg fatperc altitud $controls_nf $states if sex == 0 & fatperc > 5,  re ro

xtreg fatperc altitud $controls_nf $states if sex == 1 & fatperc > 5,  re ro

xthybrid fatperc high_altitude2 high_altitude3 $controls_nf $states if sex == 0 & fatperc > 5,  clusterid(pid_link) full

/*obese based on bmi*/

xtreg obese high_altitude2 high_altitude3 $controls_nf $states if sex == 0 ,  re ro

xtreg obese high_altitude2 high_altitude3 $controls_nf $states if sex == 1 ,  re ro

/*Diabetes*/

xtreg diabetes i.high_altitude $controls_nf $states i.sex i.t obese overweight works, ro fe

xtreg diabetes $fe $bw, ro

xtreg diabetes i.high_altitude $controls_nf $states i.sex i.t obese overweight works, ro fe
xtgee diabetes i.high_altitude $controls_nf $states i.sex i.t obese overweight works, family(binomial) link(logit) corr(exchangeable) vce(robust)
margins, dydx(i.high_altitude)


/*employment*/
xtreg works c.fatperc##sex i.high_altitude $controls_nf $states i.t, ro fe
xtreg works c.fatperc i.high_altitude $controls_nf $states i.t if sex == 0, ro fe
xtreg works c.fatperc i.high_altitude $controls_nf $states i.t if sex == 1, ro fe


reg hba1c fatperc  $controls_nf $states  if sex == 0 & fatperc > 5, ro
xthybrid diabetes fatperc  $controls_nf $states  if sex == 0 & fatperc > 5, clusterid(pid_link) full
xthybrid diabetes fatperc  $controls_nf $states  if sex == 1 & fatperc > 5, clusterid(pid_link) full

xtreg diabetes overweight obese $controls_nf $states i.t if sex == 1 & fatperc > 5, ro


xtreg diabetes i.high_altitude fatperc bmi $controls i.t if sex == 1 &fatperc > 5, ro fe
xtreg diabetes i.high_altitude  $controls if sex == 0, cluster(pid_link)
xtreg diabetes i.high_altitude  $controls c.bmi if sex == 1, cluster(pid_link)

xtreg works i.high_altitude $controls i.diabetes wealth i.t sex , ro
xtreg works i.high_altitude $controls i.diabetes i.t if sex == 0 , ro

xtreg lnincome_mth i.high_altitude $controls bmi i.diabetes wealth b(freq).ent i.t i.worktype if sex == 0 , cluster(pid_link)

mundlak works high_altitude2 high_altitude3 $controls_nf wealth bmi diabetes if sex == 1, full use($nofix wealth ent*)
mundlak works high_altitude2 high_altitude3 $controls_nf bmi wealth ent* diabetes if sex == 0, full use($nofix)

xtlogit diabetes high_altitude $controls sex bmi i.t, re 

ivreg2 works $controls_nf t2 t3 sex ent2-ent28 insurance (bmi = high_altitude2 high_altitude3) , ro ffirst endog(bmi) first 
ivreg2 works $controls_nf t2 t3 sex $states insurance bmi health_sr2 health_sr3 health_sr4 health_sr5 (diabetes = high_altitude2 high_altitude3) , ro ffirst endog(diabetes) first 






   
twoway lpolyci fatperc altitud if sex ==0
twoway lpolyci fatperc altitud if sex ==1


twoway lpolyci bmi altitud, by(sex)




