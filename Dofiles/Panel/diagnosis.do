clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
* global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""


cd $homefolder
**log-file starten**
log using Logs/diab_diagnosis.log, replace

		use "Paneldaten/final.dta", clear

global Y "diagnosis diab_severity"

global Z "i.age25to34 i.age35to44 i.age45to54 i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous i.educ_par i.sex"

global controls "i.age25to34 i.age35to44 i.age45to54 i.age55to64 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh i.indigenous wealth i.educ_par"

global Z_interact "i.diabetes##i.diab_severity"
global Z_interact_l "i.years_diabetes_groups##i.diab_severity"


global diab "diabetes diab_severity diab_diag"


foreach var of varlist $Y{   // interaction terms between diabetes diagnosis and diabetes severity
eststo `var'ia: reg `var' sex $Z if t==2, ro
		forvalues sex=0/1{
display `sex'
eststo `var'`sex'ia: reg `var' $Z if sex== `sex' & t==2, ro
	}
}	
