
/*DO-FILE WITH ALL MODELS THAT WILL APPEAR IN FINAL PAPER*/

clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""

global table "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Tables"
global estimates "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Estimates"


cd $homefolder
**log-file starten**


		use "Paneldaten/final.dta", clear

log using Logs/within_between_variation.log, replace




	/*PANEL DATA*/
global wage "age_ini age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous i.worktype i.insurance i.survey_year"
global wage_nf "age_ini age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous insurance survey_year2-survey_year7 selfemployed agricultural"

global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco Morelos Michoacn NuevoLen Oaxaca Puebla Sinaloa Yucatn Veracruz"
global controls "age_ini age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh i.indigenous wealth i.survey_year"
global controls_nf "age_ini age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh indigenous wealth survey_year2-survey_year7"

global Y1 "lnwage_hr workhrs"

mundlak works diabetes $controls_nf, use(diabetes) keep hybrid full

mundlak diabetes works  $controls_nf, use(works) keep hybrid full

/*creating means  and demeans for our estimates*/

global controls "age1 age2 age3 age4 age5 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous"
global years "i.t"

//quietly reg status $controls diabetes
//drop if e(sample)~=1									/* drop observations missing on any variables will use */


drop *mean_*


/*generate mean and demeaned variables to use in within-between estimator*/
global all "diabetes years_diabetes diab_y* age_diagnosis* type1 type2 sex t1 t2 t3 age age_sq rural smallcity city bigcity south_southeast central westcentr northwest northeastcentr noeducation primary secondary highschool college_uni married  kids_hh wealth indigenous nonagricultural agricultural selfemployed hba1c"

foreach var of varlist $all {
bysort pid_link: egen mean_`var' = mean(`var')
}

/* using the mean variables to find inconsisitent reports for sex and indigenous status and correcting it as best as possible.
For sex I use information from alternative variable sa01 which also reports sex info. For indigenous I assume that if person has reported being indigeneous
at least once across the survey that they are part of indigenous group because I guess because of stigma they are generally less likely to be part of an indigneous group
which causes measurement error*/

replace indigenous = 1 if mean_indigenous > 0 & mean_indigenous <.
replace mean_indigenous = 1 if indigenous == 1
replace sex = sa01 if mean_sex <1 & mean_sex >0 & sa01 <.
drop mean_sex
bysort pid_link: egen mean_sex = mean(sex)
replace sex = 1 if mean_sex >= 0.5 & mean_sex <.
replace sex = 0 if mean_sex < 0.5
drop mean_sex
bysort pid_link: egen mean_sex = mean(sex)


foreach var of varlist $all {
gen demean_`var' = `var' - mean_`var'
}


global fe "demean_diabetes demean_age demean_age_sq demean_smallcity demean_city demean_bigcity demean_south_southeast demean_central demean_westcentr demean_northwest  demean_secondary demean_highschool demean_college_uni demean_married demean_kids_hh demean_wealth demean_indigenous demean_t2 demean_t3"
global bw "mean_diabetes mean_age mean_age_sq mean_smallcity mean_city mean_bigcity mean_south_southeast mean_central mean_westcentr mean_northwest  mean_secondary mean_highschool mean_college_uni mean_married mean_kids_hh mean_wealth mean_indigenous mean_t2 mean_t3"
