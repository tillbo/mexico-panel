clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""
global homefolder ""\\VBOXSVR\till\Dropbox\PhD\Diabetes Mexico\article\Data""


cd $homefolder
**log-file starten**
log using Logs/gsem_mlogit_interactive.log, replace

		use "Paneldaten/final.dta", clear
		
****************************************************************************
***********USING GSEM FOR MLOGIT ON PANEL***********************************
****************************************************************************


		

global controls "age1 age2 age3 age4 age5 i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.primary i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous i.educ_par"
global years "i.t"

quietly reg status $controls diabetes
drop if e(sample)~=1									/* drop observations missing on any variables will use */


drop *mean_*

/*generate mean and demeaned variables to use in within-between estimatoe*/
global all "diabetes years_diabetes diab_y* age_diagnosis* type1 type2 sex t1 t2 t3 age age_sq rural smallcity city bigcity south_southeast central westcentr northwest northeastcentr noeducation primary secondary highschool college_uni married  kids_hh wealth indigenous nonagricultural agricultural selfemployed hba1c"

foreach var of varlist $all {
bysort pid_link: egen mean_`var' = mean(`var')
}

/* using the mean variables to find inconsisitent reports for sex and indigenous status and correcting it as best as possible.
For sex I use information from alternative variable sa01 which also reports sex info. For indigenous I assume that if person has reported being indigeneous
at least once across the survey that they are part of indigenous group because I guess because of stigma they are generally less likely to be part of an indigneous group
which causes measurement error*/

replace indigenous = 1 if mean_indigenous > 0 & mean_indigenous <.
replace mean_indigenous = 1 if indigenous == 1
replace sex = sa01 if mean_sex <1 & mean_sex >0 & sa01 <.
drop mean_sex
bysort pid_link: egen mean_sex = mean(sex)
replace sex = 1 if mean_sex >= 0.5 & mean_sex <.
replace sex = 0 if mean_sex < 0.5
drop mean_sex
bysort pid_link: egen mean_sex = mean(sex)


foreach var of varlist $all {
gen demean_`var' = `var' - mean_`var'
}


global fe "demean_diabetes demean_age demean_age_sq demean_smallcity demean_city demean_bigcity demean_south_southeast demean_central demean_westcentr demean_northwest  demean_secondary demean_highschool demean_college_uni demean_married demean_kids_hh demean_wealth demean_indigenous demean_t2 demean_t3"
global bw "mean_diabetes mean_age mean_age_sq mean_smallcity mean_city mean_bigcity mean_south_southeast mean_central mean_westcentr mean_northwest  mean_secondary mean_highschool mean_college_uni mean_married mean_kids_hh mean_wealth mean_indigenous mean_t2 mean_t3"



/*USING GSEM TO ESTIMATE MULTINOMIAL LOGIT MODEL FOR PANEL DATA AS SHOWN ON http://www.stata.com/stata-news/news29-2/xtmlogit/ */

eststo xtmlog: gsem (1.status <- $fe $bw *mean_sex RI1[pid_link]) ///
     (2.status <- $fe $bw *mean_sex RI2[pid_link]) ///
     (3.status <- $fe $bw *mean_sex RI3[pid_link]) , mlogit latent()
estwrite * using "savedestimates/gsem_mlog", id() replace
estread xtmlog using "savedestimates/gsem_mlog", id()
eststo xtmlog_m_1: estadd margins, predict(fixedonly outcome(1.status))
estread xtmlog using "savedestimates/gsem_mlog", id()
eststo xtmlog_m_2: estadd margins, predict(fixedonly outcome(2.status))
estread xtmlog using "savedestimates/gsem_mlog", id()
eststo xtmlog_m_3: estadd margins, predict(fixedonly outcome(3.status))



forvalues sex=0/1{
eststo xtmlog`sex': gsem (1.status <- $controls diabetes $years i.sex RI1[pid_link]) ///
     (2.status <- $controls diabetes $years i.sex RI2[pid_link]) ///
     (3.status <- $controls diabetes $years i.sex RI3[pid_link]) if sex == `sex', mlogit latent()
estwrite * using "savedestimates/gsem_mlog", id() replace
estread xtmlog`sex' using "savedestimates/gsem_mlog", id()
eststo xtmlog_m`sex'_1: estadd margins, predict(fixedonly outcome(1.status))
estread xtmlog`sex' using "savedestimates/gsem_mlog", id()
eststo xtmlog_m`sex'_2: estadd margins, predict(fixedonly outcome(2.status))
estread xtmlog`sex' using "savedestimates/gsem_mlog", id()
eststo xtmlog_m`sex'_3: estadd margins, predict(fixedonly outcome(3.status))
}
estwrite * using "savedestimates/gsem_mlog", id() replace

/*MLwin*/
global MLwiN_path "/home/till/.wine/drive_c/Program Files (x86)/MLwiN v2.33/i386/mlwin.exe"
global MLwiN_path "C:\Program Files (x86)\MLwiN v2.34\i386\mlwin.exe"

egen newid = group(pid_link)

gen cons=1

sort t newid
runmlwin status cons, level2(t: cons) ///
            level1(newid) discrete(distribution(multinomial) link(mlogit) ///
            denom(cons) basecategory(0)) mlwinsettings(size(536000) levels(5) columns(1500) variables(300) tempmat)


runmlwin use4 cons lc1 lc2 lc3plus, level2(district: cons)
            level1(woman) discrete(distribution(multinomial) link(mlogit)
            denom(cons) basecategory(4)) nopause


log close

