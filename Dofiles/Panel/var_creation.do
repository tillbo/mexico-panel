set more off
capture log close

global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""

cd $homefolder

log using "Logs/var_creation_panel.log", replace

use "Paneldaten/master_merged.dta", clear

/*Adding altitude of municipalities of Mexico based on www.inegi.org.mx/geo/contenidos/geoestadistica/consulta_localidades.aspx*/
do "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Dofiles/Panel/Altitud.do" // run altitude do file
gen high_altitude = 0 if altitud < 500
replace high_altitude = 1 if altitud <1500 & altitud >=500
replace high_altitude = 2 if altitud >=1500 & altitud <.
label define altitude 0 "<500 meters" 1 "500-1500 meters" 2 ">=1500 meters"
label value high_altitude altitude
dummies high_altitude

	clonevar age = sa03 
	label variable age "Age"
	// clean age_variable
	replace age = ls02_2 if sa03 ==. & ls02_2 <.
	replace age = edad if ls02_2 ==. & sa03 ==.
	gen age_strange = 1 if l.age > age & l.age <.  // find variables were age in previous wave > than current age
	bysort pid_link: egen age_strange_all = max(age_strange)  // tag all observations of pid_links were mistakes were found	


drop if  age<15 | age>64

*recoding variables from 1=Yes, 3=No to 1=Yes, 0=No

	foreach var of varlist  ec01a-ec02i {  // ec01a is self.reported diabetes	
	replace `var'=0 if `var'==3
	}

*value labels
	label define yesno 0 "no" 1 "yes"
	foreach var of varlist ec01a-ec02i {
	label values `var' yesno
	}


/*create municipality indiactor*/
	gen str2 state = string(ent, "%02.0f")
	gen str2 locality = string(loc, "%02.0f")

	gen str3 munic = string(mpio, "%03.0f")
	drop if state == "." | munic == "."
	gen munic_id = state + munic
	gen loc_id = state + munic + locality
	destring loc_id, replace
	destring munic_id, replace
	label var munic_id "Municipality identifier"


rename *, lower
**labeling estrato variable

	label var estrato "community size where hh lives"

	gen bigcity=0
	replace bigcity=1 if  estrato==1
	gen city=0
	replace city=1 if  estrato==2
	gen smallcity=0
	replace smallcity=1 if  estrato==3
	gen rural=0
	replace rural=1 if  estrato==4

	label var bigcity "City of >100,000"
	label var city "City of 15,000-100,000"
	label var smallcity "City of 2,5oo-15,000"
	label var rural "Rural village of <2,500"

// year of interview
tostring anio, replace
replace anio = "05" if anio =="5"
replace anio = "06" if anio =="6"
replace anio = "07" if anio =="7"
replace anio = "09" if anio =="9"
replace anio = "02" if t == 0  // inpute survey year for 2002 because year was not given in data
gen survey_year=yearly(anio, "20Y")
replace survey_year = 2005 if t==1
dummies survey_year



	
//	drop if age_strange_all == 1
	
sort pid_link t	
by pid_link: gen age_ini = age[1]
label var age_ini "Age when first entered survey"
gen age_ini_wrong = 1 if age_ini > age & age_ini <.  // if initial age is larger than later reported age
bysort pid_link: egen age_ini_wrong_all = max(age_ini_wrong)
sort pid_link t	
by pid_link: gen year_ini = survey_year[1]  // year came into survey


// generate new age based on initial values and then adding up the additional years between waves
*rename age age_old
*gen age = age_ini + (survey_year-year_ini) if age_ini <. & survey_year <.


	gen age_sq=age^2
	label var age_sq "Age squared"
	
	label var kids_hh "Number of children (<15) in household"

	
drop if  age<15 | age>64


**building variables and regression to do probit regression for diabetes as in Latif (2009) Table III
	gen married=0
	replace married=1 if edo_civil==3
	gen widdiv=0
	replace widdiv=1 if edo_civil==2 | edo_civil==4 | edo_civil==5
	rename ls04 sex
	replace sex=0 if sex==1
	replace sex=1 if sex==3
	recode sa01 (1=0) (3=1) // alternative 
	replace sex = sa01 if sex == .
	label define sex 1 "female" 0 "male", replace
	label variable married "Married"
	label variable widdiv "Widow or divorced or seperated"



**variables for obesity
	gen height_m =  (sa07_21/100)
	replace height_m = sa07_2/100 if t == 0			//variable with height in meters (height in cm divided by 100)
	gen weight_kg = sa08_21
	replace weight_kg = sa09_2 if t==0
	gen bmi = ( weight_kg/ height_m^2)		//BMI=kg/m²
	label var height_m "measured heigth in cm divided by 100"
	label var bmi "Body Mass Index"
	gen waist_m =  (sa14_2 /100)
	replace waist_m = sa11_21 /100 if t != 0
	label var waist_m "measured waist in meters"	
	gen overweight=0 if bmi <.
	replace overweight=1 if bmi>=25 & bmi<=29.9999999
	gen obese=0
	replace obese=1 if bmi>=30
	replace obese=. if bmi==.
	label var overweight "=1 if person has bmi 25-29.99"
	label var obese "=1 if person has bmi>=30"
	
	gen fatperc = 76 - (20 * height_m/waist_m) if sex == 1 & height_m <. & waist_m < .
	replace fatperc = 64 - (20 * height_m/waist_m) if sex == 0 & height_m <. & waist_m < .
	label var fatperc "% body fat"


	
**sleep (how many hours sleep daily?)

//clonevar sleep = ata03	



	*variable for geographic position
	label define states 1 "Aguascalientes" 2 "Baja California" 3 "Baja California Sur" 4 "Campeche" 5 "Coahuila" 6 "Colima" 7"Chiapas" 9 "Distrito Federal" 10 "Durango" 11 "Guanajuato" 12 "Guerrero" 13 "Hidalgo" 14 "Jalisco" 15 "Estado de México" 16 "Michoacán" 17 "Morelos" 18 "Nayarit" 19 "Nuevo León" 20 "Oaxaca" 21 "Puebla" 22 "Querétaro" 23 "Quintana Roo" 24 "San Luis Potosí" 25 "Sinaloa" 26 "Sonora" 28 "Tamaulipas" 29 "Tlaxcala" 30 "Veracruz" 31 "Yucatán" 32 "Zacatecas", replace
	label values ent states
	gen northeastcentr=0
	replace northeastcentr=1 if ent==5 | ent==10 | ent==19
	gen westcentr=0
	replace westcentr=1 if ent==11 | ent==14 | ent==16
	gen central=0
	replace central=1 if ent==9 | ent==15 | ent==17 | ent==21
	gen northwest=0
	replace northwest=1 if ent==3 | ent==25 | ent==26
	gen south_southeast=0
	replace south_southeast=1 if ent==20 | ent==30 | ent==31

	label var northeastcentr "Center-Northeast Region"
	label var westcentr "Central-West Region"
	label var central "Central-Mexico Region"
	label var northwest "Northwest Region"
	label var south_southeast "South-Southeast Region"

	recode ent (20 30 31 =1) (9 15 17 21 =2) (11 14 16 = 3) (5 10 19 = 4) (3 25 26 = 5) (7 22 23 28=.), gen(region)
	label define region 1 "South-Southeast Region" 2 "central" 3 "westcentral" 4 "northeastcentr" 5 "northwest"
	label value region region
	dummieslab ent


	*renaming and recoding estrato variable

	rename estrato commsize
	recode commsize (1=5) (2=3) (3=2) (4=1)
	recode commsize (5=4)
	label define size 1 "rural <2500" 2 "small city 2.500-15.000" 3 "city 15.000-100.000" 4 "big city >100.000"
	label value commsize size



	** part of indigenous group
	
	recode ed03 (3=0), gen(indigenous)
	label var indigenous "Indigenous group"

	/*EDUCATION*/

**education variable
	clonevar school = ed05 
	recode school (3=0)
	clonevar educ = ed06
	label var educ "Last level school attended"
	rename ed07_1 maxgrade
	label var maxgrade "Last completed grade"
/*
	# delimit ;
	label define educ
			1 "No instruction"
			2 "Preschool or Kinder"
			3 "Elementary"
			4 "Secondary"
			5 "Open secondary"
			6 "High school"
			7 "Open high school"
			8 "Normal Basic"
			9 "College"
			10 "Graduate";

	label define max_grade
			1."First grade"
			2 "Second grade"
			3 "Third grade"
			4 "Fourth grade"
			5 "Fifth grade"
			6 "Sixth grade"
			7 "Seventh grade"
			8 "Other(specify)";
	# delimit cr
*/
	label values educ educ
	replace educ=. if educ==98  // to missing if don't know
	replace maxgrade=. if maxgrade==98 // to missing if don't know
	gen educt0=educ if t==0
	gen educt1=educ if t==1
	gen educt2=educ if t==2
	bysort pid_link: egen educiv=total(educt1), miss
	replace educiv=educt2 if educt1==.
	label var educiv "time invariant education for people age>=25"
	label value educiv educ


	**new education variable

	recode educ (0/2=1) (3=2) (4/5=3) (6/7=4) (8/10=5), gen(education)
	label var education "education"
	label define education 1 "no education" 2 "primary" 3 "secondary" 4 "high school" 5 "university/professional school"
	label values education education
	replace education = 1 if school == 0  // if never went to school
	
	gen noeducation=0 if education<.
	replace noeducation=1 if education==1 
	gen primary=0
	replace primary=. if education==.
	replace primary=1 if education==2
	gen secondary=0
	replace secondary=. if education==.
	replace secondary=1 if education==3
	gen highschool=0
	replace highschool=. if education==.
	replace highschool=1 if education==4
	gen college_uni=0
	replace college_uni=1 if education==5
	replace college_uni=. if education==.
	

	label variable noeducation "No education"
	label variable primary "Primary"
	label variable secondary "Secondary"
	label variable highschool "High school"
	label variable college_uni "Higher education"
	
	**constructing year dummies and new age dummies etc.





	*age dummies for working population
	forvalues bot = 15(10)64 {
	local top = `bot' + 9
	gen age`bot'to`top' = age >= `bot' & age <= `top'
	}
	foreach var of varlist age15to24-age55to64{
	label var `var' "=1 if `var'"
	}
	
recode age (15/24=0 "15-24") (25/34=1 "25-34") (35/44=2 "35-44") (45/54=3 "45-54") (55/64=4 "55-64"), gen(agegroups)	
label var agegroups "age group dummies"
recode age (15/19=0 "15-19") (20/24=1 "20-24") (25/29=2 "25-29") (30/34=3 "30-34") (35/39=4 "35-39") ///
(40/44=5 "40-44") (45/49=6 "45-49") (50/54=7 "50-54") (55/59=8 "55-59") (60/64=9 "60-64"), gen(agegroups5)	
	
*Variables




/*drop pregnant women from sample because this could cause diabetes diagnosis and at the same time unemployment*/

clonevar pregnant = he01c
replace pregnant = he01_1c if t != 0
drop if pregnant == 1
	
/* create variable named diabetes equal to ec01a */

  gen diabetes = ec01a 
  label var diabetes "Diagnosed diabetes"
  label define diabetes 0 "No diabetes" 1 "Diabetes" 
  
  // looking for inconsistencies in diabetes reporting across waves. If reports diabetes in earlier wave but not in later wave we assume that has diabetes.

foreach var of varlist diabetes{  
  clonevar `var'02 = `var' if t == 0
  bysort pid_link: egen `var'02all = max(`var'02) if _n > 1

  clonevar `var'05 = `var' if t == 1
  bysort pid_link: egen `var'05all = max(`var'05) if _n > 1
  
  clonevar `var'09 = `var' if t == 2
  bysort pid_link: egen `var'09all = max(`var'09) if _n > 1

bysort pid_link (t): gen `var'_diff = `var' - l1.`var'

}

/* indicator variables about reporting consistency: 
-1 = diabetes in earlier wave but not in later wave; 
0 = same reported in both waves;
1 = reports diabetes now but not in earlier wave*/


  gen diabetes13 = 1 if diabetes02all == 1 & diabetes05all== 0 & diabetes09all == 1  // reports diabetes in 2002 and 2009 but not in 2005
  replace diabetes13 = 0 if diabetes02all == 1 & diabetes05all== 0 & diabetes09all == 0 // reports diabetes in 02 but not in 05 or 09
  gen diabetes12 = 1 if diabetes02all == 1 & diabetes05all== 1 & diabetes09all == 0  // reports diabetes in 2002 and 2005 but not in 2009
  replace diabetes12 = 0 if diabetes02all == 0 & diabetes05all== 1 & diabetes09all == 0 // reports diabetes in 05 but not in 02 or 09
  
  replace diabetes = 1 if diabetes13 == 1 & t == 1  // replace 2005 diabetes = 1 if reports diabetes in 2002 and 2009 but not in 2005
  replace diabetes = 0 if diabetes13 == 0 & t == 0  // replace 2002 diabetes = 0 if reports diabetes in 02 but not in 05 or 09	
  replace diabetes = 1 if diabetes12 == 1 & t == 2 // replace 2009 diabetes = 1 if reports diabetes in 2002 and 2005 but not in 2009	
  replace diabetes = 0 if diabetes12 == 0 & t == 1 // replace 2005 diabetes = 0 if reports diabetes in 05 but not in 02 or 09		
  replace diabetes = 0 if diabetes02all == 1 & diabetes05all== 0 & diabetes09all == . & diabetes ==1 & t == 0 // replace 2002 diabetes = 0 if reports diabetes in 02 but not in 05 and no info available for 2009		
  replace diabetes = 0 if diabetes02all == . & diabetes05all== 1 & diabetes09all == 0 & diabetes ==1 & t == 1 // replace 2005 diabetes = 0 if reports diabetes in 05 but not in 09 and no info available for 2002		
  replace diabetes = 0 if diabetes02all == 1 & diabetes05all== . & diabetes09all == 0 & diabetes ==1 & t == 0 // replace 2002 diabetes = 0 if reports missing value for diabetes in 05, reports no diabetes in 09 and reports diabetes in 2002		

gen diabetestest = 1 if diabetes13==1 | diabetes12==1
replace diabetestest=0 if diabetes13 == 0 | diabetes12 ==0
	
/* checks if transition probabilities of diabetes can now only go from no diabetes to diabetes and not the other way around*/
sort pid_link t
gen lastwave=l.diabetes  // diabetes value from wave before
bysort pid_link (t): gen wrong=diabetes[_n-1]
gen nextwave=f.diabetes // diabetes value from next wave
gen nextwave2 = f2.diabetes  // diabetes value from 2 waves before
gen lastwave2=l2.diabetes // diabetes value from second wave after

tab diabetes nextwave if t == 0, freq row // check of trans prob of diabetes from 02 to 05
tab diabetes nextwave if t == 1, freq row // check of trans prob of diabetes from 05 to 09
tab diabetes nextwave2 if t == 0, freq row  // check of trans prob of diabetes from 02 to 09

tab diabetes nextwave if t == 2, nofreq row // consistency check. should return no observations
xttrans diabetes if diabetes <. , freq // should show 0 for transition from diabetes to no diabetes



/*alternative assuming that if has reported diabetes has diabetes in all ensuing waves*/
gen dmyet= ec01a
label var dmyet "Diabetes"
by pid_link (t), sort: replace dmyet = dmyet[_n-1] if ec01a == 0 | ec01a ==.
replace dmyet = 0 if dmyet == . & ec01a == 0
by pid_link (t), sort: replace dmyet = dmyet[_n+1] if dmyet == . & dmyet[_n+1] == 0



/*To distinguish type 1 and type 2 use categorization of type 1 if diagnosis before 35, type 2 if 35 or after.
http://www.nejm.org/doi/full/10.1056/NEJMoa1605368 */

egen age_reported = min(age / (diabetes == 1)), by(pid_link)
label var age_reported "Age when diabetes was first reported in survey"


gen diabetes_t = 0 if diabetes <.
replace diabetes_t = 1 if diabetes == 1 & age_reported <35
replace diabetes_t = 2 if diabetes == 1 & age_reported >= 35 & age_reported <.
label variable diabetes_t "Diabetes grouping according to age when diagnosed"
label define diabetes_t 0 "No diabetes" 1 "Early onset (< 35)" 2 "Late onset 2 (35+)"

label values diabetes_t diabetes_t

gen type1 = diabetes_t == 1 if diabetes <.
gen type2 = diabetes_t == 2 if diabetes <.
label var type1 "Early onset (< 35)"
label var type2 "Late onset 2 (35+)"

// year diagnosed (only available for 2009 data)


gen ec01ba_string = ec01ba
tostring ec01ba_string, replace // making diagnosis
gen year_diabetes_date=yearly(ec01ba_string, "Y")

// year of diagnosis for all waves derived from the year reported in 2009
bysort pid_link: egen year_diagnosis = max(year_diabetes_date)

// years with diabetes for each wave based on diagnosis year in wave from 2009

gen year_diagnosis_corr = survey_year if year_diagnosis < survey_year & diabetes == 0 & diabetes[_n+1] == 1  // everybody with no self reported diabetes but a diagnosis year before the survey year
bysort pid_link: egen year_diagnosis_corr_all=max(year_diagnosis_corr) // extend it to all waves for person
gen years_diabetes_incorrect = survey_year - year_diagnosis if year_diagnosis < survey_year & diabetes == 0 & diabetes[_n+1] == 1 // variable indicating years of diabetes for those with diagnosis date before a survey where they don't report a diabetes diagnosis
bysort pid_link: egen years_diabetes_incorrect_all = max(years_diabetes_incorrect)  // extended to all observations of person
replace year_diagnosis = year_diagnosis_corr_all if year_diagnosis_corr_all <. & years_diabetes_incorrect_all < 6 // replaces year of diagnosis with year of survey if date of diagnosis is within 5 years of last survey where no diagnosis was reported

gen years_diabetes = survey_year - year_diagnosis


gen years_before_diabetes = year_diagnosis - survey_year if years_diabetes <0
replace years_diabetes = . if years_diabetes < 0 | diabetes09all ==.
replace years_diabetes = years_diabetes + 1   // count those with a diagnosis within the last year as one year with diabetes in order to include those without diabetes as zeros
replace years_diabetes = 0 if diabetes == 0 & diabetes09all ==0  // count no diabetes diagnosis as zero years


label var years_diabetes "Diabetes duration"
label var years_before_diabetes "years before diabetes diagnosis"
label var survey_year "year of interview"
label define test_diabetes 1 "blood test" 2 "urine test" 3 "no test" 5 "other" 8 "Don't know"
label values ec01aa_1 test_diabetes

// 5 year groups for years since diagnosis
forvalues bot = 0(5)15 {
	local top = `bot' + 4
	gen years_diabetes`bot'to`top' = years_diabetes >= `bot' & years_diabetes <= `top'
}


// year groups for years since diagnosis
gen years_diabetes_groups = 0 if years_diabetes <.
replace years_diabetes_groups = 1 if years_diabetes < 2 & diabetes == 1
replace years_diabetes_groups = 2 if years_diabetes > 1 & years_diabetes < 6 & diabetes == 1
replace years_diabetes_groups = 3 if years_diabetes > 5 & years_diabetes < 11 & diabetes == 1
replace years_diabetes_groups = 4 if years_diabetes > 10 & years_diabetes < 16 & diabetes == 1
replace years_diabetes_groups = 5 if years_diabetes > 15 & years_diabetes < . & diabetes == 1

label define diabetesyears 0 "no diabetes" 1 "< 2 years" 2 "2-5 years" 3 "6-10 years" 4 "11-15 years" 5 "> 15 years"
label value  years_diabetes_groups diabetesyears
label define treatment_type_diab 1 "tablets" 2 "Injections" 3 "Both" 4 "Other" 
label values ec02aa_1 treatment_type_diab

gen years_diabetes_groups1 = 0 if years_diabetes <.
replace years_diabetes_groups1 = 1 if years_diabetes < 5 & diabetes == 1
replace years_diabetes_groups1 = 2 if years_diabetes > 4 & years_diabetes < 9 & diabetes == 1
replace years_diabetes_groups1 = 3 if years_diabetes > 8 & years_diabetes <= 12 & diabetes == 1
replace years_diabetes_groups1 = 4 if years_diabetes > 12 & years_diabetes < . & diabetes == 1
label define years_diabetes1 0 "No diabetes" 1 "0--3" 2 "4--7" 3 "8--12" 4 "13+"
label val years_diabetes_groups1 years_diabetes1
label var years_diabetes_groups1 "Years since diabetes groups similar to splines groups"
dummies years_diabetes_groups1 


recode years_diabetes (1/5=1) (6/10=2) (11/15 =3) (16/65=4), gen(years_diabetes_groups2)
label var years_diabetes_groups2 "Diabetes duration (5 year spells)"
label define years_diabetes_groups2 0 "No diabetes" 1 "0--4 years" 2 "5--9 years" 3 "10--14 years" 4 "15+ years"
label value years_diabetes_groups2 years_diabetes_groups2
dummies years_diabetes_groups2


recode years_diabetes (1/2=1) (3/5=2) (6/8 =3) (9/11=4) (12/65=5), gen(years_diabetes_groups3)
label var years_diabetes_groups3 "Diabetes duration (5 year spells)"
label define years_diabetes_groups3 0 "No diabetes" 1 "0--1 years" 2 "2--4 years" 3 "5--7 years" 4 "8--10 years" 5 "11 + years"
label value years_diabetes_groups3 years_diabetes_groups3
dummies years_diabetes_groups3

// year groups for years before diabetes diagnosis
gen years_before_diabetes_groups = 0
replace years_before_diabetes_groups = 1 if years_before_diabetes < 2
replace years_before_diabetes_groups = 2 if years_before_diabetes <= 5 & years_before_diabetes >1
replace years_before_diabetes_groups = 3 if years_before_diabetes > 5 & years_before_diabetes < 11
replace years_before_diabetes_groups = 4 if years_before_diabetes > 10 & years_before_diabetes < 16
replace years_before_diabetes_groups = 5 if years_before_diabetes > 15 & years_before_diabetes < .



// AGE AT DIAGNOSIS

gen age_diagnosis = age-years_diabetes if t == 2
replace age_diagnosis = . if years_diabetes == 0
replace age_diagnosis =0 if diabetes == 0
bysort pid_link: egen age_diagnosis_all = max(age_diagnosis)   // extend information from 2009 to earlier waves because this info not available there



gen age_diagnosis_group = 0
replace age_diagnosis_group = 1 if age_diagnosis >= 1 & age_diagnosis < 25
replace age_diagnosis_group = 2 if age_diagnosis >= 25 & age_diagnosis < 35
replace age_diagnosis_group = 3 if age_diagnosis >= 35 & age_diagnosis < 45
replace age_diagnosis_group = 4 if age_diagnosis >= 45 & age_diagnosis < 55
replace age_diagnosis_group = 5 if age_diagnosis >= 55 & age_diagnosis < 65
label define age_diagnosis 0 "no diabetes" 1 "age < 25" 2 "25-34 age" 3 "35-44 age" 4 "45-54 age" 5 "55-64 age"
label value  age_diagnosis_group age_diagnosis



	**income variables






***work-dataset--> income etc.***


	/*NAICS*/
	#delimit ;

	label def NAICS
	11 "Agriculture, cattle ranch, forest advantage, fish and hunt"
	21 "Mining"
	22 "Gas electricity, water and provision by lines to the final consumer"
	23 "Construction"
	31 "Manufacturing industries"
	32 "Manufacturing industries"
	33 "Manufacturing industries"
	43 "Wholesale trade"
	46 "Retail trade"
	48 "Transportation, post office and storage"
	49 "Transportation, post office and storage"
	51 "Massive means information"
	52 "Financial services insurance"
	53 "Real estate services and of rent of personal and intangible property"
	54 "Professional, scientific and technical services"
	55 "Direction of corporative and companies"
	56 "Services for the support to the businesses and handling of remainders and mediation services"
	61 "Educational services"
	62 "Health an social services"
	71 "Cultural and sport services, and other recreational services"
	72 "Services of temporary lodging and preparation of foods and drinks"
	81 "Other services except government activities"
	93 "Government and international and extraterritorial organisms activities"
	99 "noncclassified previously, insufficiently specified and not specified";

	label def worktype
	1 "Peasant on your plot"
	2 "Family worker in a Hhm owned business, without remuneration"
	3 "Non-agricultural worker or employee"
	4 "Rural laborer, or land peon (agricultural worker)"
	5 "Boss, employer, or business proprietor"
	6 "Self-employed worker"
	7 "Worker without remuneration from a business or company that is not owned by the HHm";


	#delimit cr



	label values tb32p worktype
	label values tb32s worktype

	foreach var of varlist  tb14_16_scian tb24_26s_scian tb41_43p_scian tb41_43s_scian tb58_59_scian tb24_26p_scian {
	label values `var' NAICS
	}

	*generating income variable that sums all sources of labour income (first jo, secondary job, own buissness)
	*income from main job  
	
/**** NEED TO CHECK WHAT HAS CHANGED IN 2009 DATA BECAUSE INCOME VARIABLES ARE NOT THE SAME ANYMORE. TAKE LOOK AT MOST RECENT QUESTIONNAIRE AND USERS GUIDE*/

		*does work dummy (inlcudes paid and unpaid work, also for family business.
	gen works=0 // ed12 defines if currently attending school. If so, cannot be said to be unemployed
	replace works=1 if tb02==1 | tb05 == 1 | tb04 == 1  | tb03 == 1
	replace works=. if tb02==. | tb02 == 3
        
	label var works "Employed"



	egen incomemain_mth = rowtotal(tb35aa_2 tb35ab_2 tb35ac_2 tb35ad_2 tb35ae_2 tb35af_2 tb35ag_2 tb35ah_2 tb35ai_21 tb37p2_2), miss
	replace incomemain_mth = tb35a_2 if  incomemain_mth ==. | incomemain_mth == 0
	
	egen incomemain_year = rowtotal(tb36aa_2 tb36ab_2 tb36ac_2 tb36ad_2 tb36ai_2 tb36aj_2 tb36ak_2 tb36al_2 tb36am_21 tb38p2_2), miss  // left out tb36ae_2 tb36af_2 tb36ag_2 tb36ah_2 because these are bonuses that are only asked for in the question for annual income
	replace incomemain_year = tb36a_2 if  incomemain_year ==. | incomemain_year == 0

	*secondary income
	egen incomesec_mth = rowtotal(tb35b_2 tb37s2_2), miss
	egen incomesec_year = rowtotal(tb36b_2 tb38s2_2), miss

	*total income
	egen income_mth = rowtotal(incomemain_mth incomesec_mth), miss
	egen income_year = rowtotal(incomemain_year incomesec_year), miss
	replace income_mth = income_year/12 if income_mth == 0 | income_mth == .
	replace income_mth =. if works == 0
	
	gen income_adj = income_mth
	replace income_adj = income_mth * 1.75 if survey_year == 2002
	replace income_adj = income_mth * 1.46 if survey_year == 2005
	replace income_adj = income_mth * 1.38 if survey_year == 2006
	replace income_adj = income_mth * 1.32 if survey_year == 2007
	replace income_adj = income_mth * 1.24 if survey_year == 2008
	replace income_adj = income_mth * 1.20 if survey_year == 2009
	replace income_adj = income_mth * 1.15 if survey_year == 2010
	replace income_adj = income_mth * 1.10 if survey_year == 2011
	replace income_adj = income_mth * 1.06 if survey_year == 2012
	
label var income_adj "monthly income adjusted for inflation to year 2013"
gen lnincome_adj = log(income_adj)
gen lnincome = log(income_mth)
label var lnincome_adj "log income adjusted for inflation to year 2013"
generate lnincome_mth = ln(income_mth)                // Zero values will become missing
* Label the variables
label variable income_mth "monthly income"
label variable lnincome_mth "log monthly income"



	*gen working hrs variable for hours worked per week
	
	egen workhrs_pw = rowtotal(tb27p tb27s) if works == 1  // work hours in past week first and second job
	egen workhrs_normal = rowtotal(tb28p tb28s)  if works == 1 // normal work hours per week first and second job
	replace workhrs_normal =. if works == 0 | works ==.
	replace workhrs_pw =. if works == 0 | works ==.

	gen  hrsfirst_normal = tb28p
	gen  hrssecond_normal = tb28s
	gen  hrsfirst_pw = tb27p
	gen  hrssecond_pw = tb27s
	
	
	replace works = 0 if workhrs_normal < 4 & works <.
 	replace works=. if tb02==. | tb02 == 3 // delete those in school


	replace workhrs_normal = . if workhrs_normal == 0 
	replace workhrs_normal= . if works == 0 
	replace workhrs_normal = . if workhrs_normal > 112 & workhrs_normal <.  // affects 39 observations
	rename workhrs_normal workhrs
	label var workhrs "Usual weekly workinghours"

	drop workhrs_*



	label variable hrsfirst_pw "working hours last week in main job"
	label variable hrssecond_pw "working hours last week second job"
	label var incomemain_mth "income first job per month"
	label var incomesec_mth "income second job per month"
	label var income_mth "total income per month"
	
/*generate hourly wage variable because only that reflects changes in wages. 
If wage does not change but monhtly income does, then this should be driven by changes in workinghours*/

gen wage_hr = (income_adj/4.3333333) / workhrs
replace wage_hr = 0 if wage_hr < 1  // count as 0 wages if hourly wage < 1 peso. prevents negative values for log
label var wage_hr "Hourly wage"	

/*Dropping most extreme outliers at 1st and 99th percentile*/
gen wage_hr_tr = wage_hr
forvalues i= 0/2{
_pctile wage_hr if t== `i', nq(100) 
replace wage_hr_tr = . if wage_hr > r(r99) & wage_hr <. & t== `i'
replace wage_hr_tr = . if wage_hr < r(r1) & t== `i'
}

gen lnwage_hr = log(wage_hr)
label var lnwage_hr "Log hourly wage"	

gen lnwage_hr_tr = log(wage_hr_tr)
label var lnwage_hr "Log hourly wage (truncated 1st and 99th pct)"	
/*replace works variable with old definition of only at least 1 working hour with definition of employed
	 of at least 4 working hours per week.*/

/*create variable defining decision to be in paid employment*/
gen employed = 0 if works == 0
replace employed = 1 if lnwage_hr <.

** create variable defining formal and informal employment

gen labourstatus = 0 if works < .
replace  labourstatus = 2 if works ==1 & (tb33p_a < . | tb33p_b <.)

replace labourstatus = 1 if works == 1 & labourstatus != 2

label define formalinformal 0 "unemployed" 1 "informal" 2 "formal"
label values labourstatus formalinformal
label var labourstatus "unemployed, formal or informal employed"
	 *keeping needed variables
	 *keep   works incomemain_mth incomesec_mth income_mth hrsmth hrsmth2 wagejb2 wage lnwage folio ls pid_link  tb24_26p_scian  tb24_26s_scian tb32p tb32s tb02_1 tb03 tb04 tb05 tb06

** former labour status of currently unemployed

gen old_labourstatus = 0 if works == 0
replace  old_labourstatus = 2 if works ==0 & (tb18_a < . | tb18_b <.)

replace old_labourstatus = 1 if works == 0 & old_labourstatus != 2 & (tb18_c < . | tb18_d <. | tb18_e <. | tb18_f <. | tb18_g <. | tb18_h <. | tb18_i <.)

label values old_labourstatus formalinformal
label var old_labourstatus "unemployed, formal or informal employed"

	**working type variable
	clonevar worktype_main = tb32p  // generate worktype variable for renumerated work
	replace worktype_main = tb32s if tb39p == 1 & (tb32p == 2 | tb32p == 7)  // replace worktype with secondary worktype if primary worktype was not renumerated but secondary job is
	
	// generate broader worktype variable excluding those without renumeration
	gen worktype=2 if tb32p==1 | tb32p==4  //agricultural worker
	replace worktype=1 if tb32p==3 // Non-agricultural worker or employee
	replace worktype=3 if tb32p==5 | tb32p==6 // Boss, employer, or business proprietor or Self-employed worker
	
	#delimit ;
	label define worktype1
	1 "non-agricultural worker or employee"
	2 "agricultural worker"
	3 "self-employed";
//	4 "Boss, employer, or business proprietor" ;


	#delimit cr
	label value worktype worktype1
	label var worktype "Position and type of work"
	**jobtype dummies
	  dummieslab worktype, word(1)
	  
	  label var nonagricultural "Non-agricultural worker or employee"
	  label var agricultural "Agricultural worker"
	  label var selfemployed "Self-employed"
	  
	  

/*Status variable with worktypes and unemployed as base value*/

	gen status = 0 if works == 0   // type of work
	replace status = 1 if worktype == 1
	replace status = 2 if worktype == 2
	replace status = 3 if worktype == 3
	replace status = 0 if works == 0
	label define status 0 "Unemployed" 1 "Non-agricultural worker or employee" 2 "Agricultural worker" 3 "Self-employed"
	label value status status
dummies status

by pid_link: gen status_ini = status[1]   // to account for initial parameters problem as described in Wooldrige and  https://www.melbourneinstitute.com/downloads/hilda/Bibliography/Working_Discussion_Research_Papers/2012/Zucchelli_etal_Ill_Health_Transitions.pdf
/*Medical insurance*/

recode ca01 (3=0), gen(insurance) 
label var insurance "Any medical insurance"



** merging data  of parents to observations to have diabetes and socioeconomic information of parents. Therefore I merge the ls06 (Identifier for father) and ls07 (identifier for mother) ///
** with ls (identifier for observation, here child of parents)

** first for fathers. make copy of dataset only with needed variables and then change ls to ls06 to later merge it with the original dataset
save "Paneldaten/variables.dta", replace
use folio ls diabetes noeducation primary secondary highschool college_uni education t using "Paneldaten/variables.dta", clear

rename ls ls06

rename diabetes diab_father_hh
label var diab_father_hh "=1 if father has diabetes"

renvars noeducation primary secondary highschool college_uni education, postfix(_father) // rename education variables: postfix with _father


** education variable indicating if father had any education
gen educ_father = 0 if education_father < . 
replace educ_father = 1 if education_father > 1 & education_father < . 
label var educ_father "=1 if father has any education"


save "Paneldaten/temp_fatherdiab.dta", replace

use "Paneldaten/variables.dta", clear

merge m:1 folio ls06 t using "Paneldaten/temp_fatherdiab", gen(_mergediabfath) keep(master match)



save "Paneldaten/temp_fatherdiab1.dta", replace


** now for mothers

use folio ls diabetes noeducation primary secondary highschool college_uni education sex t using "Paneldaten/variables.dta", clear

rename ls ls07

rename diabetes diab_mother_hh
label var diab_mother_hh "=1 if mother has diabetes"

renvars noeducation primary secondary highschool college_uni education, postfix(_mother) // rename education variables: postfix with _mother

** education variable indicating if mother had any education

gen educ_mother = 0 if education_mother < .
replace educ_mother = 1 if education_mother > 1 & education_mother < .

label var educ_mother "=1 if mother has any education"

save "Paneldaten/temp_motherdiab.dta", replace

use "Paneldaten/temp_fatherdiab1.dta", clear

merge m:1 folio ls07 t using "Paneldaten/temp_motherdiab", gen(_mergediabmoth) keep(match master)

!rm Paneldaten/temp_*.dta // removes temporary files

// now I need to create diabetes and education variable for all parents, by also using the information on parents that are already dead or don't live in household //

gen diab_par=0
replace diab_par=1 if tp16m_2==1 |  tp16p_2==1  | diab_father_hh == 1 | diab_mother_hh == 1


label var diab_par "=1 if one parent has/had diabetes"
label value diab_par yesno

gen diab_mother=0 
replace diab_mother=1 if tp16m_2==1 | diab_mother_hh == 1
replace diab_mother =. if tp16m_2==. & diab_mother_hh ==.
label var diab_mother "=1 if mother has/had diabetes"
label value diab_mother yesno

gen diab_father=0 
replace diab_father=1 if tp16p_2==1   | diab_father_hh == 1
replace diab_father =. if tp16p_2 ==.   & diab_father_hh ==.
label var diab_father "=1 if father has/had diabetes"
label value diab_father yesno


// drop if diab_father_hh == 1 | diab_mother_hh == 1

// education parents

replace educ_mother = 1 if tp11m > 2 &  tp11m < 97
replace educ_father = 1 if tp11p > 2 &  tp11p < 97

replace educ_mother = 0 if tp11m <= 2 &  tp11m < 97
replace educ_father = 0 if tp11p <= 2 &  tp11p < 97


gen higher_educ_father = 0 if tp11p <. |  education_father < .
replace higher_educ_father = 1 if (tp11p >4 & tp11p < 9) | (education_father > 3 & education_father < .)
label var higher_educ_father " > secondary education" 
label value higher_educ_father yesno

gen higher_educ_mother = 0 if tp11m <. |  education_mother < .
replace higher_educ_mother = 1 if (tp11m >4 & tp11m < 9) | (education_mother > 3 & education_mother < .)
label var higher_educ_mother " > secondary education" 
label value higher_educ_mother yesno


gen some_educ_father = 0 if tp11p <. |  education_father < .
replace some_educ_father = 1 if tp11p == 3 | tp11p ==4 | primary_father == 1 | secondary_father == 1
label var some_educ_father "primary or secondary education" 
label value some_educ_father yesno

gen some_educ_mother = 0 if tp11m <. |  education_mother < .
replace some_educ_mother = 1 if tp11m == 3 | tp11m ==4 | primary_mother == 1 | secondary_mother == 1
label var some_educ_mother "primary or secondary education" 
label value some_educ_mother yesno

gen dk_educ_father = 0 if tp11p <. |  education_father < .
replace dk_educ_father = 1 if tp11p == 98
label var dk_educ_father "dont know education" 
label value dk_educ_father yesno

gen dk_educ_mother = 0 if tp11m <. |  education_mother < .
replace dk_educ_mother = 1 if tp11m == 98
label var dk_educ_mother "dont know education" 
label value dk_educ_mother yesno


	//create parental education indicators and household wealth indicator
	gen educ_par = 0 if educ_father < . | educ_mother <.
replace educ_par = 1 if (educ_father == 1 | educ_mother == 1) 

gen higher_educ_par = 0 if educ_father<. | educ_mother <.
replace higher_educ_par = 1 if higher_educ_father == 1 | higher_educ_mother == 1

gen some_educ_par = 0 if educ_father<. | educ_mother <.
replace some_educ_par = 1 if (some_educ_father == 1 | some_educ_mother == 1) & higher_educ_par == 0



foreach var of varlist diab_par diab_father diab_mother{  
  clonevar `var'02 = `var' if t == 0
  bysort pid_link: egen `var'02all = max(`var'02) if _n > 1

  clonevar `var'05 = `var' if t == 1
  bysort pid_link: egen `var'05all = max(`var'05) if _n > 1
  
  clonevar `var'09 = `var' if t == 2
  bysort pid_link: egen `var'09all = max(`var'09) if _n > 1

bysort pid_link (t): gen `var'_diff = `var' - l1.`var'



  gen `var'13 = 1 if `var'02all == 1 & `var'05all== 0 & `var'09all == 1  // reports `var' in 2002 and 2009 but not in 2005
  replace `var'13 = 0 if `var'02all == 1 & `var'05all== 0 & `var'09all == 0 // reports `var' in 02 but not in 05 or 09
  gen `var'12 = 1 if `var'02all == 1 & `var'05all== 1 & `var'09all == 0  // reports `var' in 2002 and 2005 but not in 2009
  replace `var'12 = 0 if `var'02all == 0 & `var'05all== 1 & `var'09all == 0 // reports `var' in 05 but not in 02 or 09
  
  replace `var' = 1 if `var'13 == 1 & t == 1  // replace 2005 `var' = 1 if reports `var' in 2002 and 2009 but not in 2005
  replace `var' = 0 if `var'13 == 0 & t == 0  // replace 2002 `var' = 0 if reports `var' in 02 but not in 05 or 09	
  replace `var' = 1 if `var'12 == 1 & t == 2 // replace 2009 `var' = 1 if reports `var' in 2002 and 2005 but not in 2009	
  replace `var' = 0 if `var'12 == 0 & t == 1 // replace 2005 `var' = 0 if reports `var' in 05 but not in 02 or 09		
  replace `var' = 0 if `var'13 == . & `var'02all == 1  & t == 1 & `var' <1 // replace 2005 `var' = 0 if reports `var' in 02 but not in 05 and no info available for 2009		
  replace `var' = 0 if `var'13 == . & `var'02all == 1  & t == 1 & `var' ==. // replace 2005 `var' = 0 if reports `var' in 02 and missing in 05 and no info available for 2009		
  replace `var' = 0 if `var'12 == . & `var'05all == 1  & t == 2 & `var' <1 // replace 2009 `var' = 0 if reports `var' in 05 but not in 09 and no info available for 2002		
  replace `var' = 0 if `var'12 == . & `var'05all == 1  & t == 2 & `var' ==. // replace 2009 `var' = 0 if reports `var' in 05 and missing in 09 and no info available for 2002		
  replace `var' = 0 if `var'12 == . & `var'13 == . & `var'02all == 1  & `var'05all == . & t == 2 & `var' ==0 // replace 2009 `var' = 0 if reports missing value for `var' in 05, reports no `var' in 09 and reports `var' in 2002		

gen `var'test = 1 if `var'13==1 | `var'12==1
replace `var'test=0 if `var'13 == 0 | `var'12 ==0
	
/* checks if transition probabilities of `var' can now only go from no `var' to `var' and not the other way around*/
sort pid_link t
gen lastwave`var'=l.`var'  // `var' value from wave before
bysort pid_link (t): gen wrong`var'=`var'[_n-1]
gen nextwave`var'=f.`var' // `var' value from next wave
gen nextwave2`var' = f2.`var'  // `var' value from 2 waves before
gen lastwave2`var'=l2.`var' // `var' value from second wave after

tab `var' nextwave`var' if t == 0, freq row // check of trans prob of `var' from 02 to 05
tab `var' nextwave`var' if t == 1, freq row // check of trans prob of `var' from 05 to 09
tab `var' nextwave2`var' if t == 0, freq row  // check of trans prob of `var' from 02 to 09

tab `var' nextwave`var' if t == 2, nofreq row // consistency check. should return no observations
xttrans `var' if `var' <. , freq // should show 0 for transition from diabetes to no diabetes


}
*/

// create a new folio variable that only identifies siblings living in the same household
	* first identify those that report that household head is their father/mother
gen str8 var1 = string(folio, "%08.0f")
gen str4 sibling = string(ls05_1, "%04.0f") if ls05_1 == 3  
gen folio_sib = var1 + sibling if ls05_1 == 3 // identifier is folio + the 0003 which identifies siblings
	*now identify household heads and their brothers/sisters
gen hhh_sibling = 1 if ls05_1 == 8  // those that report being brother/sister of hh head
bysort folio wave: egen hhh_sibling_temp = max(hhh_sibling)  // extending this info to all within folio
gen hhh_sibling_temp1 = 1 if hhh_sibling_temp == 1 & ls05_1 == 1 // identifies hh heads that have a brother/sister living with them
replace hhh_sibling = 1 if hhh_sibling_temp1 == 1 // adds info for hh heads to hhh_sibling variable
	*now adding identifier of hh heads and their brother/sister(s) to sibling identifier using folio + 0001
gen str4 hhh_sibling_str = string(hhh_sibling, "%04.0f")
replace folio_sib = var1 + hhh_sibling_str if hhh_sibling == 1
label var folio_sib "Identifies siblings in household"

drop *sibling*
destring folio_sib, replace

// create wealth indicator
gen house = 8133/6409 if ah03a == 1
gen house_other = 8133 / 1463 if ah03b == 1
gen bike = 8133/ 3164 if ah03c == 1
gen car = 8133 / 3006 if ah03d == 1
gen electrics = 8132 / 7378 if ah03e == 1
gen washingmachine = 8132 / 7058 if ah03f == 1
gen electrodomestics = 8131 / 6842 if ah03g == 1

/*egen wealth =rowtotal(house - electrodomestics)
replace wealth = . if ah03a==. | ah03b== . | ah03c== . | ah03d== . | ah03d== . | ah03e==. | ah03f==. | ah03g==.
*/
// create principal component

* create house qualitiy variable according to floor material with earth

gen floor_earth = 1 if cvo05_1 == 3
replace floor_earth =0 if cvo05_1 <. & cvo05_1 != 3

*water access if has access to clean water in house#

gen water_access = 0 if cv08_1 <.
replace water_access = 1 if cv08_1 < 3

dummies cvo05_1  // floor material
dummies cv08_1  // type of water access

foreach var of varlist ah03a-ah03n {
clonevar `var'dummy = `var'
replace `var'dummy =0 if `var' != 1
}



gen animals = 0 if ah03j <. | ah03k <.  | ah03l <. | ah03m <.
replace animals = 1 if ah03j == 1 | ah03k == 1  | ah03l == 1 | ah03m == 1
global assets "ah03adummy-ah03gdummy ah03jdummy-ah03mdummy cv08_11 cv08_12 cv08_13 cv08_15 cvo05_11 cvo05_12 cvo05_13"




gen wealth = .
forvalue i=0/2{
factor $assets if t == `i', pcf
predict wealth`i' if t ==`i'
replace wealth = wealth`i' if t ==`i'
}

label var wealth "Wealth index"

// variable indicating if information on mother or father is missing for chronic disease. if so, have to exclude it from estimation

gen missinginfo = 0 
replace missinginfo = 1 if (tp16p_1 ==. & ls06 == 51) & (tp16m_1 ==. & ls07 == 51)


/*Indicator if biomarket data exists or not*/
clonevar hba1c = sa16d_21  // hba1c measure
gen bio = 0 if hba1c == .
replace bio = 1 if hba1c <.
label val bio yesno
label var bio "has biomarker data"

/*Indicator if person is above 44. People above 44 where automatically included in biomarker data according to questionnaire.*/

gen over45 = 0 if age < 45
replace over45 = 1 if age >= 45 & age <.

// diabetes diagnosed

gen diab_diag = 0 if diabetes == 0 | hba1c < 6.5 & diabetes == 0
replace diab_diag = 1 if diabetes ==1 | hba1c >= 6.5 & hba1c < .
replace diab_diag =. if t !=2

gen diab_hba1c = 0 if hba1c < 6.5 & t == 2 
replace diab_hba1c= 1 if hba1c >= 6.5 & hba1c < .
replace diab_hba1c=. if t != 2 
label var diab_hba1c "HbA1c $\geq 6.5\%$"


gen diagnosis = 0 if hba1c >= 6.5 & hba1c < . & diabetes == 0
replace diagnosis = 1 if diabetes == 1 & hba1c <.

gen diab_ud = 0 if hba1c <.
replace diab_ud = 1 if hba1c >= 6.5 & hba1c < . & diabetes == 0
label var diab_ud "Undiagnosed diabetes"


label var diagnosis "undiagnosed = 0, diagnosed = 1"
label define diagnosis 0 "undiagnosed" 1 "diagnosed"
label value diagnosis diagnosis

gen diab_severity = 0 if diabetes <. & hba1c <.
replace diab_severity = 1 if hba1c >= 6.5 & hba1c < 8
replace diab_severity = 2 if hba1c >=8 & hba1c < 12
replace diab_severity = 3 if hba1c >=12 & hba1c < .
label value  diab_severity severity

// controlled diabetes below 6.5%

gen diab_controlled = 0 if hba1c <.
replace diab_controlled = 1 if diabetes == 1 & hba1c <6.5
label var diab_controlled "Diabetes but HbA1c < 6.5%"


// severity dummies for those with diagnosed and undiagnosed diabetes
label define severity 0 "HbA1c < 6.5" 1 "6.5 \leq HbA1c < 8" 2 "8 \leq HbA1c < 12" 3 "HbA1c \geq 12"
dummies diab_severity
gen diabetessev = diab_severity * diabetes
label value  diabetessev severity
gen diab_udsev = diab_severity * diab_ud
label value  diab_udsev severity

dummies diabetessev 
dummies diab_udsev 

// hba1c splines
mkspline sp_hba1c1 5.5 sp_hba1c2 6.4 sp_hba1c3 6.8 sp_hba1c4 8.2 sp_hba1c5 11 sp_hba1c6  = hba1c

// generate splines for diagnosed and undiagnosed
foreach var of varlist sp_hba1c1 sp_hba1c2 sp_hba1c3 sp_hba1c4 sp_hba1c5 sp_hba1c6{
gen diagn_`var' = `var'*diabetes
gen ud_`var' = `var'*diab_ud
}


label var hba1c "Glycated hemoglobin (HbA1c)"




// Hypertension measured

clonevar diastolic = sa12_22
clonevar systolic = sa12_21

// label t 

//drop if diabetes == 1 & bmi < 25 & age < 25

label define waves 0 "2002" 1 "2005" 2 "2009"
label values t waves

dummies t

/*create possible undiagnosed cases group for earlier waves befor 2009 who have undiagnosed diabetes in 2009 to 
find who likely had diabetes already in earlier waves to apply panel data analysis to undiagnosed diabetes. I use Finkelstein 2014 paper in Plos One
who looks for unidganoes cases using (a) often wake up to urinate during the night (b) a cut or
wound takes a long time to heal (c) often have headache when waking up in the morning. These variables are observed in MxFLS as well*/

clonevar pee_night = es23
label var pee_night "Pee at night"

/*Robustness check variables such as health status and other diseases*/

clonevar health_sr = es01   // self reported health
label define health_sr 1 "very good" 2 "good" 3 "fair" 4 "bad" 5 "very bad"
label value health_sr health_sr
label var health_sr "Self-reported health status"
dummies health_sr

sort pid_link t
by pid_link: gen health_sr_ini = health_sr[1]  // indicator when forst reported diabetes


clonevar hypertension = ec01b 
label var hypertension "Hypertension"
clonevar heart_disease = ec01c 
label var heart_disease "Heart_disease"
clonevar cancer = ec01d 
clonevar arthritis = ec01e 
clonevar gastric_ulcer = ec01f 
clonevar migraine = ec01g 
clonevar chronic_other1 = ec01h_1 
clonevar chronic_other2 = ec01i_1

/*CREATE RAVEN SCORE FOR ANY CHANGES DUE TO DIABETES IN COGNITIVE ABILITY. JUST SUM UP CORRECT ANSWERS TO GET A SIMPLE SCORE ACCORDING TO CORRECT ANSWERS SHOWN AT http://www.ennvih-mxfls.org/english/assets/mxfls_cognitivo.pdf*/

recode eca01 (8=1) (nonmissing = 0), gen(raven1)
recode eca02 (4=1) (nonmissing = 0), gen(raven2)
recode eca03 (5=1) (nonmissing = 0), gen(raven3)
recode eca04 (1=1) (nonmissing = 0), gen(raven4)
recode eca05 (2=1) (nonmissing = 0), gen(raven5)
recode eca06 (5=1) (nonmissing = 0), gen(raven6)
recode eca07 (6=1) (nonmissing = 0), gen(raven7)
recode eca08 (3=1) (nonmissing = 0), gen(raven8)
recode eca09 (7=1) (nonmissing = 0), gen(raven9)
recode eca10 (8=1) (nonmissing = 0), gen(raven10)
recode eca11 (7=1) (nonmissing = 0), gen(raven11)
recode eca12 (6=1) (nonmissing = 0), gen(raven12)


egen raven_score = rowtotal(raven1 - raven12), miss
label var raven_score "Raven score"

/*CREATE DEPRESSION SCALE AS SUGGESTED AT http://www.ennvih-mxfls.org/english/faq.html*/


egen depression_scale = rowtotal(sm01 - sm20), miss

recode depression_scale (19/35=0 "Normal person") (36/45=1 "Some anxiety") (46/65= 2 "Average depression") (66/80= 3 "Severe depression") (nonmissing=.) , gen(depression_indicator)
label var depression_indicator "Depression indicator"
*/

/*SPLINES*/
mkspline age1 24 age2 34 age3 44 age4 54 age5= age  // AGE GROUPS

//mkspline age1 20 age2 30 age3 40 age4 = age  // AGE GROUPS
mkspline age_cubic = age, cubic


//mkspline diab_y1 2 diab_y2 5 diab_y3 10 diab_y4 15 diab_y5 = years_diabetes  // YEARS WITH DIABETES
mkspline diab_y1 4 diab_y2 8 diab_y3 12 diab_y4 = years_diabetes  // YEARS WITH DIABETES


/*generate mean and demeaned variables to use in within-between estimatoe*/
global all "height_m diastolic systolic diabetes years_diabetes diab_y* age_diagnosis* type1 type2 sex t1 t2 t3 age age_sq rural smallcity city bigcity south_southeast central westcentr northwest northeastcentr noeducation primary secondary highschool college_uni married  kids_hh wealth indigenous  nonagricultural agricultural selfemployed hba1c health_sr* hypertension heart_disease cancer arthritis gastric_ulcer migraine chronic_other1 chronic_other2 bmi insurance BajaCalifornia-Zacatecas high_altitude2 high_altitude3 survey_*"

foreach var of varlist $all {
bysort pid_link: egen mean_`var' = mean(`var')
}

/* using the mean variables to find inconsisitent reports for sex and indigenous status and correcting it as best as possible.
For sex I use information from alternative variable sa01 which also reports sex info. For indigenous I assume that if person has reported being indigeneous
at least once across the survey that they are part of indigenous group because I guess because of stigma they are generally less likely to be part of an indigneous group
which causes measurement error*/

replace indigenous = 1 if mean_indigenous > 0 & mean_indigenous <.
replace mean_indigenous = 1 if indigenous == 1
replace sex = sa01 if mean_sex <1 & mean_sex >0 & sa01 <.
drop mean_sex
bysort pid_link: egen mean_sex = mean(sex)
replace sex = 1 if mean_sex >= 0.5 & mean_sex <.
replace sex = 0 if mean_sex < 0.5
drop mean_sex
bysort pid_link: egen mean_sex = mean(sex)



// age when diabetes is first reported for those with change within panel

bysort pid_link: egen age_firstreport = min(age) if diabetes == 1 & mean_diabetes <1


drop mean_*


global keep "ec01* ec02* *tolic* edad sa03 loc* dmyet fatperc tb* employed *height_m waist_m BajaCalifornia-Zacatecas age* diagnosis *diab* status* *survey* *work* *income* *altitud* mpio ent *wage* pid* insurance folio* mu* education t t1 t2 t3 type* *sex *group bio over* obese  *hba1c* sa01 health_sr* hypertension heart_disease cancer arthritis gastric_ulcer migraine chronic_other1 chronic_other2 bmi pee diab_* educ* folio* raven* depression*"

keep $all $keep
save "Paneldaten/final", replace

log close
exit



