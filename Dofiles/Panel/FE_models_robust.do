/*DO-FILE WITH ALL MODELS THAT WILL APPEAR IN FINAL PAPER*/

clear matrix
set more off, perm
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""

global table "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Tables/Robustness/Obese"
global estimates "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Estimates/Robust"


cd $homefolder
**log-file starten**


		use "Paneldaten/final.dta", clear

log using Logs/panel_FE_robust.log, replace




	/*PANEL DATA*/
global wage "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous i.worktype i.insurance i.survey_year obese "
global wage_nf "age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous insurance survey_year2-survey_year7 selfemployed agricultural obese"

global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco Morelos Michoacn NuevoLen Oaxaca Puebla Sinaloa Yucatn Veracruz"
global controls "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh i.indigenous wealth i.survey_year obese"
global controls_nf "age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh indigenous wealth survey_year2-survey_year7 obese"

global Y1 "lnwage_hr workhrs"




 // SELF-REPORTED DIABETES WITH FIXED EFFECTS


forvalues sex=0/1{ // by gender 
display `sex'
eststo worksFE`sex': xtreg works $controls diabetes $states if sex== `sex', fe ro
eststo worksRE`sex': xtreg works age $controls_nf diabetes $states if sex== `sex', re ro

}
forvalues sex=0/1{ // by gender and diabetes type
display `sex'
eststo worksFE`sex'_t: xtreg works $controls type* $states if sex== `sex', fe ro
eststo worksRE`sex'_t: xtreg works age $controls_nf type* $states if sex== `sex', re ro
}

	*wages and work hours

forvalues sex=0/1{
foreach var of varlist lnwage_hr workhrs { // by gender
display `sex'
eststo `var'FE`sex': xtreg `var' $wage $states  diabetes if sex== `sex', fe ro
eststo `var'RE`sex': xtreg `var' age $wage_nf $states  diabetes if sex== `sex', re ro

eststo `var'FE`sex'_t: xtreg `var' $wage $states  type* if sex== `sex', fe ro
eststo `var'RE`sex'_t: xtreg `var' age $wage_nf $states  type* if sex== `sex', re ro
	}
}
	
	

estwrite * using "$estimates/fe_results", id() replace
estread * using "$estimates/fe_results"

	*TABLES FOR SELF-REPORTED DIABETES WITH FIXED EFFECTS

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*diabetes obese) nobaselevels  coeflabels(1.diabetes "Diabetes" diabetes "Diabetes" ///
	  obese "Obese (BMI \geq 30)") 

	*table for simple self-reported fixed effects model
esttab worksFE0 worksFE1 workhrsFE0 workhrsFE1 lnwage_hrFE0 lnwage_hrFE1  using "$table/FElabouroutcomes.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.0f ) labels("N"))


esttab worksRE0 worksRE1 workhrsRE0 workhrsRE1 lnwage_hrRE0 lnwage_hrRE1 using "$table/RElabouroutcomes.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.0f ) labels("N"))



preserve

global wage "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married kids_hh wealth i.indigenous i.worktype i.insurance i.survey_year"
global wage_nf "age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous insurance survey_year2-survey_year7 selfemployed agricultural"

global states "BajaCaliforniaSur Coahuila Durango Guanajuato Jalisco Morelos Michoacn NuevoLen Oaxaca Puebla Sinaloa Yucatn Veracruz"
global controls "age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh i.indigenous wealth i.survey_year"
global controls_nf "age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh indigenous wealth survey_year2-survey_year7"

// SELF-REPORTED DIABETES WITH FIXED EFFECTS ON OBESITY SAMPLE WITHOUT CONTROLLING FOR OBESITY

drop if obese ==.
forvalues sex=0/1{ // by gender 
display `sex'
eststo worksFE`sex': xtreg works $controls diabetes $states if sex== `sex', fe ro
eststo worksRE`sex': xtreg works age $controls_nf diabetes $states if sex== `sex', re ro

}
forvalues sex=0/1{ // by gender and diabetes type
display `sex'
eststo worksFE`sex'_t: xtreg works $controls type* $states if sex== `sex', fe ro
eststo worksRE`sex'_t: xtreg works age $controls_nf type* $states if sex== `sex', re ro
}

	*wages and work hours

forvalues sex=0/1{
foreach var of varlist lnwage_hr workhrs { // by gender
display `sex'
eststo `var'FE`sex': xtreg `var' $wage $states  diabetes if sex== `sex', fe ro
eststo `var'RE`sex': xtreg `var' age $wage_nf $states  diabetes if sex== `sex', re ro

eststo `var'FE`sex'_t: xtreg `var' $wage $states  type* if sex== `sex', fe ro
eststo `var'RE`sex'_t: xtreg `var' age $wage_nf $states  type* if sex== `sex', re ro
	}
}
	
	

estwrite * using "$estimates/fe_results_obesesample", id() replace
estread * using "$estimates/fe_results_obesesample"

	*TABLES FOR SELF-REPORTED DIABETES WITH FIXED EFFECTS

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*diabetes) nobaselevels  coeflabels(1.diabetes "Diabetes" diabetes "Diabetes") 

	*table for simple self-reported fixed effects model
esttab worksFE0 worksFE1 workhrsFE0 workhrsFE1 lnwage_hrFE0 lnwage_hrFE1  using "$table/FElabouroutcomes_obesesample.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.0f ) labels("N"))

esttab worksFE0 worksFE1 workhrsFE0 workhrsFE1 lnwage_hrFE0 lnwage_hrFE1  using "$table/FElabouroutcomes_obesesample.rtf", replace ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)) ///
b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) obslast  ///
collabels(none) nonumbers nogaps  keep(*diabetes) nobaselevels  coeflabels(1.diabetes "Diabetes" diabetes "Diabetes") stats(N, fmt(%9.0f ) labels("N"))

esttab worksRE0 worksRE1 workhrsRE0 workhrsRE1 lnwage_hrRE0 lnwage_hrRE1 using "$table/RElabouroutcomes_obesesample.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.0f ) labels("N"))


restore

/*BY DIABETES TYPE*/

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*type* obese) nobaselevels  coeflabels(type1 "Type 1" type2 "Type 2" ///
	 W_type1 "Early onset (within)" B_type1 "Early onset (between)" ///
	 W_type2 "Late onset (within)" B_type2 "Late onset (between)" obese "Obese (BMI \geq 30)" ) 

esttab worksFE0_t worksFE1_t workhrsFE0_t workhrsFE1_t lnwage_hrFE0_t lnwage_hrFE1_t using "$table/FElabouroutcomes_t.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.0f ) labels("N"))

esttab worksRE0_t worksRE1_t workhrsRE0_t workhrsRE1_t lnwage_hrRE0_t lnwage_hrRE1_t  using "$table/RElabouroutcomes_t.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.0f ) labels("N"))



       

/*SELECTION INTO DIFFERENT TYPES OF WORK*/

	*SIMPLE MODELS USING XTREG AND STATUS DUMMIES
	

forvalues sex=0/1{

eststo worksFE_employed`sex': xtreg status2 $controls i.diabetes $states if sex == `sex', fe cluster(pid_link)  // 
eststo worksFE_agricultural`sex': xtreg status3 $controls i.diabetes $states if  sex == `sex', fe cluster(pid_link)  // 
eststo worksFE_selfemployed`sex': xtreg status4 $controls i.diabetes $states if sex == `sex', fe cluster(pid_link)  // 

eststo worksRE_employed`sex': xtreg status2 age $controls_nf diabetes $states if sex == `sex', re cluster(pid_link)  // 
eststo worksRE_agricultural`sex': xtreg status3 age $controls_nf diabetes $states if sex == `sex', re cluster(pid_link)  // 
eststo worksRE_selfemployed`sex': xtreg status4 age $controls_nf diabetes $states if sex == `sex', re cluster(pid_link)  // 

eststo worksFE_employed`sex'_t: xtreg status2 $controls type* $states if sex == `sex', fe cluster(pid_link)  // 
eststo worksFE_agricultural`sex'_t: xtreg status3 $controls type* $states if  sex == `sex', fe cluster(pid_link)  // 
eststo worksFE_selfemployed`sex'_t: xtreg status4 $controls type* $states if sex == `sex', fe cluster(pid_link)  // 

eststo worksRE_employed`sex'_t: xtreg status2 age $controls type* $states if sex == `sex', re cluster(pid_link)  // 
eststo worksRE_agricultural`sex'_t: xtreg status3 age $controls type* $states if sex == `sex', re cluster(pid_link)  // 
eststo worksRE_selfemployed`sex'_t: xtreg status4 age $controls type* $states if sex == `sex', re cluster(pid_link)  // 
}


estwrite * using "$estimates/fe_results", id() append
estread * using "$estimates/fe_results"


	*TABLES FOR SELF-REPORTED DIABETES EFFECT ON EMPLOYMENT BY SECTOR

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*diabetes obese) nobaselevels  coeflabels(1.diabetes "Diabetes" diabetes "Diabetes" ///
 type1 "Type 1" type2 "Type 2" obese "Obese (BMI \geq 30)" ) 
	 
* FIXED EFFECTS	 
esttab worksFE_employed0 worksFE_agricultural0 worksFE_selfemployed0 worksFE_employed1 worksFE_agricultural1 worksFE_selfemployed1 using "$table/worktype_LPM_FE.tex", replace comp ///
 mti("Non-agric." "Agric."  "Self-employed" "Non-agric." "Agric."  "Self-employed" "Non-agric." "Agric." "Self-employed") mgroups(Males Females, pattern(1 0 0 1 0 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span}))  ${authorea} stats(N, fmt(%9.0f ) labels("N"))
       
* RANDOM EFFECTS

esttab worksRE_employed0 worksRE_agricultural0 worksRE_selfemployed0 worksRE_employed1 worksRE_agricultural1 worksRE_selfemployed1 using "$table/worktype_LPM_RE.tex", replace comp ///
 mti("Non-agric." "Agric."  "Self-employed" "Non-agric." "Agric."  "Self-employed" "Non-agric." "Agric." "Self-employed") mgroups(Males Females, pattern(1 0 0 1 0 0)                   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span}))  ${authorea} stats(N, fmt(%9.0f ) labels("N"))



/*BY DIABETES TYPE*/

 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*type* obese) nobaselevels  coeflabels(type1 "Type 1" type2 "Type 2" obese "Obese (BMI \geq 30)") 

esttab worksFE_employed0_t worksFE_employed1_t worksFE_agricultural0_t worksFE_agricultural1_t worksFE_selfemployed0_t worksFE_selfemployed1_t using "$table/worktype_FE_t.tex", replace comp ///
mti( "Males" "Females" "Males" "Females" "Males" "Females") mgroups( "Non-agric." "Agriculture" "Self-employed", pattern(1 0 1 0 1 0)   ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.3f %9.3f  %9.0f ) labels("N"))

esttab worksRE_employed0_t worksRE_employed1_t worksRE_agricultural0_t worksRE_agricultural1_t worksRE_selfemployed0_t worksRE_selfemployed1_t using "$table/worktype_RE_t.tex", replace comp ///
mti( "Males" "Females" "Males" "Females" "Males" "Females") mgroups( "Non-agric." "Agriculture" "Self-employed", pattern(1 0 1 0 1 0)   ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.3f %9.3f  %9.0f ) labels("N"))

       
log close       
       
       
//  DIABETES DURATION

	*linear specification
	
log using Logs/panel_finalmodels_duration.log, replace

global controls " age_sq i.smallcity i.city i.bigcity i.secondary i.highschool i.college_uni i.married  kids_hh i.indigenous wealth i.survey_year obese "
global controls_nf " age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh indigenous wealth survey_year2 - survey_year7 obese"
global fe " age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh indigenous wealth obese"
global years "survey_year2 - survey_year7"
global wage_nf " age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous insurance survey_year2-survey_year7 selfemployed agricultural obese"
global wage_fe " age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh wealth indigenous insurance selfemployed agricultural obese"



gen type1_years = type1*years_diabetes
gen type2_years = type2*years_diabetes


forvalues sex=0/1{  // by gender
display `sex'
eststo works_l_OLS`sex': reg works $controls age years_diabetes $states if sex== `sex' & t==2, ro  // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
eststo works_l_FE`sex': xtreg works $controls years_diabetes if sex== `sex', fe  ro // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
eststo works_l_RE`sex': xtreg works age $controls years_diabetes $states if sex== `sex', re  ro // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
}

forvalues sex=0/1{  // by gender
display `sex'
eststo works_l_OLS`sex': reg works $controls age type1_years type2_years $states if sex== `sex' & t==2, ro  // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
eststo works_l_FE`sex'_t: xtreg works $fe type1_years type2_years if sex== `sex', fe  ro // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
eststo works_l_RE`sex'_t: xtreg works age $controls type1_years type2_years $states if sex== `sex', re  ro // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
}   

* wages and workhours


forvalues sex=0/1{  // by gender
foreach var of varlist $Y1{
display `sex' `var'
eststo `var'_l_OLS`sex': reg `var' $wage $states age years_diabetes if sex== `sex' & t == 2,  ro
eststo `var'_l_FE`sex': xtreg `var' $wage years_diabetes if sex== `sex', ro fe
eststo `var'_l_RE`sex': xtreg `var' age $wage $states years_diabetes if sex== `sex', ro re

}
}
forvalues sex=0/1{  // by gender
foreach var of varlist $Y1{
eststo `var'_l_OLS`sex': reg `var' $wage $states age type1_years type2_years if sex== `sex' & t == 2,  ro
eststo `var'_l_FE`sex'_t: xtreg `var' $wage type2_years if sex== `sex', fe  ro // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration
eststo `var'_l_RE`sex'_t: xtreg `var' age $wage $states type1_years type2_years if sex== `sex', re  ro // effect size and significance is similar if I leave out survey year and age to prevent collinearity with diabetes duration

}
}

estwrite * using "$estimates/duration_FE", id() replace
estread * using "$estimates/duration_FE"


	*using splines
	
		*employment
  
 * do not estimate splines for type 1 because too few observations

foreach var of varlist diab_y1-diab_y4{  // diabetes splines by type
gen type1_`var' = `var'*type1
gen type2_`var' = `var'*type2
}


forvalues sex=0/1{  // by gender
display `sex'
eststo w_sp_OLS`sex': reg works $controls age diab_y*  $states if sex== `sex' & t== 2,  ro 
eststo w_sp_FE`sex': xtreg works $controls diab_y* if sex== `sex',  cluster(pid_link) fe
eststo w_sp_RE`sex': xtreg works age $controls diab_y*  $states if sex== `sex',  cluster(pid_link) re
}

forvalues sex=0/1{  // by gender and type
display `sex'	
eststo w_sp_OLS`sex': reg works $controls age type2_diab_y* $states if sex== `sex' & t== 2,  ro 	
eststo w_sp_FE`sex'_t: xtreg works $controls type2_diab_y* $years if sex== `sex',  cluster(pid_link) fe
eststo w_sp_RE`sex'_t: xtreg works age $controls type2_diab_y*  $states if sex== `sex',  cluster(pid_link) re
}

estwrite * using "$estimates/duration_FE", id() append

		* wages and workhours


forvalues sex=0/1{  // by gender
	foreach var of varlist $Y1{
	display `sex'
	eststo `var'_sp_OLS`sex': reg `var' $wage age diab_y* $states if sex== `sex' & t== 2,  ro 	
	eststo `var'_sp_FE`sex': xtreg `var' $wage diab_y* if sex== `sex', cluster(pid_link) fe
	eststo `var'_sp_RE`sex': xtreg `var' age $wage $states diab_y* if sex== `sex', cluster(pid_link) re
	}
}


/*No differentiation by diabetes type due to too little variation especially in y4*/
forvalues sex=0/1{  // by gender
	foreach var of varlist $Y1{
	display `sex'
	eststo `var'_sp_OLS`sex'_t: reg `var' $wage age type2_diab_y* $states if sex== `sex' & t== 2,  ro 		
	eststo `var'_sp_FE`sex'_t: xtreg `var' $wage type2_diab_y* if sex== `sex', cluster(pid_link) fe
	eststo `var'_sp_RE`sex'_t: xtreg `var' age $wage $states type2_diab_y* if sex== `sex', cluster(pid_link) re
	}
}

estwrite * using "$estimates/duration_FE", id() append






// using dummies


foreach var of varlist years_diabetes_groups12-years_diabetes_groups15{
gen t2`var' = `var'*type2
}



forvalues sex=0/1{  // by gender
	display `sex'
	eststo w_dumm_OLS`sex': reg works $controls age  i.years_diabetes_groups1 $states if sex== `sex' & t== 2,  ro 
	eststo w_dumm_FE`sex': xtreg works $controls i.years_diabetes_groups1 if sex== `sex',  cluster(pid_link) fe
	eststo w_dumm_RE`sex': xtreg works age $controls_nf i.years_diabetes_groups1  $states if sex== `sex',  cluster(pid_link) re
}
				
forvalues sex=0/1{  // by gender
	display `sex'
	eststo w_dumm_OLS`sex'_t: reg works $controls age  t2years_diabetes_g* $states if sex== `sex' & t== 2,  ro 
	eststo w_dumm_FE`sex'_t: xtreg works $controls t2years_diabetes_g*  if sex== `sex',  cluster(pid_link) fe
	eststo w_dumm_RE`sex'_t: xtreg works age $controls t2years_diabetes_g*  $states if sex== `sex',  cluster(pid_link) re
		
}


forvalues sex=0/1{  // by gender
	foreach var of varlist $Y1{
	display `sex'
	eststo `var'_dumm_OLS`sex': reg `var' $wage age  i.years_diabetes_groups1 $states if sex== `sex' & t== 2,  ro 
	eststo `var'_dumm_FE`sex': xtreg `var' $wage i.years_diabetes_groups1 if sex== `sex',  cluster(pid_link) fe
	eststo `var'_dumm_RE`sex': xtreg `var' age $wage i.years_diabetes_groups1 $states if sex== `sex',  cluster(pid_link) re
	}
}

/*No differentiation by diabetes type due to too little variation especially in y4*/
/*
forvalues sex=0/1{  // by gender
	foreach var of varlist $Y1{
	display `sex'		
	eststo `var'_dumm_OLS`sex'_t: reg `var' $wage age t2years_diabetes_g* $states if sex== `sex' & t== 2,  ro 	
	eststo `var'_dumm_FE`sex'_t: xtreg `var' $wage_nf t2years_diabetes_g* $states if sex== `sex',  cluster(pid_link) fe
	eststo `var'_dumm_RE`sex'_t: xtreg `var' $wage_nf t2years_diabetes_g* $states if sex== `sex',  cluster(pid_link) re
	eststo `var'_dumm_WB`sex'_t: xthybrid `var' $wage_nf t2years_diabetes_g* $states if sex == `sex', clusterid(pid_link) test use(t2years_diabetes_g*  $wage_nf) vce(cluster pid_link) full 
		foreach var un 12 13 14 15{
		test B_t2years_diabetes_g`var'=W_t2years_diabetes_g`var'
		estadd scalar p_wb_`var' = r(p)
		}
	}
}
*/

estwrite * using "$estimates/duration_FE", id() append
estread *dumm* using "$estimates/duration_FE"
estread *_l* using "$estimates/duration_FE"
estread *sp* using "$estimates/duration_FE"



	*TABLES FOR DIABETES DURATION WITH WITHIN-BETWEEN MODEL

 global authoreal b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  ///
       coeflabels(years_diabetes "Years since diagnosis"obese "Obese (BMI \geq 30)") 

	*table for simple self-reported fixed effects model
esttab works_l_FE0 works_l_FE1 workhrs_l_FE0 workhrs_l_FE1 lnwage_hr_l_FE0 lnwage_hr_l_FE1 using "$table/FElabouroutcomes_duration.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal} stats(N, fmt(%9.0f ) labels("N")) keep(*years_diabetes obese)

esttab works_l_RE0 works_l_RE1 workhrs_l_RE0 workhrs_l_RE1 lnwage_hr_l_RE0 lnwage_hr_l_RE1 using "$table/RElabouroutcomes_duration.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal} stats(N, fmt(%9.0f ) labels("N")) keep(*years_diabetes obese)
       
       *by diabetes onset
 global authorea b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01) booktabs obslast  ///
	 alignment(S S)   collabels(none) nonotes nonumbers nogaps  keep(*type* obese) nobaselevels  coeflabels(type1_years "Early onset" type2_years "Late onset" ///
	 obese "Obese (BMI \geq 30)") 
	        
esttab works_l_FE0_t works_l_FE1_t workhrs_l_FE0_t workhrs_l_FE1_t lnwage_hr_l_FE0_t lnwage_hr_l_FE1_t using "$table/FElabouroutcomes_duration_t.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.0f ) labels("N")) 

esttab works_l_RE0_t works_l_RE1_t workhrs_l_RE0_t workhrs_l_RE1_t lnwage_hr_l_RE0_t lnwage_hr_l_RE1_t using "$table/RElabouroutcomes_duration_t.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${authorea} stats(N, fmt(%9.0f ) labels("N"))
       

*Splines
esttab w_sp_RE0 w_sp_RE1 workhrs_sp_RE0 workhrs_sp_RE1  lnwage_hr_sp_RE0 lnwage_hr_sp_RE1 ///
using "$table/duration_RE_sp.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal}  keep(*diab_y* obese) stats(N, fmt(%9.0f ) labels("N"))

esttab w_sp_FE0 w_sp_FE1 workhrs_sp_FE0 workhrs_sp_FE1 lnwage_hr_sp_FE0 lnwage_hr_sp_FE1 ///
using "$table/duration_FE_sp.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal}  keep(*diab_y* obese) stats(N, fmt(%9.0f ) labels("N"))
       
       
*Dummies

esttab w_dumm_RE0 w_dumm_RE1 workhrs_dumm_RE0 workhrs_dumm_RE1 lnwage_hr_dumm_RE0 lnwage_hr_dumm_RE1 ///
using "$table/duration_RE_dumm.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) ${authoreal}  keep(*years_diabetes_groups* obese) stats(N, fmt(%9.0f ) labels("N"))

esttab w_dumm_FE0 w_dumm_FE1 workhrs_dumm_FE0 workhrs_dumm_FE1 lnwage_hr_dumm_FE0 lnwage_hr_dumm_FE1 ///
 using "$table/duration_FE_dumm.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(}) /// 
       span erepeat(\cmidrule(lr){@span})) ${authoreal}  keep(*years_diabetes_groups* obese) stats(N, fmt(%9.0f ) labels("N"))
       
       

************************************************************************************************************************
/*********************************BIOMARKER ANALYSIS*******************************************************************/
************************************************************************************************************************
clear matrix
set more off, perm
capture log close

global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""

global table "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Tables"
global estimates "/home/till/Dropbox/PhD/Diabetes Mexico/Second paper/Estimates"


cd $homefolder

		use "Paneldaten/final.dta", clear
		
log using Logs/biomarkers.log, replace
 		

global wage1 "age age_sq i.smallcity i.city i.bigcity i.central i.westcentr i.northeastcentr i.northwest i.secondary i.highschool i.college_uni i.married wealth i.indigenous i.insurance i.worktype i.survey_year "  // no diabetes included to be used with years since diagnosis

global controls "age age_sq i.smallcity i.city i.bigcity i.primary i.secondary i.highschool i.college_uni i.married kids_hh i.indigenous i.insurance wealth i.survey_year"

global Y1 "lnwage_hr workhrs"

global controls_nf "age age_sq smallcity city bigcity secondary highschool college_uni married  kids_hh indigenous wealth survey_year2-survey_year7"

drop if diab_controlled == 1 
drop if diabetes ==.

/*First I estimate a model for comparison only using self-reported diabetes for the subsample*/

	/*EMPLOYMENT*/

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_bio`sex': reg works $controls $states i.diabetes if sex== `sex' & t==2 & bio ==1, ro
eststo works_diab_bio`sex'_fe: xtreg works $controls $states i.diabetes if sex== `sex' & t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

}
	/*WAGES AND WORK HOURS*/

foreach var of varlist $Y1{
eststo `var'diab_bio: reg `var' $wage1 sex i.diabetes if t==2 & bio ==1, ro
eststo `var'diab_bio_fe: xtreg `var' $wage1 sex i.diabetes if t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_bio`sex': reg `var' $wage1 i.diabetes if sex== `sex' & t==2 & bio ==1, cluster(folio)
eststo `var'diab_bio`sex'_fe: xtreg `var' $wage1 i.diabetes if sex== `sex' & t==2 & bio ==1, fe i(loc_id) cluster(loc_id)

	}
}

       

/*EVERYBODY WITH AN HBA1C > 6.4 */

	/*EMPLOYMENT*/

forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_all`sex': reg works $controls $states i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_all`sex'_fe: xtreg works $controls $states i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


	/*WAGES AND work hours*/

foreach var of varlist $Y1{
eststo `var'diab_all: reg `var' $wage1 sex i.diab_hba1c if t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_all_fe: xtreg `var' $wage1 sex i.diab_hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_all`sex': reg `var' $wage1 i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_all`sex'_fe: xtreg `var' $wage1 i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}


esttab works_diab_all works_diab_all0 works_diab_all1 workhrsdiab_all workhrsdiab_all0 workhrsdiab_all1 lnwage_hrdiab_all lnwage_hrdiab_all0 lnwage_hrdiab_all1 using "$table/labouroutcomes_diabetes_hba1c.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab_hba1c) label nobase

esttab works_diab_all_fe works_diab_all0_fe works_diab_all1_fe workhrsdiab_all_fe workhrsdiab_all0_fe workhrsdiab_all1_fe lnwage_hrdiab_all_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe  using "$table/labouroutcomes_diabetes_hba1c_fe.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab_hba1c) label nobase

/*HbA1c Diabetes indicator and self-reported diabetes in one specification*/

eststo works_sr_a1c_fe: xtreg works $controls $states i.diab_hba1c i.diabetes sex if t==2 & bio ==1 & diabetes <., fe i(loc_id)
eststo works_sr_a1c_fe_ia: xtreg works $controls $states i.diab_hba1c##i.diabetes sex if t==2 & bio ==1 & diabetes <., fe i(loc_id)
lincom 1.diabetes + 1.diab_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1

estadd scalar skal_sr r(estimate)
estadd scalar skalsd_sr r(se)
lincom 1.diab_hba1c + 1.diab_hba1c#1.diabetes  //effect of biomarker diagnosed diabetes irrespective of diagnosis when controlling for sr false positives
estadd scalar skal_bio r(estimate)
estadd scalar skalsd_bio r(se)

forvalues sex=0/1{
display `sex' `diab'
//eststo works_sr_a1c`sex': reg works $controls $states i.diab_hba1c i.diabetes  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_sr_a1c`sex': reg works $controls $states i.diabetes##i.diab_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_sr_a1c`sex'_fe_ia: xtreg works $controls $states i.diabetes##i.diab_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
lincom 1.diabetes + 1.diab_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  works_sr_a1c`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  works_sr_a1c`sex'_fe_ia
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  works_sr_a1c`sex'_fe_ia
}



foreach var of varlist $Y1{
eststo `var'sr_a1c: reg `var' $wage1 sex i.diab_hba1c i.diabetes if t==2 & bio ==1 & diabetes <., ro
eststo `var'sr_a1c_fe: xtreg `var' $wage1 sex i.diab_hba1c i.diabetes if t==2 & bio ==1 & diabetes <., i(loc_id) fe
eststo `var'sr_a1c_fe_ia: xtreg `var' $wage1 sex i.diab_hba1c##i.diabetes if t==2 & bio ==1 & diabetes <., i(loc_id) fe
lincom 1.diabetes + 1.diab_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) : `var'sr_a1c_fe_ia
estadd scalar skalsd_sr r(se) :  `var'sr_a1c_fe_ia
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) : `var'sr_a1c_fe_ia

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'sr_a1c`sex': reg `var' $wage1 i.diab_hba1c i.diabetes if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'sr_a1c`sex'_fe: xtreg `var' $wage1 i.diab_hba1c i.diabetes if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
eststo `var'sr_a1c`sex'_fe_ia: xtreg `var' $wage1 i.diab_hba1c##i.diabetes if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
lincom 1.diabetes + 1.diab_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) : `var'sr_a1c`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  `var'sr_a1c`sex'_fe_ia
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'sr_a1c`sex'_fe_ia
}
}


/*ONLY UNDIAGNOSED*/
       
forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_ud`sex': reg works $controls $states i.diab_ud  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_ud`sex'_fe: xtreg works $controls $states i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


foreach var of varlist $Y1{
eststo `var'diab_ud: reg `var' $wage1 sex i.diab_ud if t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_ud_fe: xtreg `var' $wage1 sex i.diab_ud if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud`sex': reg `var' $wage1 i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_ud`sex'_fe: xtreg `var' $wage1 i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}

/*DIABETES UNDIAGNOSED if HBA1C > 8 */

gen diab_ud_h = diab_ud
replace diab_ud_h = 0 if hba1c < 8 & diab_ud == 1
  
  
  forvalues sex=0/1{
display `sex' `diab'
eststo works_diab_ud_h`sex': reg works $controls $states i.diab_ud_h  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_ud_h`sex'_fe: xtreg works $controls $states i.diab_ud_h if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


foreach var of varlist $Y1{
eststo `var'diab_ud_h: reg `var' $wage1 sex i.diab_ud_h if t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_ud_h_fe: xtreg `var' $wage1 sex i.diab_ud_h if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud_h`sex': reg `var' $wage1 i.diab_ud_h if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_ud_h`sex'_fe: xtreg `var' $wage1 i.diab_ud_h if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}
       

/*SELF-REPORTED DIABETES AND UNDIAGNOSED DIABETES IN ONE SPECIFICATION*/


	/*EMPLOYMENT*/
eststo works_diab_ud_sr: reg works $controls $states sex i.diabetes i.diab_ud if t==2 & bio ==1 & diabetes <., ro
eststo works_diab_ud_sr_fe: xtreg works $controls $states sex i.diabetes i.diab_ud if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe

forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_ud_sr`sex': reg works $controls $states i.diabetes i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_ud_sr`sex'_fe: xtreg works $controls $states i.diabetes i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <. & diab_controlled == 0, i(loc_id) fe 	
}




	/*WAGES AND work hours*/
foreach var of varlist $Y1{
eststo `var'diab_ud_sr: reg `var' $wage1 $states sex i.diabetes i.diab_ud if t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_ud_sr_fe: xtreg `var' $wage1 $states  sex diabetes diab_ud if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 	

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_ud_sr`sex': reg `var' $wage1 $states  i.diabetes i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'diab_ud_sr`sex'_fe: xtreg `var' $wage1 $states i.diabetes i.diab_ud if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 

	}
}





/*DIABETES SEVERITY USING CONTINUOUS HBA1C VARIABLE INSTEAD OF DUMMIES (Panel D Table 9)*/

gen ud_hba1c = diab_ud*hba1c - 6.4 if diab_ud == 1
replace ud_hba1c = 0 if diab_ud == 0
gen diab_sr_hba1c = diabetes*hba1c - 6.4 if diabetes ==1 & hba1c >= 6.5
replace diab_sr_hba1c = 0 if diabetes == 0 & hba1c <.
gen diab_all_hba1c =  diab_hba1c * hba1c - 6.4 if diab_hba1c == 1
replace diab_all_hba1c = 0 if diab_hba1c == 0

forvalues sex=0/1{
display `sex' `diab'
*eststo works_bio_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_bio_sev`sex'_fe: xtreg works $controls $states i.diabetes  diab_all_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
eststo works_bio_sev`sex'_fe_ia: xtreg works $controls $states i.diabetes##c.diab_all_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
lincom 1.diabetes + c.diab_all_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  works_bio_sev`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  works_bio_sev`sex'_fe_ia
}	



foreach var of varlist $Y1{
eststo `var'bio_sev: reg `var' $wage1 $states sex i.diabetes  diab_all_hba1c if t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_sev_fe: xtreg `var' $wage1 $states  sex i.diabetes diab_all_hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 	
eststo `var'bio_sev_fe_ia: xtreg `var' $wage1 $states  sex i.diabetes##c.diab_all_hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 	
lincom 1.diabetes + c.diab_all_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate):  `var'bio_sev_fe_ia
estadd scalar skalsd_sr r(se) :  `var'bio_sev_fe_ia

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'bio_sev`sex': reg `var' $wage1 $states  i.diabetes diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_sev`sex'_fe_ia: xtreg `var' $wage1 $states i.diabetes##c.diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 
lincom 1.diabetes + c.diab_all_hba1c#1.diabetes  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'bio_sev`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  `var'bio_sev`sex'_fe_ia

	}
}



//Diabetes and other disease indicators (Panel B Table 10)
eststo works_biochronic_fe_ia: xtreg works $controls $states i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease sex  if t==2 & bio ==1 & diabetes <., fe i(loc_id)

       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_bio_chronic`sex': reg works $controls $states i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_biochronic`sex'_fe_ia: xtreg works $controls $states i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease  if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  works_biochronic`sex'_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  works_biochronic`sex'_fe_ia
estadd scalar skalsd_sr r(se) :  works_biochronic`sex'_fe_ia

}


foreach var of varlist $Y1{
//eststo `var'_bio_chronic: reg `var' $wage1 sex i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if t==2 & bio ==1 & diabetes <., ro
eststo `var'_biochronic_fe_ia: xtreg `var' $wage1 sex i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease systolic if t==2 & bio ==1 & diabetes <., i(loc_id) fe
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'_biochronic_fe_ia
estadd scalar skalsd_sr r(se) : `var'_biochronic_fe_ia
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'_biochronic_fe_ia

forvalues sex=0/1{
display "`var'"`sex'
//eststo `var'_biochronic`sex': reg `var' $wage1 i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'_biochronic`sex'_fe_ia: xtreg `var' $wage1 i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'_biochronic`sex'_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'_biochronic`sex'_fe_ia
estadd scalar skalsd_sr r(se) : `var'_biochronic`sex'_fe_ia
	}
}
       




       
//Diabetes and self-reported health (Panel C Table 10)

       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_biohealth`sex': reg works $controls $states i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_biohealth`sex'_fe_ia: xtreg works $controls $states i.diab_hba1c##i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) : works_biohealth`sex'_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  works_biohealth`sex'_fe_ia
estadd scalar skalsd_sr r(se) : works_biohealth`sex'_fe_ia

}


foreach var of varlist $Y1{
eststo `var'_biohealth_ia: reg `var' $wage1 sex i.diab_hba1c##i.diabetes i.health_sr if t==2 & bio ==1 & diabetes <., ro
eststo `var'_biohealth_fe_ia: xtreg `var' $wage1 sex i.diab_hba1c##i.diabetes i.health_sr if t==2 & bio ==1 & diabetes <., i(loc_id) fe
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'_biohealth_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'_biohealth_fe_ia
estadd scalar skalsd_sr r(se) : `var'_biohealth_fe_ia

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'_biohealth`sex': reg `var' $wage1 i.diab_hba1c##i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'_biohealth`sex'_fe_ia: xtreg `var' $wage1 i.diab_hba1c##i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
test 1.diabetes + 1.diab_hba1c#1.diabetes = 1.diab_hba1c
estadd scalar ftest r(p) :  `var'_biohealth`sex'_fe_ia
lincom 1.diabetes + 1.diabetes#1.diab_hba1c  // effect of sr-diabetes both true positives and false positives when controlling for undiagnosed diabetes
estadd scalar skal_sr r(estimate) :  `var'_biohealth`sex'_fe_ia
estadd scalar skalsd_sr r(se) : `var'_biohealth`sex'_fe_ia
	}
}
       
       
       


estwrite * using "$estimates/biomarker_results", id() replace
estread * using "$estimates/biomarker_results"


/*New table*/
 
 
 global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01)  booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) nonotes  /// 
	 addnote("Robust standard errors in parentheses" ///
	 "Other control variables:age, age squared, state dummies, urbanisation dummies, education dummies, married dummy, number children < 6 and wealth" ///
	 "Calender year dummies are included as data collection for the third wave was streched out over several years" ///
	 "The wage and working hour models additionally control for type of work (agricultural and self employed with non-agricultural employment as the base)" "and for health insurance status")


  


esttab works_diab_bio0_fe works_diab_bio1_fe workhrsdiab_bio0_fe workhrsdiab_bio1_fe lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", replace comp nogaps ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*) label fragment noobs 
       
esttab works_diab_all0_fe works_diab_all1_fe workhrsdiab_all0_fe workhrsdiab_all1_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diab*) label fragment noobs nomtitles nonumbers 
       
esttab works_sr_a1c0_fe_ia works_sr_a1c1_fe_ia	workhrssr_a1c0_fe_ia workhrssr_a1c1_fe_ia  lnwage_hrsr_a1c0_fe_ia lnwage_hrsr_a1c1_fe_ia  ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diab*) label fragment noobs nomtitles nonumbers ///
      stats(skal_sr skalsd_sr ftest, layout(@ (@) @) labels("Linear Combination: Self-reported" " " "F-test: $\beta_{1}+\beta_{3} = \beta_{2}$") fmt(%9.3f %9.3f %9.3f))
       
esttab works_bio_sev0_fe_ia works_bio_sev1_fe_ia workhrsbio_sev0_fe_ia workhrsbio_sev1_fe_ia lnwage_hrbio_sev0_fe_ia lnwage_hrbio_sev1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
 ${table_reduced}  keep(1.diab* diab_all_hba1c) label fragment nomtitles nonumbers 
 
esttab works_biochronic0_fe_ia works_biochronic1_fe_ia workhrs_biochronic0_fe_ia workhrs_biochronic1_fe_ia lnwage_hr_biochronic0_fe_ia lnwage_hr_biochronic1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diab* overweight obese hypertension heart_disease) label fragment noobs nomtitles nonumbers ///
      stats(skal_sr skalsd_sr ftest, layout(@ (@) @) labels("Linear Combination: Self-reported" " " "F-test: $\beta_{1}+\beta_{3} = \beta_{2}$") fmt(%9.3f %9.3f %9.3f))

esttab works_biohealth0_fe_ia works_biohealth1_fe_ia workhrs_biohealth0_fe_ia workhrs_biohealth1_fe_ia lnwage_hr_biohealth0_fe_ia lnwage_hr_biohealth1_fe_ia   ///
 using "$table/labouroutcomes_all_biomarkermodels_fe.tex", append comp nogaps   ///
 ${table_reduced}  keep(1.diab* *.health_sr) label fragment nomtitles nonumbers   ///
      stats(skal_sr skalsd_sr ftest, layout(@ (@) @) labels("Linear Combination: Self-reported" " " "F-test: $\beta_{1}+\beta_{3} = \beta_{2}$") fmt(%9.3f %9.3f %9.3f))



/*Old tables*/

esttab works_diab_ud_sr works_diab_ud_sr0 works_diab_ud_sr1  workhrsdiab_ud_sr workhrsdiab_ud_sr0 workhrsdiab_ud_sr1 lnwage_hrdiab_ud_sr lnwage_hrdiab_ud_sr0 lnwage_hrdiab_ud_sr1 using "$table/labouroutcomes_diagnosed_undiagnosed.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*)

esttab works_diab_ud_sr_fe works_diab_ud_sr0_fe works_diab_ud_sr1_fe workhrsdiab_ud_sr_fe workhrsdiab_ud_sr0_fe workhrsdiab_ud_sr1_fe lnwage_hrdiab_ud_sr_fe lnwage_hrdiab_ud_sr0_fe lnwage_hrdiab_ud_sr1_fe using "$table/labouroutcomes_diagnosed_undiagnosed_fe.tex", replace comp ///
 mti("Pooled" "Males" "Females" "Pooled" "Males" "Females" "Pooled" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 0 1 0 0 1 0 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*)
       
       
// have 1 big table per dependent variable including all community fixed effects estimates sr_a1c
esttab works_diab_bio0_fe works_diab_bio1_fe works_diab_all0_fe works_diab_all1_fe works_sr_a1c0_fe works_sr_a1c1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_employment_fe.tex", replace comp nogaps ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Self-reported" "A1c" "A1c and self-reported", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*) label nogaps
       
esttab  lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe lnwage_hrsr_a1c0_fe lnwage_hrsr_a1c1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_wage.tex", replace comp nogaps ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Self-reported" "A1c" "A1c and self-reported", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*) label  fragment nogaps
       
esttab workhrsdiab_bio0_fe workhrsdiab_bio1_fe workhrsdiab_all0_fe workhrsdiab_all1_fe workhrssr_a1c0_fe workhrssr_a1c1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_hours.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Self-reported" "A1c" "A1c and self-reported", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab*) label  fragment nogaps


label define diabetes 0 "No diabetes diagnosis" 1 "Diabetes diagnosis", replace
label val diabetes diabetes
label define diab_hba1c 0 "HbA1c $< 6.5\%$" 1 "HbA1c $\geq 6.5\%$", replace
label val diab_hba1c diab_hba1c
eststo summary: estpost tab diabetes diab_hba1c if sample ==1, col row
bysort sex: tab diabetes diab_hba1c if sample ==1, col row

esttab summary using "$table/bio_matrix.tex", replace unstack cells("b(label(freq)) pct(label(\%)fmt(2))") booktabs noobs nonumber collabels("{n}" "\%")   ///
 eqlabels("HbA1c $< 6.5\%$" "HbA1c $\geq 6.5\%$", prefix(\multicolumn{@span}{c}{) suffix(}) ///
       span erepeat(\cmidrule(lr){@span})) alignment(S S) nolz
 
/*DIABETES SEVERITY USING CONTINUOUS HBA1C VARIABLE INSTEAD OF DUMMIES (Panel A Table 10)*/

gen ud_hba1c = diab_ud*hba1c
gen diab_sr_hba1c = diabetes*hba1c
gen diab_all_hba1c =  diab_hba1c * hba1c

forvalues sex=0/1{
display `sex' `diab'
*eststo works_bio_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_bio_sev`sex'_fe_ia: xtreg works $controls $states i.diabetes##c.diab_all_hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
}	



foreach var of varlist $Y1{
eststo `var'bio_sev_ia: reg `var' $wage1 $states sex i.diabetes##c.diab_all_hba1c if t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_sev_fe_ia: xtreg `var' $wage1 $states  sex diabetes##c.diab_all_hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 	

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'bio_sev`sex'_ia: reg `var' $wage1 $states  i.diabetes##c.diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_sev`sex'_fe_ia: xtreg `var' $wage1 $states i.diabetes##c.diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe 

	}
}


esttab works_bio_sev0_fe_ia works_bio_sev1_fe_ia workhrsbio_sev0_fe_ia workhrsbio_sev1_fe_ia lnwage_hrbio_sev0_fe_ia lnwage_hrbio_sev1_fe_ia using "$table/labouroutcomes_sev_fe.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* diab_all_hba1c) label 


//Diabetes and other disease indicators (Panel B Table 10)

       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_bio_chronic`sex': reg works $controls $states i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo worksbio_chronic`sex'_fe_ia: xtreg works $controls $states i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease  if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


foreach var of varlist $Y1{
//eststo `var'_bio_chronic: reg `var' $wage1 sex i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if t==2 & bio ==1 & diabetes <., ro
eststo `var'bio_chronic_fe_ia: xtreg `var' $wage1 sex i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease systolic if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
//eststo `var'_bio_chronic`sex': reg `var' $wage1 i.diabetes diab_all_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_chronic`sex'_fe_ia: xtreg `var' $wage1 i.diabetes##i.diab_hba1c overweight obese hypertension heart_disease systolic if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}
       
esttab worksbio_chronic0_fe_ia worksbio_chronic1_fe_ia workhrsbio_chronic0_fe_ia workhrsbio_chronic1_fe_ia lnwage_hrbio_chronic0_fe_ia lnwage_hrbio_chronic1_fe_ia using "$table/labouroutcomes_diagnosed_undiagnosed_fe_chronic.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0 )    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* overweight obese hypertension heart_disease) label nobase


       
//Diabetes and self-reported health (Panel C Table 10)

       
forvalues sex=0/1{
display `sex' `diab'
//eststo works_bio_health`sex': reg works $controls $states i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo worksbio_health`sex'_fe_ia: xtreg works $controls $states i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., fe i(loc_id)
}


foreach var of varlist $Y1{
eststo `var'bio_health: reg `var' $wage1 sex i.diab_hba1c##i.diabetes i.health_sr if t==2 & bio ==1 & diabetes <., ro
eststo `var'bio_health_fe_ia: xtreg `var' $wage1 sex i.diab_hba1c i.diabetes i.health_sr if t==2 & bio ==1 & diabetes <., i(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'bio_health`sex': reg `var' $wage1 i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., cluster(folio)
eststo `var'bio_health`sex'_fe_ia: xtreg `var' $wage1 i.diab_hba1c i.diabetes i.health_sr if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) fe
	}
}
       
esttab worksbio_health0_fe_ia worksbio_health1_fe_ia workhrsbio_health0_fe_ia workhrsbio_health1_fe_ia lnwage_hrbio_health0_fe_ia lnwage_hrbio_health1_fe_ia using "$table/labouroutcomes_diagnosed_undiagnosed_fe_srhealth.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0 )    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* *.health_sr) label nobase
       


* Table 10

global table_reduced b(%9.3f) se(%9.3f) star(* 0.10 ** 0.05 *** 0.01)  booktabs obslast nolz  ///
	 alignment(S S)   collabels(none) nonotes


  


esttab works_diab_bio0_fe works_diab_bio1_fe workhrsdiab_bio0_fe workhrsdiab_bio1_fe lnwage_hrdiab_bio0_fe lnwage_hrdiab_bio1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", replace comp nogaps ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)   ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diabetes) label fragment noobs 
       
esttab works_diab_all0_fe works_diab_all1_fe workhrsdiab_all0_fe workhrsdiab_all1_fe lnwage_hrdiab_all0_fe lnwage_hrdiab_all1_fe ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diab_hba1c) label fragment noobs nomtitles nonumbers 
       
esttab works_sr_a1c0_fe_ia works_sr_a1c1_fe_ia workhrssr_a1c0_fe_ia workhrssr_a1c1_fe_ia  lnwage_hrsr_a1c0_fe_ia lnwage_hrsr_a1c1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diabetes 1.diab_hba1c) label fragment noobs nomtitles nonumbers ///
      stats(skal_sr skalsd_sr skal_bio skalsd_bio, layout(@ (@) @ @) labels("Linear Combination: Self-reported" " " "Linear Combination: Biomarker" " ") fmt(%9.3f %9.3f %9.3f %9.3f))
       
esttab works_bio_sev0_fe_ia works_bio_sev1_fe_ia workhrsbio_sev0_fe_ia workhrsbio_sev1_fe_ia lnwage_hrbio_sev0_fe_ia lnwage_hrbio_sev1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
 ${table_reduced}  keep(1.diabetes diab_all_hba1c) label fragment  noobs nomtitles nonumbers 

esttab worksbio_chronic1_fe_ia worksbio_chronic1_fe_ia workhrsbio_chronic0_fe_ia workhrsbio_chronic1_fe_ia lnwage_hrbio_chronic0_fe_ia lnwage_hrbio_chronic1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
      ${table_reduced}  keep(1.diabetes 1.diab_hba1c) label fragment noobs nomtitles nonumbers 
      
esttab worksbio_health0_fe_ia worksbio_health1_fe_ia workhrsbio_health0_fe_ia workhrsbio_health1_fe_ia lnwage_hrbio_health0_fe_ia lnwage_hrbio_health1_fe_ia ///
 using "$table/labouroutcomes_all_biomarkermodels_fe_condensed.tex", append comp nogaps   ///
 ${table_reduced}  keep(1.diabetes 1.diab_hba1c) label fragment noobs nomtitles nonumbers  





       
// DIABETES SEVERITY FOR DIAGNOSED AND UNDIAGNOSED USING DIABETES DUMMIES BECAUSE USING INTERACTION TERMS DOES NOT WORK AS EVERYBODY WITH HBA1C
// ABOVE 6.4 AND WITHOUT DIAGNOSIS IS AUTOMATICALLY UNDIAGNOSED. HENCE THERE IS NO REFERENCE GROUP OF THOSE WITH UNDIAGNOSED DIABETES BELOW HBA1C 6.4


eststo works_diab_sev: reg works $controls $states sex i.diabetes i.diab_hba1c hba1c if t==2 & bio ==1 & diabetes <., ro
eststo works_diab_sev_fe: xtreg works $controls $states sex i.diabetes i.diab_hba1c hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe


forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo works_diab_sev`sex'_fe: xtreg works $controls $states i.diabetes i.diab_hba1c i.health_sr hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
}	

	/*WAGES AND work hours*/
foreach var of varlist $Y1{
eststo `var'diab_sev: reg `var' $wage1 $states sex i.diabetes i.diab_hba1c hba1c if t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_sev_fe: xtreg `var' $wage1 $states sex i.diabetes i.diab_hba1c hba1c if t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe

forvalues sex=0/1{
display "`var'"`sex'
eststo `var'diab_sev`sex': reg `var' $wage1 $states i.diabetes i.diab_hba1c hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
eststo `var'diab_sev`sex'_fe: xtreg `var' $wage1 $states i.diabetes i.diab_hba1c hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
	}
}

/*esttab works_diab_sev0 works_diab_sev1 lnwage_hrdiab_sev0 lnwage_hrdiab_sev1 workhrsdiab_sev0 workhrsdiab_sev1 using "$table/labouroutcomes_sev.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* hba1c) label*/
       
esttab works_diab_sev0_fe works_diab_sev1_fe  workhrsdiab_sev0_fe workhrsdiab_sev1_fe lnwage_hrdiab_sev0_fe lnwage_hrdiab_sev1_fe using "$table/labouroutcomes_sev_fe.tex", replace comp ///
 mti("Males" "Females" "Males" "Females" "Males" "Females") mgroups("Employment" "Weekly work hours" "Log hourly wages", pattern(1 0 1 0 1 0)    ///
       prefix(\multicolumn{@span}{c}{) suffix(})   ///
       span erepeat(\cmidrule(lr){@span})) ${table_reduced}  keep(1.diab* hba1c) label







forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
xtreg works $controls $states diab_all_hba1c if sex== `sex' & t==2 & bio ==1 & diabetes <., i(loc_id) cluster(loc_id) fe
}	


clonevar diabetes_treat= ec02aa_1
replace diabetes_treat = 0 if diabetes <1 
recode diabetes_treat (3 = 2) (4=3)

gen diabetes_med = 0 if diabetes <.
replace diabetes_med = 1 if ec02a == 0
replace diabetes_med = 2 if ec02a == 1

forvalues sex=0/1{
display `sex' `diab'
//eststo works_diab_sev`sex': reg works $controls $states i.diabetes i.diab_hba1c hba1c  if sex== `sex' & t==2 & bio ==1 & diabetes <., ro
xtreg works i.diabetes_med $controls $states  if sex== `sex', cluster(pid_link) fe
}	




/*Obesity checks*/

forvalues sex=0/1{
foreach var of varlist lnwage_hr workhrs { // by gender
display `sex'
eststo `var'WB`sex'_obese: xthybrid `var' age $wage_nf $states overweight obese  diabetes if sex== `sex', clusterid(pid_link) test use(diabetes $controls_nf overweight obese) vce(cluster pid_link) full
test B_diabetes=W_diabetes
	}
}
	
forvalues sex=0/1{
foreach var of varlist lnwage_hr workhrs { // by gender
display `sex'
xthybrid `var' age $wage_nf $states diabetes if sex== `sex' & obese <., clusterid(pid_link) test use(diabetes $controls_nf) vce(cluster pid_link) full
test B_diabetes=W_diabetes
	}
}
