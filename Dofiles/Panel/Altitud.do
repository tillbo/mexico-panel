*******************************************************************
********************MUNICIPIOS AND ALTITUD IN MXFLS****************
*******************************************************************

/*I used this site to find tha altitude http://www.municipios.mx/. If it was not on there I used the INEGI page http://www.inegi.org.mx/geo/contenidos/geoestadistica/consulta_localidades.aspx . Potentially, one should redo it only using the INEGI information to have only one source of information*/

gen altitud = .
/*Baja California Sur*/
replace altitud = 57 if ent == 3 & mpio == 1  // Ciudad Constitución
replace altitud = 27 if ent == 3 & mpio == 3  // La paz
replace altitud = 16 if ent == 3 & mpio == 2  // Mulege
replace altitud = 2 if ent == 3 & mpio == 9  // Loreto
replace altitud = 2 if ent == 3 & mpio == 45  // San José del Cabo
/*Baja California*/
replace altitud = 21 if ent == 2 & mpio == 1  // Ensenada
replace altitud = 10 if ent == 2 & mpio == 2  // Mexicali
replace altitud = 31 if ent == 2 & mpio == 4  // Tijuana
replace altitud = 10 if ent == 2 & mpio == 5  // Rosarito in Playas de Rosarito
/*Campeche*/
replace altitud = 10 if ent == 4 & mpio == 3  // Ciudad del Carmen
/*Coahuila*/
replace altitud = 422 if ent == 5 & mpio == 5  // Candela
replace altitud = 740 if ent == 5 & mpio == 7  // Cuatrociénegas
replace altitud = 1100 if ent == 5 & mpio == 9  // Francisco I. Madero
replace altitud = 580 if ent == 5 & mpio == 10  // Frontera
replace altitud = 1120 if ent == 5 & mpio == 17  // Matamoros
replace altitud = 504 if ent == 5 & mpio == 20  // Múzquiz
replace altitud = 518 if ent == 5 & mpio == 21  // Nadadores
replace altitud = 223 if ent == 5 & mpio == 25  // Piedras Negras
replace altitud = 494 if ent == 5 & mpio == 31  // San Buenaventura
replace altitud = 1090 if ent == 5 & mpio == 33  // San Pedro
replace altitud = 1120 if ent == 5 & mpio == 35  // Torreón
/*Colima*/
replace altitud = 500 if ent == 6 & mpio == 2  // Colima
/*Chiapas*/
replace altitud = 521 if ent == 7 & mpio == 27  // Chiapa del Corzo
replace altitud = 60 if ent == 7 & mpio == 65  // Palenque
replace altitud = 177 if ent == 7 & mpio == 89  // Tapachula
/*DF*/
replace altitud = 2240 if ent == 9 & mpio == 2  // Azcapotzalco
replace altitud = 2240 if ent == 9 & mpio == 3  // Coyoacán
replace altitud = 2240 if ent == 9 & mpio == 5  // Gustavo A. Madero
replace altitud = 2235 if ent == 9 & mpio == 6  // Iztacalco
replace altitud = 2247 if ent == 9 & mpio == 7  // Iztapalapa
replace altitud = 2350 if ent == 9 & mpio == 8  // La Magdalena Contreras
replace altitud = 2410 if ent == 9 & mpio == 9  // Milpa Alta
replace altitud = 2320 if ent == 9 & mpio == 10  // Alvaro Obregón
replace altitud = 2410 if ent == 9 & mpio == 12  // Tlalpan
replace altitud = 2260 if ent == 9 & mpio == 13  // Xochimilco
replace altitud = 2230 if ent == 9 & mpio == 15  // Cuauhtémoc
replace altitud = 2265 if ent == 9 & mpio == 17  // Venustiano Carranza
/*Durango*/
replace altitud = 1960 if ent == 10 & mpio == 1  // Canatlán
replace altitud = 1960 if ent == 10 & mpio == 3  // Coneto de Comonfort
replace altitud = 1580 if ent == 10 & mpio == 4  // Cuencamé
replace altitud = 1885 if ent == 10 & mpio == 5  // Durango
replace altitud = 1880 if ent == 10 & mpio == 6  // Gral. Simón Boívar
replace altitud = 1150 if ent == 10 & mpio == 7  // Gómez Palacio
replace altitud = 2000 if ent == 10 & mpio == 8  // Guadalupe Victoria
replace altitud = 1700 if ent == 10 & mpio == 10  // Hidalgo
replace altitud = 1140 if ent == 10 & mpio == 12  // Lerdo
replace altitud = 1250 if ent == 10 & mpio == 15  // Nazas
replace altitud = 1740 if ent == 10 & mpio == 16  // Nombre de Dios
replace altitud = 1740 if ent == 10 & mpio == 17  // Ocampo
replace altitud = 1900 if ent == 10 & mpio == 22  // Poanas
replace altitud = 1660 if ent == 10 & mpio == 30  // San Pedro del Gallo
replace altitud = 1720 if ent == 10 & mpio == 32  // Santiago Papasquiaro
replace altitud = 1990 if ent == 10 & mpio == 39  // Nuevo Ideal
/*Guanajuato*/
replace altitud = 1770 if ent == 11 & mpio == 5  // Apaseo el Grande
replace altitud = 1750 if ent == 11 & mpio == 7  // Celaya
replace altitud = 1920 if ent == 11 & mpio == 14  // Dolores Hidalgo
replace altitud = 1730 if ent == 11 & mpio == 17  // Irapuato
replace altitud = 1800 if ent == 11 & mpio == 20  // León
replace altitud = 1770 if ent == 11 & mpio == 23  // Pénjamo
replace altitud = 1720 if ent == 11 & mpio == 27  // Salamanca
replace altitud = 2070 if ent == 11 & mpio == 29  // San Diego de la Unión
replace altitud = 2100 if ent == 11 & mpio == 33  // San Luis de la Paz
replace altitud = 1750 if ent == 11 & mpio == 35  // Santa Cruz de Juventino Rosas
replace altitud = 1770 if ent == 11 & mpio == 39  // Tarimoro
replace altitud = 1730 if ent == 11 & mpio == 44  // Villagrán
/*Guerrero*/
replace altitud = 1250 if ent == 12 & mpio == 29  // Chilpancingo de los Bravo
/*Hidalgo*/
replace altitud = 1700 if ent == 13 & mpio == 30  // Ixmiquilpan
replace altitud = 2000 if ent == 13 & mpio == 48 // Pachuca de Soto
/*Jalisco*/
replace altitud = 1680 if ent == 14 & mpio == 1  // Acatic
replace altitud = 1260 if ent == 14 & mpio == 5  // Amatitán
replace altitud = 1380 if ent == 14 & mpio == 9  // EL Arenal
replace altitud = 1300 if ent == 14 & mpio == 12  // Atenguillo
replace altitud = 1350 if ent == 14 & mpio == 14  // Atoyac
replace altitud = 1610 if ent == 14 & mpio == 16  // Ayotlán
replace altitud = 1530 if ent == 14 & mpio == 30  // Chapala
replace altitud = 1780 if ent == 14 & mpio == 33  // Degollado
replace altitud = 1440 if ent == 14 & mpio == 38  // Guachinango
replace altitud = 1550 if ent == 14 & mpio == 39  // Guadalajara
replace altitud = 2100 if ent == 14 & mpio == 48  // Jesús María
replace altitud = 10 if ent == 14 & mpio == 67  // Puerto Vallarta
replace altitud = 1330 if ent == 14 & mpio == 83  // Tala
replace altitud = 1180 if ent == 14 & mpio == 94  // Tequila
replace altitud = 1560 if ent == 14 & mpio == 97  // Tlajomulco de Zúñiga
replace altitud = 1540 if ent == 14 & mpio == 98  // Tlaquepaque
replace altitud = 1660 if ent == 14 & mpio == 101  // Tonalá
replace altitud = 1140 if ent == 14 & mpio == 108  // Tuxpan
replace altitud = 1355 if ent == 14 & mpio == 114  // Villa Corona
replace altitud = 1560 if ent == 14 & mpio == 120 // Zapopan
/*Estado de Mexico*/
replace altitud = 2600 if ent == 15 & mpio == 5  // Almoloya de Juárez
replace altitud = 2480 if ent == 15 & mpio == 9  // Amecameca
replace altitud = 2240 if ent == 15 & mpio == 11  // Atenco
replace altitud = 2600 if ent == 15 & mpio == 12  // Atizapán
replace altitud = 2690 if ent == 15 & mpio == 18  // Calimaya
replace altitud = 2250 if ent == 15 & mpio == 24  // Cuautitlán
replace altitud = 2240 if ent == 15 & mpio == 25  // Chalco
replace altitud = 2255 if ent == 15 & mpio == 28  // Chiautla
replace altitud = 2250 if ent == 15 & mpio == 29  // Chicoloapan
replace altitud = 2400 if ent == 15 & mpio == 31  // Chimalhuacán
replace altitud = 2250 if ent == 15 & mpio == 33  // Ecatepec de Morelos
replace altitud = 2680 if ent == 15 & mpio == 37 // Huixquilucan
replace altitud = 2260 if ent == 15 & mpio == 39  // Ixtapaluca
replace altitud = 2452 if ent == 15 & mpio == 45  // Jilotepec
replace altitud = 2795 if ent == 15 & mpio == 46  // Jilotzingo
replace altitud = 2570 if ent == 15 & mpio == 51  // Lerma
replace altitud = 2400 if ent == 15 & mpio == 57 // Naucalpan de Juárez
replace altitud = 2440 if ent == 15 & mpio == 58  // Nezahualcóyotl
replace altitud = 2400 if ent == 15 & mpio == 60  // Nicolás Romero
replace altitud = 2340 if ent == 15 & mpio == 68 // Ozumba
replace altitud = 2330 if ent == 15 & mpio == 70  // La Paz
replace altitud = 1380 if ent == 15 & mpio == 78 // Santo Tomás
replace altitud = 2380 if ent == 15 & mpio == 85  // Temascalcingo
replace altitud = 2580 if ent == 15 & mpio == 98  // Texcalyacac
replace altitud = 2620 if ent == 15 & mpio == 101  // Tianguistenco
replace altitud = 2250 if ent == 15 & mpio == 104 // Tlalnepantla de Baz
replace altitud = 1840 if ent == 15 & mpio == 105 // Tlatlaya
replace altitud = 2680 if ent == 15 & mpio == 106 // Toluca
replace altitud = 2250 if ent == 15 & mpio == 108 // Tultepec
replace altitud = 2250 if ent == 15 & mpio == 109 // Tultitlán
replace altitud = 1820 if ent == 15 & mpio == 110 // Valle de Bravo
replace altitud = 2570  if ent == 15 & mpio == 114 // Villa Victoria
replace altitud = 2575 if ent == 15 & mpio == 115  // Xonacatlán
replace altitud = 2740 if ent == 15 & mpio == 118 // Zinacantepec
replace altitud = 2260 if ent == 15 & mpio == 121 // Cuautitlán Izcalli
replace altitud = 1130 if ent == 15 & mpio == 123 // Luvianos
replace altitud = 2760 if ent == 15 & mpio == 424 // San José del Rincón (I think this is a coding error and should be 124 which is San José del Rincón)
/*Michoacan*/
replace altitud = 920 if ent == 16 & mpio == 2  // Aguililla
replace altitud = 1840  if ent == 16 & mpio == 3  // Alvaro Obregón
replace altitud = 1900 if ent == 16 & mpio == 9  // Ario
replace altitud = 840 if ent == 16 & mpio == 10  // Arteaga
replace altitud = 10 if ent == 16 & mpio == 14  // Coahuayana
replace altitud = 2040 if ent == 16 & mpio == 32  // Erongarícuaro
replace altitud = 640 if ent == 16 & mpio == 33  // Gabriel Zamora
replace altitud = 2060 if ent == 16 & mpio == 34  // Hidalgo
replace altitud = 1920 if ent == 16 & mpio == 40  // Indaparapeo
replace altitud = 1300 if ent == 16 & mpio == 47  // Jungapeo
replace altitud = 2250 if ent == 16 & mpio == 50  // Maravatío
replace altitud = 10 if ent == 16 & mpio == 52 // Lázaro Cárdenas
replace altitud = 1920 if ent == 16 & mpio == 53  // Morelia
replace altitud = 1300 if ent == 16 & mpio == 75  // Los Reyes
replace altitud = 1840 if ent == 16 & mpio == 78  // Santa Ana Maya
replace altitud = 1260 if ent == 16 & mpio == 81  // Susupuato
replace altitud = 1860 if ent == 16 & mpio == 88 // Tarímbaro
replace altitud = 740 if ent == 16 & mpio == 97  // Turicato
replace altitud = 1720 if ent == 16 & mpio == 98  // Tuxpan
replace altitud = 1620 if ent == 16 & mpio == 102 // Uruapan
replace altitud = 1880 if ent == 16 & mpio == 110 // Zinapécuaro
replace altitud = 1380 if ent == 16 & mpio == 111 // Ziracuaretiro
replace altitud = 1940 if ent == 16 & mpio == 112 // Zitácuaro
/*Morelos*/
replace altitud = 900 if ent == 17 & mpio == 1  // Amacuzac
replace altitud = 1640 if ent == 17 & mpio == 2  // Atlatlahucan
replace altitud = 1200 if ent == 17 & mpio == 4  // Ayala
replace altitud = 1300 if ent == 17 & mpio == 6  // Cuautla
replace altitud = 1510 if ent == 17 & mpio == 7  // Cuernavaca
replace altitud = 1250 if ent == 17 & mpio == 8  // Emiliano Zapata
replace altitud = 1350 if ent == 17 & mpio == 11  // Jiutepec
replace altitud = 890 if ent == 17 & mpio == 12  // Jojutla
replace altitud = 1000 if ent == 17 & mpio == 15  // Miacatlán
replace altitud = 1290 if ent == 17 & mpio == 18  // Temixco
replace altitud = 950 if ent == 17 & mpio == 24  // Tlaltizapán
replace altitud = 920 if ent == 17 & mpio == 25 // Tlaquiltenango
replace altitud = 1210 if ent == 17 & mpio == 29 // Yautepec
/*Nayarit*/
replace altitud = 40 if ent == 18 & mpio == 20  // Bahía de Banderas
/*Nuevo León*/
replace altitud = 500 if ent == 19 & mpio == 1  // Abasolo
replace altitud = 460  if ent == 19 & mpio == 4  // Allende
replace altitud = 155 if ent == 19 & mpio == 5 // Anáhuac
replace altitud = 430 if ent == 19 & mpio == 6 // Apodaca
replace altitud = 1100 if ent == 19 & mpio == 7 // Aramberri
replace altitud = 330 if ent == 19 & mpio == 9 // Cadereyta Jiménez
replace altitud = 500 if ent == 19 & mpio == 10 // Carmen
replace altitud = 360 if ent == 19 & mpio == 16 // Dr. González
replace altitud = 700 if ent == 19 & mpio == 18 // García
replace altitud = 360 if ent == 19 & mpio == 25 // Gral. Zuazua
replace altitud = 480 if ent == 19 & mpio == 26 // Guadalupe
replace altitud = 490 if ent == 19 & mpio == 28 // Higueras
replace altitud = 370 if ent == 19 & mpio == 31 // Juárez
replace altitud = 350 if ent == 19 & mpio == 33 // Linares
replace altitud = 400 if ent == 19 & mpio == 34 // Marín
replace altitud = 430 if ent == 19 & mpio == 38 // Montemorelos
replace altitud = 530 if ent == 19 & mpio == 39 // Monterrey
replace altitud = 500 if ent == 19 & mpio == 46 // San Nicolás de los Garza
replace altitud = 680 if ent == 19 & mpio == 48 // Santa Catarina
replace altitud = 420 if ent == 19 & mpio == 51 // Villaldama
/*Oaxaca*/
replace altitud = 2260 if ent == 20 & mpio == 1  // Abejones
replace altitud = 115  if ent == 20 & mpio == 2  // Acatlán de Pérez Figueroa
replace altitud = 1600 if ent == 20 & mpio == 39 // Heroica Ciudad de Huajuapan de León
replace altitud = 30 if ent == 20 & mpio == 43 // Juchitán de Zaragoza
replace altitud = 1550 if ent == 20 & mpio == 59 // Miahuatlán de Porfirio Díaz
replace altitud = 67 if ent == 20 & mpio == 66 // Santiago Niltepec
replace altitud = 1555 if ent == 20 & mpio == 67 // Oaxaca de Juárez
replace altitud = 1500 if ent == 20 & mpio == 68 // Ocotlán de Morelos
replace altitud = 22 if ent == 20 & mpio == 79 // Salina Cruz
replace altitud = 1525 if ent == 20 & mpio == 83 // San Agustín de las Juntas
replace altitud = 1540 if ent == 20 & mpio == 107 // San Antonio de la Cal
replace altitud = 20 if ent == 20 & mpio == 184 // San Juan Bautista Tuxtepec
replace altitud = 1862 if ent == 20 & mpio == 187 // San Juan Coatzóspam
replace altitud = 345 if ent == 20 & mpio == 266 // San Miguel del Puerto
replace altitud = 2120 if ent == 20 & mpio == 296 // San Pablo Macuiltianguis
replace altitud = 80 if ent == 20 & mpio == 307 // San Pedro Huamelula
replace altitud = 220 if ent == 20 & mpio == 318 // San Pedro Mixtepec
replace altitud = 150 if ent == 20 & mpio == 324 // San Pedro Pochutla
replace altitud = 1240 if ent == 20 & mpio == 326 // San Pedro Sochiapam
replace altitud = 1840 if ent == 20 & mpio == 340 // San Pedro y San Pablo Tequixtepec
replace altitud = 1540 if ent == 20 & mpio == 342 // San Raymundo Jalpan
replace altitud = 1530 if ent == 20 & mpio == 385 // Santa Cruz Xoxocotlán
replace altitud = 205 if ent == 20 & mpio == 482 // Santiago Pinotepa Nacional
replace altitud = 55 if ent == 20 & mpio == 515 // Santo Domingo Tehuantepec
replace altitud = 1560 if ent == 20 & mpio == 549 // Tezoatlán de Segura y Luna
replace altitud = 20 if ent == 20 & mpio == 557 // Unión Hidalgo
replace altitud = 1520 if ent == 20 & mpio == 565 // Villa de Zaachila
replace altitud = 1916 if ent == 20 & mpio == 568 // Zapotitlán Palmas
/*Puebla*/
replace altitud = 2560 if ent == 21 & mpio == 1  // Acajete
replace altitud = 1950 if ent == 21 & mpio == 17  // Atempan
replace altitud = 1060 if ent == 21 & mpio == 30 // Coatepec
replace altitud = 1900 if ent == 21 & mpio == 37 // Coyotepec
replace altitud = 980 if ent == 21 & mpio == 47 // Chiautla
replace altitud = 1520 if ent == 21 & mpio == 71 // Huauchinango
replace altitud = 540 if ent == 21 & mpio == 89 // Jopala
replace altitud = 2135 if ent == 21 & mpio == 114 // Puebla
replace altitud = 2130 if ent == 21 & mpio == 119 // San Andrés Cholula
replace altitud = 1620 if ent == 21 & mpio == 156 // Tehuacán
replace altitud = 1560 if ent == 21 & mpio == 162 // Tepango de Rodríguez
replace altitud = 1860 if ent == 21 & mpio == 173 // Teteles de Avila Castillo
replace altitud = 1900 if ent == 21 & mpio == 186 // Tlatlauquitepec
replace altitud = 1960 if ent == 21 & mpio == 212 // Zautla
/*Queretaro*/
replace altitud = 2620 if ent == 22 & mpio == 1  // Amealco de Bonfil
replace altitud = 2400 if ent == 22 & mpio == 2  // Pinal de Amoles
replace altitud = 1920 if ent == 22 & mpio == 16 // San Juan del Río
/*Queretaro*/
//replace altitud = 2620 if ent == 23 & mpio == 25  // does not exist
/*Sinaloa*/
replace altitud = 10 if ent == 25 & mpio == 1  // Ahome
replace altitud = 30 if ent == 25 & mpio == 2  // Angostura
replace altitud = 60 if ent == 25 & mpio == 6 // Culiacán
replace altitud = 80 if ent == 25 & mpio == 10 // El Fuerte
replace altitud = 50 if ent == 25 & mpio == 11 // Guasave
replace altitud = 10 if ent == 25 & mpio == 12 // Mazatlán
replace altitud = 20 if ent == 25 & mpio == 14 // Rosario
replace altitud = 80 if ent == 25 & mpio == 17 // Sinaloa
replace altitud = 10 if ent == 25 & mpio == 18 // Navolato
//replace altitud = 1620 if ent == 25 & mpio == 20 // does not exist
/*Sonora*/
replace altitud = 600 if ent == 26 & mpio == 1  // Aconchi
replace altitud = 20 if ent == 26 & mpio == 12  // Bácum
replace altitud = 280 if ent == 26 & mpio == 17 // Caborca
replace altitud = 40 if ent == 26 & mpio == 18 // Cajeme
replace altitud = 380 if ent == 26 & mpio == 21 // La Colorada
replace altitud = 10 if ent == 26 & mpio == 29 // Guaymas
replace altitud = 210 if ent == 26 & mpio == 30 // Hermosillo
replace altitud = 10 if ent == 26 & mpio == 33 // Huatabampo
replace altitud = 760 if ent == 26 & mpio == 36 // Magdalena
replace altitud = 50 if ent == 26 & mpio == 42 // Navojoa
replace altitud = 1200 if ent == 26 & mpio == 43 // Heroica Nogales
replace altitud = 470 if ent == 26 & mpio == 46 // Oquitoa
replace altitud = 10 if ent == 26 & mpio == 48 // Puerto Peñasco
/*Tamaulipas*/
replace altitud = 10 if ent == 28 & mpio == 3  // Altamira
replace altitud = 110 if ent == 28 & mpio == 18  // Jiménez
replace altitud = 10 if ent == 28 & mpio == 22 // Matamoros
replace altitud = 188 if ent == 28 & mpio == 27 // Nuevo Laredo
replace altitud = 40 if ent == 28 & mpio == 32 // Reynosa
replace altitud = 10 if ent == 28 & mpio == 38 // Tampico
/*Tlaxcala*/
replace altitud = 2300 if ent == 29 & mpio == 22  // Acuamanala de Miguel Hidalgo
/*Veracruz*/
replace altitud = 2020 if ent == 30 & mpio == 1  // Acajete
replace altitud = 100 if ent == 30 & mpio == 3  // Acayucan
replace altitud = 1100 if ent == 30 & mpio == 9 // Alto Lucero de Gutiérrez Barrios
replace altitud = 1890 if ent == 30 & mpio == 10 // Altotonga
replace altitud = 2300 if ent == 30 & mpio == 19 // Astacinga
replace altitud = 1760 if ent == 30 & mpio == 20 // Atlahuilco
replace altitud = 1340 if ent == 30 & mpio == 30 // Camerino Z. Mendoza
replace altitud = 340 if ent == 30 & mpio == 32 // Catemaco
replace altitud = 1200 if ent == 30 & mpio == 38 // Coatepec
replace altitud = 50 if ent == 30 & mpio == 39 // Coatzacoalcos
replace altitud = 850 if ent == 30 & mpio == 44 // Córdoba
replace altitud = 40 if ent == 30 & mpio == 48 // Cosoleacaque
replace altitud = 1000 if ent == 30 & mpio == 68 // Fortín
replace altitud = 60 if ent == 30 & mpio == 77 // Isla
replace altitud = 1460 if ent == 30 & mpio == 87 // Xalapa
replace altitud = 130 if ent == 30 & mpio == 94 // Juan Rodríguez Clara
replace altitud = 20 if ent == 30 & mpio == 108 // Minatitlán
replace altitud = 1280 if ent == 30 & mpio == 115 // Nogales
replace altitud = 1230 if ent == 30 & mpio == 118 // Orizaba
replace altitud = 50 if ent == 30 & mpio == 120 // Oteapan
replace altitud = 180 if ent == 30 & mpio == 124 // Papantla
replace altitud = 50 if ent == 30 & mpio == 131 // Poza Rica de Hidalgo
replace altitud = 10 if ent == 30 & mpio == 133 // Pueblo Viejo
replace altitud = 20 if ent == 30 & mpio == 142 // San Juan Evangelista
replace altitud = 2100 if ent == 30 & mpio == 147 // Soledad Atzompa
replace altitud = 260 if ent == 30 & mpio == 167 // Tepetzintla
replace altitud = 1600 if ent == 30 & mpio == 168 // Tequila
replace altitud = 1320 if ent == 30 & mpio == 188 // Totutla
replace altitud = 10 if ent == 30 & mpio == 189 // Túxpam
replace altitud = 10 if ent == 30 & mpio == 193 // Veracruz
/*Yucatán*/
replace altitud = 17 if ent == 31 & mpio == 1  // Abalá
replace altitud = 31 if ent == 31 & mpio == 3  // Akil
replace altitud = 7 if ent == 31 & mpio == 31 // Dzoncauich
replace altitud = 9 if ent == 31 & mpio == 50 // Mérida
replace altitud = 19 if ent == 31 & mpio == 53 // Muna
replace altitud = 2 if ent == 31 & mpio == 59 // Progreso
replace altitud = 37 if ent == 31 & mpio == 79 // Tekax
replace altitud = 7 if ent == 31 & mpio == 101 // Umán
/*Zacatecas*/
replace altitud = 1990 if ent == 32 & mpio == 22  // Juan Aldama



#delimit ;

label define baja 
