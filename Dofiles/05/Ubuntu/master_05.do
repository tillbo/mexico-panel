set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/master_05.log", replace
*merging 05 datasets

use "Daten 05/iiib_ec_05chro.dta", clear

merge 1:1 folio ls using "Daten 05/iiib_portad", nogen keep(master match)
merge m:1 folio using "Daten 05/hhexp05",
merge m:1 folio using "Daten 05/ii_se_05shock", gen(_merge1) keep(master match)
merge 1:1 folio ls using "Daten 05/iiia_tb_05inco", gen(_merge2) keep(master match)
merge 1:1 folio ls using "Daten 05/c_ls_05contr", gen(_merge3) keep(master match)
merge 1:1 folio ls using "Daten 05/s_sa_05heal", gen(_merge4) keep(master match)
merge m:1 folio using "Daten 05/c_portad_05hhsize", gen(_merge5) keep(master match)
merge 1:1 folio ls using "Daten 05/iiib_es_05hlth", gen(_merge6) keep(master match)
merge m:1 folio using "Daten 05/ii_in_05nolabinc", gen(_merge7) keep(master match)
merge 1:1 folio ls using "Daten 05/iiib_tp_05parents", gen(_merge8) keep(master match)
merge 1:1 folio ls using "Daten 05/iiib_sm_05mental", gen(_merge9) keep(master match)
merge 1:1 folio ls using "Daten 05/iiia_ed_05educ", gen(_merge10) keep(master match)
merge 1:1 folio ls using "Daten 05/iiib_ca_05insur", keep(master match) nogen
merge 1:1 folio ls  using "Daten 05/iiib_05conpor", nogen keep(master match)

merge m:1 folio using "Daten 05/ii_ah_hhassets05", gen(_merge11) keep(master match)
merge m:1 folio using "Daten 05/c_cvo", gen(_merge12) keep(master match)
merge m:1 folio using "Daten 05/c_cv", gen(_merge13) keep(master match)
**destringing folio ls ls00

	drop pid_link //old pid_link
	//gen str8 var1 = string(folio, "%08.0f")
	//gen str2 var2 = string(ls, "%02.0f")
	gen pid_link = folio + ls

	destring pid_link ls ls00 ls06 ls07 folio, replace
	drop if pid_link==. //dropping observations with no observations



save "Daten 05/master_merged.dta", replace

log close
exit


