
set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder


**Master do-file**

    	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/criiib_ec_05.do"					//dropping unneeded variabels from chronic disease dataset, summing health expenditures and creating variabel for diab related diseases
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/criiah_05.do"					//household assets
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crc_ls_05.do"					//"Achtung: delimit cr funktioniert nicht, daher do-file manuell ausführen"   labeling control book, value lables etc.
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crhhexp05_i_cs.do"				//expenditure dataset(food, general daily use items), creating new variables capturing total expenditures of hh
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crhhexp05_i_cs1.do"				//expenditure dataset (electronics, school), creating new variables capturing total expenditures of hh
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crexpend_hh.do"					//merging expenditure datasets and creating variable with total expenditure of hh for 1 month
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/criiib_ca_05.do"  				//insurance dataset, only labeling and recoding
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crii_se_05.do"					//economic shocks of hhs, u.a. new variable captur. if sick and unemployed in last 5 years
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crs_sa_05.do"					//"Achtung: delimit cr funktioniert nicht, daher do-file manuell ausführen"health dataset: creating BMI, labeling and recoding variables
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/criiia_tb_05.do"					//labour dataset, creation of income and workload variables
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/criiib_es_05.do"					//health status
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crii_inr_05.do"					//rural income household
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crii_in_05.do"					//no-labor household income
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/crc_portad_05.do"				//control book, portad dataset containing information on geography and size of communities hhs living in
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/iiib_sm_05.do"					//mental health
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/criiia_ed_05.do"					//education and indigenous
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/criiib_compor_05.do"					// adding date of interview

	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/05/Ubuntu/master_05.do"					//merging 2005 data




        /* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/criiib_ec_05.do"					//dropping unneeded variabels from chronic disease dataset, summing health expenditures and creating variabel for diab related diseases */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crc_ls_05.do"					//"Achtung: delimit cr funktioniert nicht, daher do-file manuell ausführen"   labeling control book, value lables etc. */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crhhexp05_i_cs.do"				//expenditure dataset(food, general daily use items), creating new variables capturing total expenditures of hh */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crhhexp05_i_cs1.do"				//expenditure dataset (electronics, school), creating new variables capturing total expenditures of hh */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crexpend_hh.do"					//merging expenditure datasets and creating variable with total expenditure of hh for 1 month */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/criiib_ca_05.do"  				//insurance dataset, only labeling and recoding  */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crii_se_05.do"					//economic shocks of hhs, u.a. new variable captur. if sick and unemployed in last 5 years */
	/* *do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crs_sa_05.do"					//"Achtung: delimit cr funktioniert nicht, daher do-file manuell ausführen"health dataset: creating BMI, labeling and recoding variables																							 */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/criiia_tb_05.do"					//labour dataset, creation of income and workload variables */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/criiib_es_05.do"					//health status */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crii_inr_05.do"					//rural income household */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crii_in_05.do"					//no-labor household income */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/crc_portad_05.do"				//control book, portad dataset containing information on geography and size of communities hhs living in */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/iiib_sm_05.do"					//mental health */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/criiia_ed_05.do"					//education and indigenous */

	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/05/Ubuntu/master_05.do"					//merging 2005 data										 */

exit
