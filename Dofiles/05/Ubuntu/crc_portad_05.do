capture log close
set more off

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/crc_portad_05.log", replace

**control book, portad dataset containing information on geography and size of communities hhs living in

use "Daten 05/c_portad_05", clear

destring id_loc, replace


save "Daten 05/c_portad_05hhsize", replace
log close
exit


