capture log close
set more off

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiia_tb_05.log", replace

use "Daten 05/iiia_tb_05", clear



save "Daten 05/iiia_tb_05inco", replace

log close
exit
