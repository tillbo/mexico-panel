capture log close
set more off
global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/crexpend_hh.log", replace

use "Daten 05/hhexp05_i_cs", clear

*merging hh-expenditures of hhexp05_i_cs and hhexp05_i_cs1 and creating expenditure variables for 1 month

	*keeping only newly created variables capturing expenditures
		keep  ex_fruit ex_cg ex_meet ex_procfo ex_trans ex_mainfood ex_allfood ex_nonfood  folio
	*merging with hhexp05_i_cs1
		merge 1:1 folio using "Daten 05/hhexp05_i_cs1.dta", keepusing(ex_goods ex_goods1 ex_schoolm ex_schoolf ex_schoolnhm ex_school)

	*brake down variables so that they are represent expenditures for one month (30 days)
		*interpolate 7 days expenditures to 30 days
			foreach var of varlist  ex_fruit ex_cg ex_meet ex_procfo ex_trans ex_mainfood ex_allfood {
			gen `var'_mth=(`var'/7)*30
			}
		*divide school period expenditures to get monthly expenditures
			foreach var of varlist ex_schoolm ex_schoolf ex_schoolnhm ex_school {
			gen `var'_mth=`var'/6
			}
		*divide 3 month expenditure
			gen ex_goods_mth = ex_goods/3
		*divide yearly expenditure
			gen ex_goods1_mth = ex_goods1/12

*create variable that captures all monthly expenditures

	egen ex_month = rowtotal(ex_allfood_mth ex_school_mth ex_goods_mth ex_goods1_mth ex_nonfood), miss

*labeling variables

		label var ex_fruit_mth "1 month exp. VEGETABLES AND FRUITS"
		label var ex_cg_mth "1 month exp. CEREALS AND GRAINS"
		label var ex_meet_mth "1 month exp. MEETS AND ANIMAL ORIGINATED FOOD"
		label var ex_procfo_mth "1 month exp. OTHER INDUSTRIALLY-PROCESED FOOD"
		label var ex_trans_mth "1 month exp. PUBLIC TRANSPORTATION AND OTHER"
		label var ex_mainfood_mth "1 month exp. Main sorts of foods Corn Tortillas, etc."
		label var ex_allfood_mth "1 month exp. Sum all food"
		label var ex_goods_mth "1 months exp. clothes,health,decoration,mainenance"
		label var ex_goods1_mth "1 month exp. electr.,cars,furniture, transport"
		label var ex_schoolm_mth "1 month exp. males school"
		label var ex_schoolf_mth "1 month exp. females school"
		label var ex_schoolnhm_mth "1 month exp. non hh-members"
		label var ex_school_mth "1 month total school exp."
		label var ex_month "1 month total expenditure hh"
		rename  ex_nonfood  ex_nonfood_mth

*keeping only new variables
	drop ex_fruit ex_cg ex_meet ex_procfo ex_trans ex_mainfood ex_allfood ex_goods ex_goods1 ex_schoolm ex_schoolf ex_schoolnhm ex_school _merge

save "Daten 05/hhexp05", replace

log close
exit
