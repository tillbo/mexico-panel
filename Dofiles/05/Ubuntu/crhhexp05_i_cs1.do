capture log close
set more off

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/crhhexp05_i_cs1.log", replace

use"Daten 05/i_cs1_05", clear

**expenditures hhs

	*household expenditures last 3 months (clothes, stuff for house(decoration etc.), health, vehicle maintenance)
		egen ex_goods = rowtotal(cs22a_2 cs22b_2 cs22c_2 cs22d_2 cs22e_2 cs22f_2 cs22g_2 cs22h_2), miss
	*household expenditures last year (electronics, vehicles, furniture, transportation)
		egen ex_goods1 = rowtotal( cs27a_2 cs27b_2 cs27c_2 cs27d_2 cs27e_2 cs27f_2), miss
	*household expenditures for school current school period (fees, supplies and uniforms, transportation)
		egen ex_schoolm = rowtotal( cs34a_12 cs35a_12 cs36a_12), miss   			//exp. for male hh-members
		egen ex_schoolf = rowtotal( cs34a_22 cs35a_22 cs36a_22), miss				//exp. for femal hh-members
		egen ex_schoolnhm = rowtotal( cs34a_32 cs35a_32 cs36a_32), miss			//exp. for non hh-members
		egen ex_school = rowtotal( ex_schoolm ex_schoolf ex_schoolnhm), miss		// total hh-expenditures for school

	*labels above variables
		label var ex_goods "3 months exp. clothes,health,decoration,mainenance"
		label var ex_goods1 "year exp. electr.,cars,furniture, transport"
		label var ex_schoolm "exp. males school for curr. period"
		label var ex_schoolf "exp. females school for curr. period"
		label var ex_schoolnhm "exp. non hh-members school for curr. period"
		label var ex_school "total school exp. for curr. period"


save "Daten 05/hhexp05_i_cs1", replace
log close
exit
