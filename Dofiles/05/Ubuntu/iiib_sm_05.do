capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/mentalhealth_05.log", replace


use "Daten 05/iiib_sm_05.dta", clear

**changing variable according to coding of Calderon(2007) presented in FAQ of MxFLS
label define Mental 1 "No" 3 "a lot of times " 2 "sometimes" 4 "all the time"

foreach var of varlist  sm01 - sm21 {
replace `var'=0 if `var'==4
replace `var'=4 if `var'==3
replace `var'=3 if `var'==2
replace `var'=2 if `var'==1
replace `var'=1 if `var'==0
replace `var'=. if `var'==5
label values `var' Mental
}

*questions 1-20 are summed to make a scale of depression, 21 is excluded according to FAQ of MXFLS
egen mentalhealth=rowtotal( sm01- sm20), miss

label var mentalhealth "sum of all mental conditions"

save "Daten 05/iiib_sm_05mental.dta", replace
log close
exit
