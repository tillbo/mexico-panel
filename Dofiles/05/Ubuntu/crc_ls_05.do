set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
//global homefolder ""/home/till/Dropbox/PhD/Diabetes Mexico/article/Data""
cd $homefolder
**log-file starten**

log using "Logs/crc_ls_05.log", replace


***control book, answered by head of houshold. most essential socioeconomic information
use "Daten 05/c_ls_05", clear

*recoding variables from 1=Yes, 3=No to 1=Yes, 0=No

	foreach var of varlist  ls12 ls16 {
	replace `var'=0 if `var'==3
	}

*recode don't know to missing

	replace ls02_1=. if ls02_1==8
	replace ls13_1=. if ls13_1==8
	replace ls03_1=. if ls03_1==8
	replace	ls19a_1=. if ls19a_1==8
	replace ls14=. if ls14==98
	replace ls08=. if ls08==99

*value labels
	label define yesno 0 "no" 1 "yes"
	foreach var of varlist ls01b ls01c ls12 ls16 {
	label values `var' yesno
	}

	#delimit ;

		label define hhhead
			1 "HH head"
			2 "Spouse/couple"
			3 "Son/Daughter"
			4 "Step son/daughter"
			5 "Son/daughter in law"
			6 "Father/Mother"
			7 "Father/mother in law"
			8 "Brother/sister"
			9 "Brother/sister in law"
			10 "Grandson/grandaughter"
			11. "Grandfather/grandmother"
			12. "Uncle/aunt"
			13. "Nephew/niece"
			14. "Cousin"
			15. "Worker"
			16. "Ex-spouse/ ex-couple"
			17. "Without relationship"
			18. "Other (specify)" ;


		label define marr
			1 "Concubinage"
			2 "Separated"
			3 "Divorced"
			4 "Widowed"
			5 "Married"
			6 "Single";

		label define educ
			1 "No education"
			2 "Preschool or Kinder"
			3 "Elementary"
			4 "Secondary"
			5 "Open secondary"
			6 "High school"
			7 "Open high school"
			8 "Normal Basic"
			9 "College"
			10 "Graduate";

		label define sex
			1 "male"
			3 "female";

		label define max_grade
			1."First grade"
			2 "Second grade"
			3 "Third grade"
			4 "Fourth grade"
			5 "Fifth grade"
			6 "Sixth grade"
			7 "Seventh grade"
			8 "Other(specify)";

		label define live_hh
			0 "Death"
			1 "Still living at this household"
			3 "Use to live in 2002 but not 2005"
			4 "New member";

		label define alive
			0 "Death"
			3 "Use to live in 2002 but not 2005"
			4 "New member";

		label define when
			1 "Year/Month";

	#delimit cr
		label values ls01a live_hh
		label values ls05_1 hhhead
		label values ls10 marr
		label values ls14 educ
		label values ls04 sex
		label values ls15_1 max_grade
		label values ls19 live_hh
		label values ls19c alive
		label values ls19a_1 when


*creating variable that contains number of hh-members for each hh-id (folio)

	bysort folio: gen mem_hh = _N
	label var mem_hh "total nr. of persons in hh"


	bysort folio: gen mem_hh_older = _N if ls02_2 > 14
	label var mem_hh "total nr. of persons in hh > 14"
*creating variable that contains number of hh-members for each hh-id (folio)

	gen kids = 0 if ls02_2 < .
	replace kids=1 if ls02_2 < 15
	bysort folio: egen kids_hh=total(kids)
	label var kids_hh "nr of children < 15 years in hh"

	*generating variables that capture age groups
	gen age1_20=0
	replace age1_20=1 if  ls02_2>=0 &  ls02_2<=20			//1-20 years
	gen age21_40=0
	replace age21_40=1 if  ls02_2>=21 &  ls02_2<=40			//21-40
	gen age41_60=0
	replace age41_60=1 if  ls02_2>=41 &  ls02_2<=60			//41-60
	gen age61_80=0
	replace age61_80=1 if  ls02_2>=61 &  ls02_2<=80			//61-80
	gen age81=0
	replace age81=1 if  ls02_2>=81 &  ls02_2<.				//>=81

	label var age1_20 "1 if 1-20 years"
	label var age21_40 "1 if 21-40 years"
	label var age41_60 "1 if 41-60 years"
	label var age61_80 "1 if 61-80 years"
	label var age81 "1 if >=81 years"



save "Daten 05/c_ls_05contr.dta", replace

log close
exit








