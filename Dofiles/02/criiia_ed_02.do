set more off
capture log close
global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiia_ed_02.log", replace


***insurance information

use "Daten 02/iiia_ed_02", clear

**indigenous variable
	rename ed03 indigenous
	replace indigenous=0 if indigenous==3
	label variable indigenous "PART GROUP/INDIGENOUS GROUP"
	label define Yesno 0 "No" 1 "Yes"
	label values indigenous Yesno

**education variable
	rename ed05 school
	replace school=0 if school==3
	rename ed06 educ
	label var educ "Last level school attended"
	rename ed07_1 maxgrade
	label var maxgrade "Last completed grade"

	# delimit ;
	label define educ
			1 "No education"
			2 "Preschool or Kinder"
			3 "Elementary"
			4 "Secondary"
			5 "Open secondary"
			6 "High school"
			7 "Open high school"
			8 "Normal Basic"
			9 "College"
			10 "Graduate";

	label define max_grade
			1."First grade"
			2 "Second grade"
			3 "Third grade"
			4 "Fourth grade"
			5 "Fifth grade"
			6 "Sixth grade"
			7 "Seventh grade"
			8 "Other(specify)";
	# delimit cr

	label values educ educ
	replace educ=0 if educ==98
	replace maxgrade=0 if maxgrade==98

	keep indigenous school educ folio ls maxgrade

	save "Daten 02/iiia_ed_02educ", replace
	log close
	exit


