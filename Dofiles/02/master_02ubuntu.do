set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Daten 02""

cd $homefolder
**log-file starten**

log using "/Logs/master_02ubuntu", replace

*merging 02 datasets

use "iiib_ec_02chro.dta", clear

	merge m:1 folio using hhexp02,
	merge m:1 folio using ii_se_02shock, gen(_merge1)
	merge 1:1 folio ls using iiia_tb_02inco, gen(_merge2)
	merge 1:1 folio ls using c_ls_02contr, gen(_merge3)
	merge 1:1 folio ls00 using s_sa_02heal, gen(_merge4)
	merge m:1 folio using c_portad_02hhsize, gen(_merge5)
	merge 1:1 folio ls using iiib_es_02hlth, gen(_merge6)
	merge m:1 folio using ii_in_02nolabinc, gen(_merge7)
	merge 1:1 folio ls using iiib_tp_02parents, gen(_merge8)
	merge 1:1 folio ls using iiib_sm_02mental, gen(_merge9)

**generate pid_link

	drop pid_link var1 var2 //old pid_link
	gen str8 var1 = string(folio, "%08.0f")
	gen str2 var2 = string(ls, "%02.0f")
	gen pid_link = var1 + var2

	destring pid_link



save "master_merged.dta", replace

log close
exit


