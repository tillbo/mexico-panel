set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/master_02.log", replace

*merging 02 datasets

use "Daten 02/iiib_ec_02chro.dta", clear

merge m:1 folio using "Daten 02/hhexp02",
merge m:1 folio using "Daten 02/ii_se_02shock", gen(_merge1) keep(master match)
merge 1:1 folio ls using "Daten 02/iiia_tb_02inco", gen(_merge2) keep(master match)
merge 1:1 folio ls using "Daten 02/c_ls_02contr", gen(_merge3) keep(master match)
merge 1:1 folio ls using "Daten 02/s_sa_02heal", gen(_merge4) keep(master match)
merge m:1 folio using "Daten 02/c_portad_02hhsize", gen(_merge5) keep(master match)
merge 1:1 folio ls using "Daten 02/iiib_portad", nogen keep(master match)

merge 1:1 folio ls using "Daten 02/iiib_es_02hlth", gen(_merge6) keep(master match)
merge m:1 folio using "Daten 02/ii_in_02nolabinc", gen(_merge7) keep(master match)
merge 1:1 folio ls using "Daten 02/iiib_tp_02parents", gen(_merge8) keep(master match)
merge m:1 folio using "Daten 02/ii_ah_hhassets02", gen(_merge11) keep(master match)
merge 1:1 folio ls using "Daten 02/iiib_sm_02mental", gen(_merge9) keep(master match)
merge 1:1 folio ls using "Daten 02/iiia_ed_02educ", gen(_merge10) keep(master match)
merge 1:1 folio ls using "Daten 02/iiib_ca_02insur", keep(master match) nogen
merge m:1 folio using "Daten 02/c_cvo", gen(_merge12) keep(master match)
merge m:1 folio using "Daten 02/c_cv", gen(_merge13) keep(master match)
**generate pid_link

	drop pid_link var1 var2 //old pid_link
	gen str8 var1 = string(folio, "%08.0f")
	gen str2 var2 = string(ls, "%02.0f")
	gen pid_link = var1 + var2

	destring pid_link, replace
	drop if pid_link==. //dropping observations with no observations






save "Daten 02/master_merged.dta", replace

log close
exit


