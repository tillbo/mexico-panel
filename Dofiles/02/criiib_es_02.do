set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiib_es_02.log", replace
*health dataset--> health status

use"Daten 02/iiib_es_02", clear


keep  es01-es16  es23-ls


**create pid_link

*recoding variables from 1=Yes, 3=No to 1=Yes, 0=No

	foreach var of varlist   es02 es03_1 es04_1 es06 es08_1 es09 es09 es23 {
	replace `var'=0 if `var'==3
	}

*value labels
	label define yesno 0 "no" 1 "yes"
	foreach var of varlist  es02 es03_1 es04_1 es06 es08_1 es09 es09 es23 {
	label values `var' yesno
	}
*generate pid_link

	gen str8 var1 = string(folio, "%08.0f")
	gen str2 var2 = string(ls, "%02.0f")
	gen pid_link = var1 + var2

	drop var1 var2

save "Daten 02/iiib_es_02hlth.dta", replace

log close

exit
