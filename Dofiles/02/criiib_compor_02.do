set more off
capture log close

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiib_conpor_02.log", replace
*health dataset--> chronic diseases

use "Daten 02/iiib_conpor", clear
destring folio ls, replace
duplicates drop folio ls, force

save "Daten 02/iiib_02conpor.dta", replace

log close

exit
