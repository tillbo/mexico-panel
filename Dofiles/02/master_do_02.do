
set more off
capture log close





**Master do-file**

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"

	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/criiib_ec_02.do"					//dropping unneeded variabels from chronic disease dataset, summing health expenditures and creating variabel for diab related diseases
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crc_ls_02.do"	
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/criiah_02.do"					//household assets
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crhhexp02_i_cs.do"				//expenditure dataset(food, general daily use items), creating new variables capturing total expenditures of hh
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crhhexp02_i_cs1.do"				//expenditure dataset (electronics, school), creating new variables capturing total expenditures of hh
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crexpend_hh.do"					//merging expenditure datasets and creating variable with total expenditure of hh for 1 month
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/criiib_ca_02.do"  				//insurance dataset, only labeling and recoding
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crii_se_02.do"					//economic shocks of hhs, u.a. new variable captur. if sick and unemployed in last 5 years
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crs_sa_02.do"					//"Achtung: delimit cr funktioniert evtl. nicht, daher do-file manuell ausführen" health dataset: creating BMI, labeling and recoding variables
	*do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/criiib_compor_02.do"					// adding date of interview. file not needed for 02 as whole survey conducted in 02

        do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/criiia_tb_02.do"					//labour dataset, creation of income and workload variables
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/criiib_es_02.do"					//health status
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crii_inr_02.do"					//rural income household
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crii_in_02.do"					//no-labor household income
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/crc_portad_02.do"				//control book, portad dataset containing information on geography and size of communities hhs living in
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/iib_sm_02.do"					//mental health
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/criiia_ed_02.do"					//education and indigenous
	do "/gpfs/med/gsd12ytu/thesis/ENNViH/Data/Do Files/02/master_02.do"					//merging 2002 data

	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/criiib_ec_02.do"					//dropping unneeded variabels from chronic disease dataset, summing health expenditures and creating variabel for diab related diseases */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crc_ls_02.do"					//"Achtung: delimit cr funktioniert evtl. nicht, daher do-file manuell ausführen"   labeling control book, value lables etc. */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crhhexp02_i_cs.do"				//expenditure dataset(food, general daily use items), creating new variables capturing total expenditures of hh */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crhhexp02_i_cs1.do"				//expenditure dataset (electronics, school), creating new variables capturing total expenditures of hh */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crexpend_hh.do"					//merging expenditure datasets and creating variable with total expenditure of hh for 1 month */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/criiib_ca_02.do"  				//insurance dataset, only labeling and recoding  */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crii_se_02.do"					//economic shocks of hhs, u.a. new variable captur. if sick and unemployed in last 5 years */
	/* *do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crs_sa_02.do"					//"Achtung: delimit cr funktioniert evtl. nicht, daher do-file manuell ausführen" health dataset: creating BMI, labeling and recoding variables																							 */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/criiia_tb_02.do"					//labour dataset, creation of income and workload variables */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/criiib_es_02.do"					//health status */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crii_inr_02.do"					//rural income household */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crii_in_02.do"					//no-labor household income */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/crc_portad_02.do"				//control book, portad dataset containing information on geography and size of communities hhs living in */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/iib_sm_02.do"					//mental health */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/criiia_ed_02.do"					//education and indigenous */
	/* do "Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze/Do Files/02/master_02.do"					//merging 2002 data */


exit
