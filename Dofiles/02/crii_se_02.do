set more off
capture log close
global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datens�tze""
cd $homefolder
**log-file starten**

log using "Logs/crii_se_02.log", replace


***book 2 --> economic shocks of hh

use "Daten 02/ii_se_02", clear

*recoding variables from 1=Yes, 3=No to 1=Yes, 0=No

	foreach var of varlist  se01a-se01f{
	replace `var'=0 if `var'==3
	}

*replace 8 with missing, because it seems that there was an 8 put everywhere where ther was a 1 for the first hh member that died
	foreach var of varlist se02ab_1-se02fc_2 {
	replace `var'=. if  `var'==8
	}

*creating variables showing number of years that have passed since hh-member died/diseased

	foreach var of varlist  se02aa_2 se02ab_2 se02ac_2 se02ba_2 se02bb_2 se02bc_2 se02ca_2 se02cb_2 se02cc_2 {
	gen time_`var' = (2006-`var')
	}

*renameming and labeling new variables

	rename  time_se02aa_2 t_died1
	rename  time_se02ab_2 t_died2
	rename  time_se02ac_2 t_died3
	rename  time_se02ba_2 t_dise1
	rename 	time_se02bb_2 t_dise2
	rename	time_se02bc_2 t_dise3
	rename  time_se02ca_2 t_unem1
	rename  time_se02cb_2 t_unem2
	rename  time_se02cc_2 t_unem3

	label var t_died1 "Years since hh-member died (till 2006)"
	label var t_died2 "Years since hh-member died (till 2006)"
	label var t_died3 "Years since hh-member died (till 2006)"
	label var t_dise1 "Years since hh-member diseased (till 2006)"
	label var t_dise2 "Years since hh-member diseased (till 2006)"
	label var t_dise3 "Years since hh-member diseased (till 2006)"
	label var t_unem1 "Years since hh-member is unemployed/without buisness (till 2006)"
	label var t_unem2 "Years since hh-member is unemployed/without buisness (till 2006)"
	label var t_unem3 "Years since hh-member is unemployed/without buisness (till 2006)"


*Value labeling who died/diseased/lost job?

#delimit ;
	label define who
	1 "The respondent"
	2 "The respondent's spouse"
	3 "Respondent's son(s)/daughter(s)"
	4 "The respondent's parents"
	5 "The respondent's parents-in-law"
	6 "Respondent's brother(s)/sister(s)"
	7 "Respondent's brother(s)/sister(s)-in-law"
	8 "Others household members (specify)";
	foreach var of varlist  se03aa_1-se03cc_1 {;
	label values `var' who;
	};
#delimit cr



#delimit ;
	label define action1
	1 "Borrow money";
	label define action2
	2 "Sell assets";
	label define action3
	3 "Work overtime";
	label define action4
	4 "new job/activity to help cover expenses";
	label define action5
	5 "Get an extra job";
	label define action6
	6 "Stop going to school";
	label define action7
	7 "Started/sold a business";
	label define action8
	8 "Other (specify)";
	label define action9
	9 "Did not do anything";
	label define action10
	10 "Saved";
	label define action11
	11 "Received help from family/friends";
	label define action98
	98 "Don�t know";

#delimit cr

	label values  se05_1a action1
	label values  se05_1b action2
	label values  se05_1c action3
	label values  se05_1d action4
	label values  se05_1e action5
	label values  se05_1f action6
	label values  se05_1g action7
	label values  se05_1h action8
	label values  se05_1i action9
	label values  se05_1j action10
	label values  se05_1k action11
	label values  se05_1l action98




*creating dummy for those who diseased and have lost employment

	gen dis_unem = 0
	replace dis_unem =1 if se01b==1 & se01c==1
	replace dis_unem =. if se01b==. & se01c==.

	gen dis_unem1 = 0
	replace dis_unem1 = 1 if t_dise1 >= t_unem1 & t_dise1 < .
	replace dis_unem1 = . if t_dise1==. | t_unem1==.

	gen dis_unem2 = 0
	replace dis_unem2 = 1 if t_dise2 >= t_unem2 & t_dise2 < .
	replace dis_unem2 = . if t_dise2==. | t_unem2==.

	gen dis_unem3 = 0
	replace dis_unem3 = 1 if t_dise3 >= t_unem3 & t_dise3 < .
	replace dis_unem3 = . if t_dise3==. | t_unem3==.

	label var dis_unem "=1 if household had diseased and unemployed member in last 5 years"
	label var dis_unem1 "=1 if member first diseased and then unemployed"
	label var dis_unem2 "=1 if a 2nd hh-member first diseased and then unemployed"
	label var dis_unem3 "=1 if a 3rd hh-member first diseased and then unemployed"



save "Daten 02/ii_se_02shock.dta", replace

log close

exit


