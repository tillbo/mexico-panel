capture log close
set more off
global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/crii_in_02.log", replace

use "Daten 02/ii_in_02", clear


**no-labor income of household

	egen incnolabhh=rowtotal( in01a1_2 in01a2_2 in01a3_2 in01a4_2 in01a5_2 in01a6_2 in01a7_2 in01a8_2 in01a9_2 in01a10_21 in01b_2 in01c_2 in01d_2 in01e_2 in01f_2 in01g_2 in01h_2 in01i_2 in01j_2 in01k_21)  //no laber income for total household in last 12 months




**labeling variables
	label var incnolabhh "12 mth no-labor income household"


**keep needed variables

	keep incnolabhh folio

save "Daten 02/ii_in_02nolabinc", replace

log close

exit


