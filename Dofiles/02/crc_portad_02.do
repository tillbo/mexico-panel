capture log close
set more off

global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/crc_portad_02.log", replace

**control book, portad dataset containing information on geography and size of communities hhs living in

use"Daten 02/c_portad_02", clear

rename edo ent




**droping missing observations in folio and one double folio

drop if  folio==. |  folio==8486000 & ls==7

save "Daten 02/c_portad_02hhsize", replace
log close
exit


