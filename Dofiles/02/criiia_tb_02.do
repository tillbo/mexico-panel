capture log close
set more off
global homefolder "/gpfs/med/gsd12ytu/thesis/ENNViH/Data"
*global homefolder ""Z:/home/till/Dropbox/Master/Master Thesis/ENNViH/Datensätze""
cd $homefolder
**log-file starten**

log using "Logs/criiia_tb_02.log", replace

use "Daten 02/iiia_tb_02", clear

***work-dataset--> income etc.***
	*generate pid_link

	gen str8 var1 = string(folio, "%08.0f")
	gen str2 var2 = string(ls, "%02.0f")
	gen pid_link = var1 + var2









save "Daten 02/iiia_tb_02inco", replace

log close
exit
